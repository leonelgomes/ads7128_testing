# SPI Driver Design

This document describes the design of the SPI (Serial Peripheral Interface)
driver for the C28x DSP core.

## Requirements

1. The SPI driver shall be DMA-driven to allow memory-transfer offload
from the CPU.
1. The SPI driver shall support different clock polarity and phase
configurations.
1. The SPI driver shall support all available hardware SPI instances.
1. The SPI driver shall operate in controller mode only.
1. The SPI driver shall control the CLK (clock), PICO (Peripheral-In,
Controller-Out), and POCI (Peripheral-Out, Controller-In) lines.
1. The SPI driver shall work within an RTOS context, avoiding race
conditions, task starvation, and interrupt blocking.
1. The SPI driver shall support a transmit function, where data is only
sent.
1. The SPI driver shall support a receive function, where data is only
received.
1. The SPI driver shall support a transceive function, where data can be
received and sent at the same time.
1. The SPI driver shall allow the user to configure the baud rate.
1. The SPI driver shall only support transfers that are a multiple of
16-bits.
1. The SPI driver shall support a configurable user function callback,
for notifying the user of transfer completion and errors.
1. The SPI driver shall not be responsible for buffer placement or
alignment.
1. The maximum SPI transfer size (in 16-bit bytes) shall be 65,535.

## High Level Design

### Enumeration Macros

Note, each C `enum`:

* Shall not use explicit numerical assignments, unless required in the
driver design
* Shall have a final entry with postfix `_MAX` to denote the number of
entries in the enum
* Shall require each entry to have a prefix as to the name of the enum
type, separate by a `_`.

#### Return Enum

An enum type shall be returned from everything public API function to
indicate if the call succeeded, and if not, why it failed.

##### Example

```c
typedef enum SPI_RETURN_en {
    SPI_RETURN_SUCCESS,
    SPI_RETURN_NULL,
    SPI_RETURN_NOT_INIT,
    SPI_RETURN_BUSY,
    SPI_RETURN_INVALID_SIZE,
    SPI_RETURN_DEVICE_ERROR,
    SPI_RETURN_MAX,
} SPI_RETURN_t;
```

#### Return Enum

An enum type shall be used to indicate the internal state machine of the
driver for each instance.

##### Example

```c
typedef enum SPI_STATE_en {
    SPI_STATE_UNINIT,
    SPI_STATE_READY,
    SPI_STATE_BUSY,
} SPI_STATE_t;
```

#### Instance Enum

An enum type shall be used to represent each available SPI hardware
instance.

##### Example

```c
typedef enum SPI_INSTANCE_en {
    SPI_INSTANCE_A,
    SPI_INSTANCE_B,
} SPI_INSTANCE_t;
```

#### Clock Polarity and Phase Enum

An enum type shall be used to represent the available clock polarity and
phase configurations.

##### Example

```c
typedef enum SPI_CLK_CONFIG_en {
    SPI_CLK_CONFIG_RISE_NO_DELAY,
    SPI_CLK_CONFIG_RISE_DELAY,
    SPI_CLK_CONFIG_FALL_NO_DELAY,
    SPI_CLK_CONFIG_FALL_DELAY,
} SPI_CLK_CONFIG_t;
```

### Type Definitions

#### Word Unit

The only supported word size is 16-bits. This is due to a requirement that
the DMA on the C28x only supports 16-bit word transfers.

```c
typedef uint16_t spi_word_unit_t;
```

#### Callback Function

The user-provided callback function shall not return a value, and shall
pass a single boolean value to denote transfer success or failure.

```c
typedef (void)(* functionCallback_t)(bool);
```

### Public Structures

#### Configuration Struct

This struct shall be used when first initialising a SPI instance.
It shall contain all necessary information to allow the one-time setup.

```c
typedef struct spi_config_st {
    SPI_INSTANCE     instance;
    SPI_CLK_CONFIG   clk_config;
    uint32_t            baud_rate;
} spi_config_t;
```

#### Transfer Struct

This struct shall be used when for transmit, received, and transceive
functions to denote the buffer, size, and optional callback to use for
the transfer.

The user shall ensure the provided buffers are 16-bit aligned and in
globally shared RAM so it can be accessed by the DMA.

The provided size shall be in 16-bit bytes.

The provided callback is optional, and if non-NULL shall be called when
a transfer has finished (whether successfully, or unsuccessfully).

```c
typedef struct spi_transfer_st {
    const spi_word_unit *const txBuffer;
    spi_word_unit *const rxBuffer;
    const uint16_t size;
    const functionCallback callback;
} spi_transfer_t;
```

### Public APIs

#### Initialisation Function

The initialisation function shall be called to initialise **each** SPI
instance before any other functions can be called.

It shall initialise the SPI and DMA hardware based-on the user provided
configuration, as well as enabling all required interrupts. It should
prepare all required internal buffers and variables for the driver to
function.

##### Prototype

```c
SPI_RETURN spi_init(const SPI_INSTANCE instance,
    const SPI_CONFIG *const config);
```

#### Transmit Function

The transmit function shall perform a DMA-driven transfer of data in the Tx
(PICO) direction only. It shall require the initialisation function for the
SPI instance to be called prior.

The user-provided chip select pin will be used to drive the specified line
active during the transfer.

##### Prototype

```c
SPI_RETURN spi_transmit(const SPI_INSTANCE instance,
    const uint32_t cs_pin,
    const SPI_TRANSFER *const transfer);
```

#### Receive Function

The transmit function shall perform a DMA-driven receive of data in the Rx
(POCI) direction only. It shall require the initialisation function for the
SPI instance to be called prior.

The user-provided chip select pin will be used to drive the specified line
active during the transfer.

##### Prototype

```c
SPI_RETURN spi_receive(const SPI_INSTANCE instance,
    const uint32_t cs_pin,
    const SPI_TRANSFER *const transfer);
```

#### Transceive Function

The transceive function shall perform a DMA-driven transfer of data in both
the Tx and Rx direction simultaneously. The size shall be used for the whole
transfer.

If it is required by the user to have a subsection of the transfer where only
one direction is necessary, then the user shall:

* Fill the necessary part of the Tx buffer with dummy data *or*
* Post-process the Rx buffer after the transaction to ignore the necessary
part.

It shall require the initialisation function for the
SPI instance to be called prior.

The user-provided chip select pin will be used to drive the specified line
active during the transfer.

##### Prototype

```c
SPI_RETURN spi_transceive(const SPI_INSTANCE instance,
    const uint32_t cs_pin,
    const SPI_TRANSFER *const transfer);
```

#### Deinitialise Function

The deinitialise function shall return all SPI and relevant DMA register to
their default-on-reset values, clear any internal buffers, and disable any
interrupts to bring the device to a pre-driver-initialisation state.

##### Prototype

```c
SPI_RETURN spi_deinit(const SPI_INSTANCE instance);
```

#### Status Function

The status function shall return the state of the driver, with respect to
the provided instance.

##### Prototype

```c
SPI_RETURN spi_status(const SPI_INSTANCE instance,
    SPI_STATE *const state);
```

## References

[TMS320F2838x Technical Reference Manual](https://www.ti.com/lit/ug/spruii0e/spruii0e.pdf?ts=1697125839831)