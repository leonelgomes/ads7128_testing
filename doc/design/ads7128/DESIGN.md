# I2C Driver Design

This document describes the design of the ADS7128 [ 8-channel Analogue-to-Digital converter ]
middleware for the C28x DSP core.

## Requirements

1. The module shall intialise the underlying I2C driver, storage buffers, and any other
necessary resources.
1. The module shall allow for ADC channel configuration, using default values if not provided by the user.
1. The module shall handle samplings from the 8-independent ADC channels.
1. The module shall provide the most recent ADC samples on demand, either by channel id, or all channels in one go.
1. The module shall provide a configurable interval between sampling cycles, to avoid excessive use of the I2C bus.
1. The module shall allow the user to register a callback function that will be called whenever a sampling cycle is completed.
1. The module shall provide a way to calibrate the ADS channels.
1. The module shall handle potential errors gracefully and return error codes, to communicate issues such as I2C communication errors.
1. The module shall provide a deinitialise function, to release any acquired resources.

## High Level Design

### Enumeration Macros

Note, each C `enum`:

* Shall use explicit numerical assignment, in the first element only, e.g. '0'
* Shall have a final entry with postfix `_MAX` to denote the number of
entries in the enum
* Shall require each entry to have a prefix as to the name of the enum
type, separate by a `_`

#### Return Enum

An enum type shall be returned from everything public API function to indicate if the call
succeeded, and if not, why it failed.

##### Example

```c
typedef enum ADS_RETURN_en {
    ADS_RETURN_SUCCESS = 0,
    ADS_RETURN_NULL,
    ADS_RETURN_NOT_INIT,
    ADS_RETURN_ALREADY_INIT,
    ADS_RETURN_DEVICE_ERROR,
    ADS_RETURN_MAX,
} ADS_RETURN_t;
```

#### Return Enum

An enum type shall be used to indicate the internal state machine of the module.

##### Example

```c
typedef enum ADS_STATE_en {
    ADS_STATE_UNINIT = 0,
    ADS_STATE_READY,
    ADS_STATE_MAX,
} ADS_STATE_t;
```

#### Instance Enum

An enum type shall be used to represent each available ADC channel.

##### Example

```c
typedef enum ADS_CHANNEL_ID_en {
    ADS_CHANNEL_0 = 0,
    ADS_CHANNEL_1,
    ADS_CHANNEL_2,
    ADS_CHANNEL_3,
    ADS_CHANNEL_4,
    ADS_CHANNEL_5,
    ADS_CHANNEL_6,
    ADS_CHANNEL_7,
    ADS_CHANNEL_MAX,
} ADS_CHANNEL_ID_t;
```

#### Callback Function

Defines the user-provided callback function that will be called when the sampling cycle is completed.
Boolean value represents cycle success (true ==> success).

```c
typedef void (*functionCallback_t)(bool);
```

### Public Structures

#### Channel Struct

This struct shall be used to configure the ADS7128 internal registers.

```c
typedef struct ads_reg_st {
    uint16_t reg_addr;
    uint16_t reg_value;
} ads_reg_t;
```

### Public APIs

#### Initialisation Function

This API intialises the underlying I2C driver and the ADS7128 internal registers. If no configuration is passed, the module will use defaults.
It also allow the user to register a callback function that will be called whenever a sampling cycle is completed.

##### Prototype

```c
ADS_RETURN_t ads_init(const ads_reg_t const ads_config[],
const uint16_t size,
const functionCallback_t callback);
```

#### Deinitialise Function

The deinitialise function releases any acquired resources and sets the internal module state to uninitialised.

##### Prototype

```c
ADS_RETURN_t ads_deinit(void);
```

#### Run Function

The run function should be called periodically to run the internal state machine.

##### Prototype

```c
ADS_RETURN_t ads_run(void);
```

#### Start Function

The start function initiates the periodic sampling cycles of the ADC channels.

##### Prototype

```c
ADS_RETURN_t ads_start(void);
```

#### Stop Function

The stop function shall stops the sampling of the ADC channels.

##### Prototype

```c
ADS_RETURN_t ads_stop(void);
```

#### Read channel Function

The module handles readings from the 8-independent ADC channels, and provides the most recent one on demand, either by channel id, or all channels [ADS_CHANNEL_MAX] in one go.

##### Prototype

```c
ADS_RETURN_t ads_read_channel(uint16_t *const data, const ADS_CHANNEL_ID_t channel_id);
```

#### Calibration Function

The calibration function calibrates a single or all ADC channels [ADS_CHANNEL_MAX] in the same operation.

##### Prototype

```c
ADS_RETURN_t ads_calibration(const uint16_t calibration_value, const ADS_CHANNEL_ID_t channel_id);
```

#### Set Sampling Interval Function

This API allows a configurable interval between sampling cycles, to avoid excessive use of the I2C bus.

##### Prototype

```c
ADS_RETURN_t ads_set_sampling_interval(const uint16_t interval_seconds);
```

#### Status Function

The status function shall return the internal state of ADS module.

##### Prototype

```c
ADS_RETURN_t ads_get_status(ADS_STATE_t *state);
```

## References

[ADS7128 Datasheet](https://www.ti.com/lit/ds/symlink/ads7128.pdf?ts=1702569617401&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FADS7128)