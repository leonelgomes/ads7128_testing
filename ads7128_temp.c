/*
 * ads7128_temp.c
 *
 *  Created on: 9 Nov 2023
 *      Author: LeonelGomes
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include "board.h"
#include "i2c.h"
#include "app.h"
#include "drivers/dsp1_i2c.h"
#include "middleware/dsp1_ads7128.h"

/** I2C_1 For 8 channel PE measurement (4 are useful) */
#define I2CADDR_PE (0x13)

/** I2C_2 For 8 channel EM temperature measurement */
#define I2CADDR_EM (0x17)

#if 0
/** To verify the configuration register order during test */
const ads7128_reg_t k_ADS7128_CONFIG[] =
{
    /** Enable RMS, disable CRC, all analog */
    {.addr = 0x01, .value = 0x84},
    /** OSR is set to 128 */
    {.addr = 0x03, .value = 0x07},
    /** Autonomous conversion with high speed oscillator, 166kSPS */
    {.addr = 0x04, .value = 0x25},
    /** All analog */
    {.addr = 0x05, .value = 0x00},
    /** Autonomous sequencing starts */
    {.addr = 0x10, .value = 0x11},
    /** RMS 1024 for Channel 0 */
    {.addr = 0xC0, .value = 0x00},
};
#endif
const ads7128_reg_t k_ADS7128_CONFIG[] =
{
     /** ALERT_MAP - Alert pin is asserted when RMS_DONE = 1b */
     {.addr = 0x16, .value = 0x02},
     /** ALERT_PIN_CFG - Open-drain output. Connect external pullup resistor, */
     /** ALERT_PIN_CFG - Alert pin active high */
     {.addr = 0x17, .value = 0x01},
     /** RMS_CFG - RMS on CH1, Do not subtract DC, samples = 1024 */
     {.addr = 0xC0, .value = 0x10},
     /** GENERAL_CFG - RMS_EN, No CRC, STATS_EN, No Comparator, */
     /** GENERAL_CFG - normal conv start, default ch config, no cal, no reset */
     {.addr = 0x01, .value = 0xA0},
     /** SEQUENCE_CFG - SEQ_MODE = '00', */
     /** SEQUENCE_CFG - No write needed, default works */
//     {.addr = 0x10, .value = 0x00},
     /** OPMODE_CFG - CONV_MODE = '00', */
     /** OPMODE_CFG - No write needed, default works */
//     {.addr = 0x04, .value = 0x00},
     /** GPIO_CFG - No write needed, default works */
//     {.addr = 0x07, .value = 0x00},
     /** AUTO_SEQ_CH_SEL - This selects channel 1 as the only channel in the */
     /** sequence (not completely clear in data sheet) */
     {.addr = 0x12, .value = 0x02},
     /** SEQUENCE_CFG - Stop channel sequencing, auto sequence mode */
     /** (this is done per flow chart) */
     {.addr = 0x10, .value = 0x01},
     /** OPMODE_CFG - Autonomous mode, high speed oscillator, */
     /** OPMODE_CFG - Sampling freq = 125ksps */
     {.addr = 0x04, .value = 0x26},
};

#if 0
/* To keep track of the i2c transaction states */
/* Needs to be volatile because its value is changed within ISR scope */
volatile static uint16_t g_i2c_status = 0;
#endif

/* Needs to be volatile because its value is changed within ISR scope */
volatile static int g_i2c_status_done = 0;

/* Needs to be volatile because its value is changed within ISR scope */
volatile static int g_ads7128_status_done = 0;

static void i2c_callback(bool result);

static void ads7128_callback(void);
static int read_int_from_UART(void);
static void i2c_commands(void);

void ads7128_temp_proc()
{
    static int state = 2;

    switch (state)
    {
    case 1:
        CPUTimer_enableInterrupt(CPUTIMER1_BASE);
        CPUTimer_startTimer(CPUTIMER1_BASE);
        ESTOP0;
        break;

    case 2: {
        uint16_t static user_choice = 0U;
        ADS7128_STATE_t ads_state = ADS7128_STATE_MAX;
        ADS7128_RETURN_t rc = ADS7128_RETURN_MAX;

        printf("\nSelect an option:\n");
        printf("1. ADS init with user data\n");
        printf("2. ADS init with defaults\n");
        printf("3. ADS de-init\n");
        printf("4. ADS sw reset\n");
        printf("5. ADS start [ RMS ]\n");
        printf("6. ADS stop [ RMS ]\n");
        printf("7. ADS Calibration\n");
        printf("8. I2C cmd\n");
        printf("10. ADS status\n");
        printf("\n");
        fflush(stdout);
        DEVICE_DELAY_US(500000);

        if (1U == user_choice) {
            printf("You selected: %u\n", user_choice);
            // de-initialise first
            rc = ads7128_deinit();
            ads7128_get_status(&ads_state);
            printf("rc: %u, st: %u\n", rc, ads_state);
            // call init with user-data
            uint16_t arr_size = sizeof(k_ADS7128_CONFIG) / sizeof(k_ADS7128_CONFIG[0]);
            rc = ads7128_init(k_ADS7128_CONFIG, arr_size, I2CADDR_PE, ads7128_callback);
            ads7128_get_status(&ads_state);
            printf("rc: %u, st: %u\n", rc, ads_state);
        } else if (2U == user_choice) {
            printf("You selected: %u\n", user_choice);
            // de-initialise first
            rc = ads7128_deinit();
            ads7128_get_status(&ads_state);
            printf("rc: %u, st: %u\n", rc, ads_state);
            // call init with defaults
            rc = ads7128_init(NULL, 0U, I2CADDR_PE, ads7128_callback);
            ads7128_get_status(&ads_state);
            printf("rc: %u, st: %u\n", rc, ads_state);
        } else if (3U == user_choice) {
            printf("You selected: %u\n", user_choice);
            rc = ads7128_deinit();
            ads7128_get_status(&ads_state);
            printf("rc: %u, st: %u\n", rc, ads_state);
        } else if (4U == user_choice) {
            printf("You selected: %u\n", user_choice);
            rc = ads7128_reset();
            ads7128_get_status(&ads_state);
            printf("rc: %u, st: %u\n", rc, ads_state);
        } else if (5U == user_choice) {
            printf("You selected: %u\n", user_choice);
            rc = ads7128_start();
            printf("rc: %u\n", rc);
            ads7128_get_status(&ads_state);
            printf("rc: %u, st: %u\n", rc, ads_state);
            uint16_t data[ADS7128_MAX_CH];
            rc = ads7128_read(data);
            printf("rc: %u\n", rc);
            for (uint16_t i = 0; i < ADS7128_MAX_CH; i++) {
                printf("ch: %u, 0x%04x \n", i, data[i]);
            }
            printf("Notify: %d\n", g_ads7128_status_done);
            g_ads7128_status_done = 0;
        } else if (6U == user_choice) {
            printf("You selected: %u\n", user_choice);
            rc = ads7128_stop();
            ads7128_get_status(&ads_state);
            printf("rc: %u, st: %u\n", rc, ads_state);
        } else if (7U == user_choice) {
            printf("You selected: %u\n", user_choice);
            rc = ads7128_calibration();
            ads7128_get_status(&ads_state);
            printf("rc: %u, st: %u\n", rc, ads_state);
        } else if (8U == user_choice) {
            printf("You selected: %u\n", user_choice);
            i2c_commands();
        } else if (10U == user_choice) {
            printf("You selected: %u\n", user_choice);
            rc = ads7128_get_status(&ads_state);
            printf("rc: %u, st: %u\n", rc, ads_state);
        } else {
            printf("You selected: %u\n", user_choice);
            printf("Invalid selection: %d\n", user_choice);
            rc = ads7128_get_status(&ads_state);
            printf("rc: %u, st: %u\n", rc, ads_state);
        }
    } break;

    case 10:
        printf("No more jobs to do\r\n");
        state++;
    break;

    default:
        break;
    }

    fflush(stdout);
}

static void i2c_callback(bool result)
{
    if (!result)
    {
        g_i2c_status_done = -1;
    }
    else
    {
        g_i2c_status_done = 1;
    }
}

static void ads7128_callback(void)
{
    g_ads7128_status_done = 1;
}
/** 1ms tick */
__interrupt void INT_CPU_TIMER1_ISR(void)
{
//    GPIO_togglePin(debug_pin_GPIO);
    ads7128_run();
}

static int read_int_from_UART(void) {

    char user_input[100]; // Adjust the buffer size as needed
    int int_value = -1;
//    printf("Enter an integer: ");

    // Use fgets to read a line of text from the user
    if (fgets(user_input, sizeof(user_input), stdin) != NULL) {
        // Convert the string to an integer using atoi
        int_value = atoi(user_input);
    }

    return int_value;
}

static void i2c_commands(void) {

    /** SYSTEM_STATUS Register */
    #define SYS_STAT_R      (0x0)

    /** RMS_LSB Register */
    #define RMS_LSB_R       (0xC1)
    /** RMS_MSB Register */
    #define RMS_MSB_R       (0xC2)

    static uint16_t g_channel_id = 2U;
    static uint16_t i2c_cmd = 1U;
    static uint16_t tx_buf[10];
    static uint16_t rx_buf[10];

    I2C_RETURN_t rc = I2C_RETURN_SUCCESS;

    i2c_transfer_t i2c_handler = {
            .targetAddr = I2CADDR_PE,
            .txBuffer = tx_buf,
            .rxBuffer = rx_buf,
            .dataSize = 0U,
            .callback = i2c_callback,
    };

    printf("1. Read STATUS register\n");
    printf("2. Clear RMS_DONE in STATUS register, with 0x10\n");
    printf("3. Clear RMS_EN in GENERAL_CFG register, with 0x00\n");
    printf("4. Set RMS_EN in GENERAL_CFG register, with 0x80\n");
    printf("5. Start channel sequencing, SEQUENCE_CFG with 0x11\n");
    printf("6. Stop channel sequencing, SEQUENCE_CFG with 0x01\n");
    printf("7. Read RMS_LSB register\n");
    printf("8. Read RMS_MSB register\n");
    printf("\n");
    fflush(stdout);
    DEVICE_DELAY_US(500000);

    // Read STATUS register
    if (1U == i2c_cmd) {
        uint16_t i = 0U;
        tx_buf[i++] = OPCODE_SR;
        tx_buf[i++] = SYS_STAT_R;
        i2c_handler.dataSize = i;

        printf("You've selected: %u\n", i2c_cmd);

        // write first,
        rc = i2c_write(I2C_INSTANCE_A, &i2c_handler);

        if (I2C_RETURN_SUCCESS != rc) {
            printf("I2C ERROR, returned: %u\n", rc);
            return;
        }

        g_i2c_status_done = 0;
        while (0 == g_i2c_status_done) {}
        if (g_i2c_status_done < 0) {
            printf("I2C NAK\n");
            return;
        }

        DEVICE_DELAY_US(500);

        // then read
        i2c_handler.dataSize = 1U;
        rc = i2c_read(I2C_INSTANCE_A, &i2c_handler);

        if (I2C_RETURN_SUCCESS != rc) {
            printf("I2C ERROR, returned: %u\n", rc);
            return;
        }

        g_i2c_status_done = 0;
        while (0 == g_i2c_status_done) {}
        if (g_i2c_status_done > 0) {
            printf("SYS_STAT_R: 0x%02x\n", rx_buf[0U]);
            fflush(stdout);
            DEVICE_DELAY_US(500000);
            i2c_cmd = 10U;
        } else {
            printf("I2C NAK\n");
        }
    }

    // Select RMS channel, RMS_CFG
    if (10U == i2c_cmd) {
        uint16_t i = 0U;
        tx_buf[i++] = OPCODE_SW;
        tx_buf[i++] = RMS_CFG_R;
        if (++g_channel_id > 2U) {
            g_channel_id = 1U;
        }
        tx_buf[i++] = g_channel_id << 4U;
        i2c_handler.dataSize = i;

        printf("You've selected: %u\n", i2c_cmd);

        // write
        rc = i2c_write(I2C_INSTANCE_A, &i2c_handler);

        if (I2C_RETURN_SUCCESS != rc) {
            printf("I2C ERROR, returned: %u\n", rc);
            return;
        }

        g_i2c_status_done = 0;
        while (0 == g_i2c_status_done) {}
        if (g_i2c_status_done < 0) {
            printf("I2C NAK\n");
            return;
        } else {
            printf("OK\n");
            i2c_cmd++;
        }
    }

    // Select channel sequence, AUTO_SEQ_CH_SEL
    if (11U == i2c_cmd) {
        uint16_t i = 0U;
        tx_buf[i++] = OPCODE_SW;
        tx_buf[i++] = AUTO_SEQ_CH_SEL_R;
        tx_buf[i++] = 1U << g_channel_id;
        i2c_handler.dataSize = i;

        printf("You've selected: %u\n", i2c_cmd);

        // write
        rc = i2c_write(I2C_INSTANCE_A, &i2c_handler);

        if (I2C_RETURN_SUCCESS != rc) {
            printf("I2C ERROR, returned: %u\n", rc);
            return;
        }

        g_i2c_status_done = 0;
        while (0 == g_i2c_status_done) {}
        if (g_i2c_status_done < 0) {
            printf("I2C NAK\n");
            return;
        } else {
            printf("OK\n");
            i2c_cmd++;
        }
    }

    // Clear RMS_DONE in STATUS register, with 0x10
    if (12U == i2c_cmd) {
        uint16_t i = 0U;
        tx_buf[i++] = OPCODE_SW;
        tx_buf[i++] = SYS_STAT_R;
        tx_buf[i++] = 0x10;
        i2c_handler.dataSize = i;

        printf("You've selected: %u\n", i2c_cmd);

        // write
        rc = i2c_write(I2C_INSTANCE_A, &i2c_handler);

        if (I2C_RETURN_SUCCESS != rc) {
            printf("I2C ERROR, returned: %u\n", rc);
            return;
        }

        g_i2c_status_done = 0;
        while (0 == g_i2c_status_done) {}
        if (g_i2c_status_done < 0) {
            printf("I2C NAK\n");
            return;
        } else {
            printf("OK\n");
            i2c_cmd++;
        }
    }

    // Clear RMS_EN in GENERAL_CFG register, with 0x00
    if (13U == i2c_cmd) {
        uint16_t i = 0U;
        tx_buf[i++] = OPCODE_SW;
        tx_buf[i++] = GEN_CFG_R;
        tx_buf[i++] = 0x00;
        i2c_handler.dataSize = i;

        printf("You've selected: %u\n", i2c_cmd);

        // write
        rc = i2c_write(I2C_INSTANCE_A, &i2c_handler);

        if (I2C_RETURN_SUCCESS != rc) {
            printf("I2C ERROR, returned: %u\n", rc);
            return;
        }

        g_i2c_status_done = 0;
        while (0 == g_i2c_status_done) {}
        if (g_i2c_status_done < 0) {
            printf("I2C NAK\n");
            return;
        } else {
            printf("OK\n");
            i2c_cmd++;
        }
    }

    // Set RMS_EN in GENERAL_CFG register, with 0x80
    if (14U == i2c_cmd) {
        uint16_t i = 0U;
        tx_buf[i++] = OPCODE_SW;
        tx_buf[i++] = GEN_CFG_R;
        tx_buf[i++] = 0x80;
        i2c_handler.dataSize = i;

        printf("You've selected: %u\n", i2c_cmd);

        // write
        rc = i2c_write(I2C_INSTANCE_A, &i2c_handler);

        if (I2C_RETURN_SUCCESS != rc) {
            printf("I2C ERROR, returned: %u\n", rc);
            return;
        }

        g_i2c_status_done = 0;
        while (0 == g_i2c_status_done) {}
        if (g_i2c_status_done < 0) {
            printf("I2C NAK\n");
            return;
        } else {
            printf("OK\n");
            i2c_cmd++;
        }
    }

    // Start channel sequencing, SEQUENCE_CFG with 0x11
    if (15U == i2c_cmd) {
        uint16_t i = 0U;
        tx_buf[i++] = OPCODE_SW;
        tx_buf[i++] = SEQUENCE_CFG_R;
        tx_buf[i++] = 0x11;
        i2c_handler.dataSize = i;

        printf("You've selected: %u\n", i2c_cmd);

        // write
        rc = i2c_write(I2C_INSTANCE_A, &i2c_handler);

        if (I2C_RETURN_SUCCESS != rc) {
            printf("I2C ERROR, returned: %u\n", rc);
            return;
        }

        // turn on
        GPIO_writePin(debug_pin_GPIO, 1U);

        g_i2c_status_done = 0;
        while (0 == g_i2c_status_done) {}
        if (g_i2c_status_done < 0) {
            printf("I2C NAK\n");
            return;
        } else {
            printf("OK\n");
            i2c_cmd++;
            DEVICE_DELAY_US(10000);
        }
    }

    /**
     * Either poll the RMS_DONE flag, or wait a fixed time long enough to
     * guarantee that the conversion is done
     */

    // Stop channel sequencing, SEQUENCE_CFG with 0x01
    if (16U == i2c_cmd) {
        uint16_t i = 0U;
        tx_buf[i++] = OPCODE_SW;
        tx_buf[i++] = SEQUENCE_CFG_R;
        tx_buf[i++] = 0x01;
        i2c_handler.dataSize = i;

        printf("You've selected: %u\n", i2c_cmd);

        // turn off
        GPIO_writePin(debug_pin_GPIO, 0U);

        // write
        rc = i2c_write(I2C_INSTANCE_A, &i2c_handler);

        if (I2C_RETURN_SUCCESS != rc) {
            printf("I2C ERROR, returned: %u\n", rc);
            return;
        }

        g_i2c_status_done = 0;
        while (0 == g_i2c_status_done) {}
        if (g_i2c_status_done < 0) {
            printf("I2C NAK\n");
            return;
        } else {
            printf("OK\n");
            i2c_cmd++;
        }
    }

    // Read RMS_LSB register
    if (17U == i2c_cmd) {
        uint16_t i = 0U;
        tx_buf[i++] = OPCODE_SR;
        tx_buf[i++] = RMS_LSB_R;
        i2c_handler.dataSize = i;

        printf("You've selected: %u\n", i2c_cmd);

        // write first,
        rc = i2c_write(I2C_INSTANCE_A, &i2c_handler);

        if (I2C_RETURN_SUCCESS != rc) {
            printf("I2C ERROR, returned: %u\n", rc);
            return;
        }

        g_i2c_status_done = 0;
        while (0 == g_i2c_status_done) {}
        if (g_i2c_status_done < 0) {
            printf("I2C NAK\n");
            return;
        }

        DEVICE_DELAY_US(500);

        // then read
        i2c_handler.dataSize = 1U;
        rc = i2c_read(I2C_INSTANCE_A, &i2c_handler);

        if (I2C_RETURN_SUCCESS != rc) {
            printf("I2C ERROR, returned: %u\n", rc);
            return;
        }

        g_i2c_status_done = 0;
        while (0 == g_i2c_status_done) {}
        if (g_i2c_status_done > 0) {
            printf("CH: %u, RMS_LSB: 0x%02x\n", g_channel_id, rx_buf[0U]);
            i2c_cmd++;
        } else {
            printf("I2C NAK\n");
        }
    }

    // Read RMS_MSB register
    if (18U == i2c_cmd) {
        uint16_t i = 0U;
        tx_buf[i++] = OPCODE_SR;
        tx_buf[i++] = RMS_MSB_R;
        i2c_handler.dataSize = i;

        printf("You've selected: %u\n", i2c_cmd);

        // write first,
        rc = i2c_write(I2C_INSTANCE_A, &i2c_handler);

        if (I2C_RETURN_SUCCESS != rc) {
            printf("I2C ERROR, returned: %u\n", rc);
            return;
        }

        g_i2c_status_done = 0;
        while (0 == g_i2c_status_done) {}
        if (g_i2c_status_done < 0) {
            printf("I2C NAK\n");
            return;
        }

        DEVICE_DELAY_US(500);

        // then read
        i2c_handler.dataSize = 1U;
        rc = i2c_read(I2C_INSTANCE_A, &i2c_handler);

        if (I2C_RETURN_SUCCESS != rc) {
            printf("I2C ERROR, returned: %u\n", rc);
            return;
        }

        g_i2c_status_done = 0;
        while (0 == g_i2c_status_done) {}
        if (g_i2c_status_done > 0) {
            printf("CH: %u, RMS_MSB: 0x%02x\n", g_channel_id, rx_buf[0U]);
            i2c_cmd = 1U;
        } else {
            printf("I2C NAK\n");
        }
    }
}
