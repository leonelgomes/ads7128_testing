/**
 * @file dsp1_spi.c
 * @author Adam Lofthouse-Hill (Enoda Ltd.)
 * @brief DMA-Driven SPI driver to work with the DSP C28x core in an
 * RTOS Context.
 * @date 2023-11-16
 *
 * @copyright Copyright (C) 2023 Enoda Ltd. - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

/* ==== INCLUDES ===== */

#include "dsp1_spi.h"

#include <FreeRTOS.h>
#include <board.h>
#include <semphr.h>
#include <string.h>

/* ==== DEFINES ===== */

/** SPI Driver is fixed to 16-bit word units */
#define WORD_UNIT_WIDTH (16u)

/** Configure the FIFO transmit delay in clock cycles */
#define TX_FIFO_DELAY (0u)

/** Sets the PIE interrupt group used for DMA interrupts */
#define DMA_PIE_GROUP (INTERRUPT_ACK_GROUP7)

/* ==== TYPEDEFS ===== */

/**
 * Represents the internal state of a SPI hardware
 * instance/bus.
 */
typedef volatile struct hw_instance_state_st {
    SPI_STATE_t state;       /*!< Current hardware state */
    spi_transfer_t transfer; /*!< Current transfer parameters */
    bool tx_done;            /*!< Has the Tx side of the transfer finished */
    bool rx_done;            /*!< Has the Rx side of the transfer finished */
} hw_instance_state_t;

/**
 * Represents the static mapping of a SPI HW instances to
 * its linked DMA configuration.
 */
typedef struct dma_cfg_st {
    uint32_t rx_channel_base; /*!< Register base for the DMA Rx Channel */
    uint32_t tx_channel_base; /*!< Register base for the DMA Tx Channel */
    uint16_t *spi_rx_buf;     /*!< Source for DMA Rx to read from */
    uint16_t *spi_tx_buf;     /*!< Destination for DMA Tx to write to */
} dma_cfg_t;

/* ==== GLOBAL VARIABLES ===== */

/** Internal global state tracker for each SPI HW instance */
static hw_instance_state_t g_instances[SPI_INSTANCE_MAX];

/** Internal mutex to manage access to the global state tracker within
 * an RTOS context.
 */
static volatile SemaphoreHandle_t g_instanceMutex[SPI_INSTANCE_MAX] = {NULL, NULL};

/**
 * Compile-time configuration describing the relationship between a SPI
 * HW instance and it's linked DMA Tx and Rx channels.
 */
static const dma_cfg_t k_dma_cfg[SPI_INSTANCE_MAX] = {
    {
        .rx_channel_base = SPI_A_RX_DMA_BASE,
        .tx_channel_base = SPI_A_TX_DMA_BASE,
        .spi_rx_buf = SPI_A_RX_DMA_ADDRESS,  // cppcheck-suppress misra-c2012-11.4
        .spi_tx_buf = SPI_A_TX_DMA_ADDRESS,  // cppcheck-suppress misra-c2012-11.4
    },
    {
        .rx_channel_base = SPI_B_RX_DMA_BASE,
        .tx_channel_base = SPI_B_TX_DMA_BASE,
        .spi_rx_buf = SPI_B_RX_DMA_ADDRESS,  // cppcheck-suppress misra-c2012-11.4
        .spi_tx_buf = SPI_B_TX_DMA_ADDRESS,  // cppcheck-suppress misra-c2012-11.4
    }};

/* ==== FUNCTION PROTOTYPES ===== */

static void init_spi(const SPI_INSTANCE_t instance);
static SPI_RETURN_t check_ready(const SPI_INSTANCE_t instance);
static SPI_RETURN_t get_state(const SPI_INSTANCE_t instance, SPI_STATE_t *const state,
                              const bool retain_lock);

static void setup_transfer(const SPI_INSTANCE_t instance, const spi_transfer_t *const transfer);

static SPI_RETURN_t check_transfer_params(const SPI_INSTANCE_t instance,
                                          const spi_transfer_t *const transfer, const bool is_tx,
                                          const bool is_rx);

static void setup_tx(const SPI_INSTANCE_t instance, const spi_transfer_t *const transfer);

static void setup_rx(const SPI_INSTANCE_t instance, const spi_transfer_t *const transfer);

static inline uint32_t spi_base(const SPI_INSTANCE_t instance);
static inline void cs_assert(const uint32_t pin, const bool active_low);
static inline void cs_deassert(const uint32_t pin, const bool active_low);
static inline SPI_RETURN_t acquire_lock(const SPI_INSTANCE_t instance);
static inline SPI_RETURN_t release_lock(const SPI_INSTANCE_t instance);

static void handle_interrupt(const SPI_INSTANCE_t instance, const bool isTx);
__interrupt void INT_SPI_A_RX_DMA_ISR(void);
__interrupt void INT_SPI_A_TX_DMA_ISR(void);
__interrupt void INT_SPI_B_RX_DMA_ISR(void);
__interrupt void INT_SPI_B_TX_DMA_ISR(void);

/* ==== APIs ===== */

SPI_RETURN_t spi_init(const SPI_INSTANCE_t instance) {
    SPI_RETURN_t rc = SPI_RETURN_SUCCESS;

    if (SPI_INSTANCE_MAX <= instance) {
        rc = SPI_RETURN_INVALID_INSTANCE;
    } else {
        /** We shouldn't try to initialise if we've already initialised
         * our global state mutex. This function is marked as not RTOS-safe
         * so we can't rely on our usual global state to check.
         */
        if (NULL != g_instanceMutex[instance]) {
            rc = SPI_RETURN_ALREADY_INIT;
        } else {
            init_spi(instance);
        }
    }

    return rc;
}

SPI_RETURN_t spi_transmit(const SPI_INSTANCE_t instance, const spi_transfer_t *const transfer) {
    SPI_RETURN_t rc = SPI_RETURN_SUCCESS;

    if (SPI_INSTANCE_MAX <= instance) {
        rc = SPI_RETURN_INVALID_INSTANCE;
    } else {
        rc = check_transfer_params(instance, transfer, true, false);
        if (SPI_RETURN_SUCCESS == rc) {
            /** Last validation step should be checking internal global as this
             * acquires the mutex lock
             */
            rc = check_ready(instance);
        }
    }

    // Validation done, start actually doing something now
    if (SPI_RETURN_SUCCESS == rc) {
        // Mark the internal global flags
        g_instances[instance].state = SPI_STATE_BUSY;
        g_instances[instance].rx_done = false;
        g_instances[instance].tx_done = false;

        // Do the actual transfer
        setup_transfer(instance, transfer);

        // Finally, we should release the lock we have on the global state.
        rc = release_lock(instance);
    }

    return rc;
}

SPI_RETURN_t spi_receive(const SPI_INSTANCE_t instance, const spi_transfer_t *const transfer) {
    SPI_RETURN_t rc = SPI_RETURN_SUCCESS;

    if (SPI_INSTANCE_MAX <= instance) {
        rc = SPI_RETURN_INVALID_INSTANCE;
    } else {
        rc = check_transfer_params(instance, transfer, false, true);
        if (SPI_RETURN_SUCCESS == rc) {
            /** Last validation step should be checking internal global as this
             * acquires the mutex lock
             */
            rc = check_ready(instance);
        }
    }

    // Validation done, start actually doing something now
    if (SPI_RETURN_SUCCESS == rc) {
        // Mark the internal global flags
        g_instances[instance].state = SPI_STATE_BUSY;
        g_instances[instance].rx_done = false;
        g_instances[instance].tx_done = false;

        // Do the actual transfer
        setup_transfer(instance, transfer);

        // Finally, we should release the lock we have on the global state.
        rc = release_lock(instance);
    }

    return rc;
}

SPI_RETURN_t spi_transceive(const SPI_INSTANCE_t instance, const spi_transfer_t *const transfer) {
    SPI_RETURN_t rc = SPI_RETURN_SUCCESS;

    if (SPI_INSTANCE_MAX <= instance) {
        rc = SPI_RETURN_INVALID_INSTANCE;
    } else {
        rc = check_transfer_params(instance, transfer, true, true);
        if (SPI_RETURN_SUCCESS == rc) {
            /** Last validation step should be checking internal global as this
             * acquires the mutex lock
             */
            rc = check_ready(instance);
        }
    }

    // Validation done, start actually doing something now
    if (SPI_RETURN_SUCCESS == rc) {
        // Mark the internal global flags
        g_instances[instance].state = SPI_STATE_BUSY;
        g_instances[instance].rx_done = false;
        g_instances[instance].tx_done = false;

        // Do the actual transfer
        setup_transfer(instance, transfer);

        // Finally, we should release the lock we have on the global state.
        rc = release_lock(instance);
    }

    return rc;
}

SPI_RETURN_t spi_deinit(const SPI_INSTANCE_t instance) {
    SPI_RETURN_t rc = SPI_RETURN_SUCCESS;

    if (SPI_INSTANCE_MAX <= instance) {
        rc = SPI_RETURN_INVALID_INSTANCE;
    } else {
        // This will acquire and keep the lock on the global state
        rc = check_ready(instance);

        if (SPI_RETURN_SUCCESS == rc) {
            uint32_t base_spi = spi_base(instance);
            uint32_t base_dma_rx = k_dma_cfg[instance].rx_channel_base;
            uint32_t base_dma_tx = k_dma_cfg[instance].tx_channel_base;

            SPI_disableModule(base_spi);

            // Clear any lingering interrupts and errors
            SPI_clearInterruptStatus(base_spi,
                                     (SPI_INT_RX_OVERRUN | SPI_INT_RX_DATA_TX_EMPTY | SPI_INT_RXFF |
                                      SPI_INT_TXFF | SPI_INT_RXFF_OVERFLOW));
            DMA_clearErrorFlag(base_dma_rx);
            DMA_clearErrorFlag(base_dma_tx);
            DMA_clearTriggerFlag(base_dma_rx);
            DMA_clearTriggerFlag(base_dma_tx);

            // We must remove the semaphore last as this is the flag
            // indicating init/deinit status
            vSemaphoreDelete(g_instanceMutex[instance]);
            g_instanceMutex[instance] = NULL;
        }
    }

    return rc;
}

SPI_RETURN_t spi_status(const SPI_INSTANCE_t instance, SPI_STATE_t *const state) {
    SPI_RETURN_t rc = SPI_RETURN_SUCCESS;

    if (SPI_INSTANCE_MAX <= instance) {
        rc = SPI_RETURN_INVALID_INSTANCE;
    } else {
        if (NULL == state) {
            rc = SPI_RETURN_NULL;
        } else {
            rc = get_state(instance, state, false);
        }
    }

    return rc;
}

/* ==== STATIC FUNCTIONS ===== */

/* ==== Inline Functions ===== */

/**
 * Utility function to convert between instance enum and SPI base.
 *
 * @note This relies on instance being checked as valid before this
 * function is called.
 *
 * @param instance The SPI Hardware Instance to convert
 * @returns The relevant SPI register base
 */
static inline uint32_t spi_base(const SPI_INSTANCE_t instance) {
    return ((instance == SPI_INSTANCE_A) ? SPI_A_BASE : SPI_B_BASE);
}

/**
 * Assert the given CS (Chip Select) pin by driving it according to the
 * given parameter. Should be called before a transfer.
 *
 * @param pin The GPIO pin to assert
 * @param active_low If true, pin is driven low. Otherwise, pin is driven high.
 */
static inline void cs_assert(const uint32_t pin, const bool active_low) {
    uint32_t csOutput = active_low ? 0uL : 1uL;
    GPIO_writePin(pin, csOutput);
}

/**
 * Deassert the given CS (Chip Select) pin by driving it according to the
 * given parameter. Should be called after a transfer.
 *
 * @param pin The GPIO pin to deassert
 * @param active_low If true, pin is driven high. Otherwise, pin is driven low.
 */
static inline void cs_deassert(const uint32_t pin, const bool active_low) {
    uint32_t csOutput = active_low ? 1uL : 0uL;
    GPIO_writePin(pin, csOutput);
}

/**
 * Attempts to acquire the mutex lock on the global instance state.
 *
 * @note This should *not* be called from an ISR context.
 *
 * @param instance SPI HW instance to acquire lock for.
 *
 * @return SPI_RETURN_SUCCESS on success.
 * @return SPI_RETURN_RTOS_ERROR on failure within RTOS.
 * @return SPI_RETURN_NOT_INIT if instance is not initialised.
 */
static inline SPI_RETURN_t acquire_lock(const SPI_INSTANCE_t instance) {
    SPI_RETURN_t rc = SPI_RETURN_SUCCESS;

    // The mutex is used to flag init/uninit status
    if (g_instanceMutex[instance] == NULL) {
        rc = SPI_RETURN_NOT_INIT;
    } else {
        if (pdTRUE != xSemaphoreTake(g_instanceMutex[instance], 0)) {
            rc = SPI_RETURN_RTOS_ERROR;
        }
    }

    return rc;
}

/**
 * Attempts to release the mutex lock on the global instance state.
 *
 * @note This should *not* be called from an ISR context.
 *
 * @param instance SPI HW instance to release lock.
 *
 * @return SPI_RETURN_SUCCESS on success
 * @return SPI_RETURN_RTOS_ERROR on failure within RTOS.
 * @return SPI_RETURN_NOT_INIT if instance is not initialised.
 */
static inline SPI_RETURN_t release_lock(const SPI_INSTANCE_t instance) {
    SPI_RETURN_t rc = SPI_RETURN_SUCCESS;

    // The mutex is used to flag init/uninit status
    if (g_instanceMutex[instance] == NULL) {
        rc = SPI_RETURN_NOT_INIT;
    } else {
        if (pdTRUE != xSemaphoreGive(g_instanceMutex[instance])) {
            rc = SPI_RETURN_RTOS_ERROR;
        }
    }

    return rc;
}

/* ==== Utility Functions ===== */

/**
 * Initialises any internal global states and enables the HW instance
 *
 * @param instance SPI HW instance to initialise
 */
static void init_spi(const SPI_INSTANCE_t instance) {
    uint32_t base = spi_base(instance);

    // Start with blank uninitialised state for global state and create mutexes
    g_instanceMutex[instance] = xSemaphoreCreateMutex();
    g_instances[instance].tx_done = false;
    g_instances[instance].rx_done = false;

    // Clear out the transfer configuration
    (void)memset((void *)&(g_instances[instance].transfer), 0u,
                 sizeof(g_instances[instance].transfer));

    // Finally re-enable the module and set state to init
    SPI_enableModule(base);
    g_instances[instance].state = SPI_STATE_READY;
}

/**
 * Checks that the given instance is marked as ready in the global state.
 * If it is, then a lock is held on the global state so we can continue
 * with the transfer.
 *
 * @param instance SPI HW instance to check
 * @returns SPI_RETURN_SUCCESS on success, otherwise related error code.
 */
static SPI_RETURN_t check_ready(const SPI_INSTANCE_t instance) {
    SPI_STATE_t state;
    SPI_RETURN_t rc = get_state(instance, &state, true);

    if (SPI_RETURN_SUCCESS == rc) {
        switch (state) {
            case SPI_STATE_BUSY:
                rc = SPI_RETURN_BUSY;
                break;
            case SPI_STATE_READY:
                break;
            case SPI_STATE_MAX:
                /** Fallthrough */
            default:
                ASSERT(false);
                break;
        }

        // If any of the above failed, we must release the lock
        if (SPI_RETURN_SUCCESS != rc) {
            rc = release_lock(instance);
        }
    }

    return rc;
}

/**
 * Validates passed transfer configuration user parameters.
 *
 * @param instance SPI HW instance
 * @param transfer Pointer to user-provided transfer configuration
 * @param is_tx If true, user specified a Tx part
 * @param is_rx If true, user specified a Rx part
 * @returns SPI_RETURN_SUCCESS on success, otherwise related error code.
 */
static SPI_RETURN_t check_transfer_params(const SPI_INSTANCE_t instance,
                                          const spi_transfer_t *const transfer, const bool is_tx,
                                          const bool is_rx) {
    SPI_RETURN_t rc = SPI_RETURN_SUCCESS;

    if (NULL == transfer) {
        rc = SPI_RETURN_NULL;
    } else {
        if (is_tx && (NULL == transfer->tx_buf)) {
            rc = SPI_RETURN_NULL;
        }

        if (is_rx && (NULL == transfer->rx_buf)) {
            rc = SPI_RETURN_NULL;
        }

        if ((SPI_MAX_TRANSFER < transfer->size) || (0u == transfer->size)) {
            rc = SPI_RETURN_INVALID_SIZE;
        }

        if (!GPIO_isPinValid(transfer->cs.pin)) {
            rc = SPI_RETURN_INVALID_PIN;
        }

        if (SPI_INSTANCE_MAX <= instance) {
            rc = SPI_RETURN_INVALID_INSTANCE;
        }
    }

    return rc;
}

/**
 * Get the global state of the given instance.
 *
 * @param instance SPI HW Instance to query the state of
 * @param state Pointer to populate fetched state at.
 * @param retain_lock If true, mutex lock is not released after query.
 *
 * @note This should *not* be called from an ISR context.
 *
 * @return SPI_RETURN_SUCCESS on success
 * @return SPI_RETURN_RTOS_ERROR on failure.
 */
static SPI_RETURN_t get_state(const SPI_INSTANCE_t instance, SPI_STATE_t *const state,
                              const bool retain_lock) {
    SPI_RETURN_t rc = SPI_RETURN_SUCCESS;

    rc = acquire_lock(instance);
    if (SPI_RETURN_SUCCESS == rc) {
        *state = g_instances[instance].state;

        if (!retain_lock) {
            rc = release_lock(instance);
        }
    }

    return rc;
}

/**
 * Setup Tx side of the transfer in the DMA.
 *
 * @param instance SPI HW Instance
 * @param transfer Transfer configuration
 */
static void setup_tx(const SPI_INSTANCE_t instance, const spi_transfer_t *const transfer) {
    /** Configures the dummy bytes sent over Tx for Rx only transfers */
    static const uint16_t dummy_data = SPI_DUMMY_DATA;

    const uint32_t dmaBase = k_dma_cfg[instance].tx_channel_base;

    /** If the user-provided Tx buffer is NULL we still need
     * to send something to trigger the CLK generation
     */
    if (NULL == transfer->tx_buf) {
        // Configure DMA to read from dummy data const
        DMA_configAddresses(dmaBase, k_dma_cfg[instance].spi_tx_buf, &dummy_data);

        /** Step size should be 0 for both src + dest as this means
         * it will keep writing the same dummy data to the Tx FIFO
         */
        DMA_configBurst(dmaBase, 1, 0, 0);
        DMA_configTransfer(dmaBase, transfer->size, 0, 0);
    } else {
        DMA_configAddresses(dmaBase, k_dma_cfg[instance].spi_tx_buf, transfer->tx_buf);

        DMA_configBurst(dmaBase, 1, 1, 0);
        DMA_configTransfer(dmaBase, transfer->size, 1, 0);
    }

    DMA_enableTrigger(dmaBase);
}

/**
 * Setup Rx side of the transfer in the DMA.
 *
 * @param instance SPI HW Instance
 * @param transfer Transfer configuration
 */
static void setup_rx(const SPI_INSTANCE_t instance, const spi_transfer_t *const transfer) {
    const uint32_t dmaBase = k_dma_cfg[instance].rx_channel_base;

    DMA_configAddresses(dmaBase, transfer->rx_buf, k_dma_cfg[instance].spi_rx_buf);

    DMA_configBurst(dmaBase, 1, 0, 1);
    DMA_configTransfer(dmaBase, transfer->size, 0, 1);

    DMA_enableTrigger(dmaBase);

    // Start Rx now so it's ready before kicking off the Tx
    DMA_startChannel(dmaBase);
}

/**
 * Setup a SPI transfer configuring both SPI and DMA appropriately,
 * before finally starting the transfer.
 *
 * @note Validation of parameters should be done before calling this function.
 *
 * @param instance SPI HW Instance
 * @param transfer Transfer configuration
 */
static void setup_transfer(const SPI_INSTANCE_t instance, const spi_transfer_t *const transfer) {
    const uint32_t spiBase = spi_base(instance);

    // Update the global tracker
    g_instances[instance].state = SPI_STATE_BUSY;
    (void)memcpy((void *)(&(g_instances[instance].transfer)), transfer, sizeof(spi_transfer_t));

    SPI_setcharLength(spiBase, WORD_UNIT_WIDTH);

    // Reset the SPI FIFO buffers to clear out stale data
    SPI_disableFIFO(spiBase);
    SPI_enableFIFO(spiBase);

    SPI_setTxFifoTransmitDelay(spiBase, TX_FIFO_DELAY);

    // Tx Setup is always required even for Rx only transfer
    setup_tx(instance, transfer);

    // However, Rx Setup is only required if the buffer is provided
    if (NULL != transfer->rx_buf) {
        setup_rx(instance, transfer);
    }

    // Before starting the transfer, drive the Chip Select
    cs_assert(transfer->cs.pin, transfer->cs.active_low);

    // Finally, trigger the Tx part to start the SPI transaction
    DMA_startChannel(k_dma_cfg[instance].tx_channel_base);
}

/* ==== Interrupt Routines ===== */

/**
 * DMA Rx Channel interrupt routine for SPI A instance
 */
__interrupt void INT_SPI_A_RX_DMA_ISR(void) { handle_interrupt(SPI_INSTANCE_A, false); }

/**
 * DMA Tx Channel interrupt routine for SPI A instance
 */
__interrupt void INT_SPI_A_TX_DMA_ISR(void) { handle_interrupt(SPI_INSTANCE_A, true); }

/**
 * DMA Rx Channel interrupt routine for SPI B instance
 */
__interrupt void INT_SPI_B_RX_DMA_ISR(void) { handle_interrupt(SPI_INSTANCE_B, false); }

/**
 * DMA Tx Channel interrupt routine for SPI B instance
 */
__interrupt void INT_SPI_B_TX_DMA_ISR(void) { handle_interrupt(SPI_INSTANCE_B, true); }

/**
 * Handles a generic DMA interrupt, updating and referencing the global state
 * and calling the user-provided callback when a full transfer has finished.
 *
 * @param instance SPI HW instance
 * @param isTx If true, this is a Tx interrupt. Otherwise, this is Rx.
 */
static void handle_interrupt(const SPI_INSTANCE_t instance, const bool isTx) {
    hw_instance_state_t *volatile state;
    BaseType_t taskWokenByTake = pdFALSE;
    BaseType_t taskWokenByGive = pdFALSE;

    // Need to grab mutex as we're updating the global state
    configASSERT(xSemaphoreTakeFromISR(g_instanceMutex[instance], &taskWokenByTake) == pdPASS);

    state = &g_instances[instance];

    if (isTx) {
        state->tx_done = true;
    } else {
        state->rx_done = true;
    }

    /** The transfer is only done if, and only if:
     * - This is a Tx done interrupt AND
     *      - There is no Rx transfer
     *      - There is a Rx transfer, and it's already finished.
     * - This is a Rx done interrupt and Tx has already finished
     */
    bool finishedTransfer = false;
    if (isTx) {
        if (NULL == state->transfer.rx_buf) {
            finishedTransfer = true;
        } else {
            finishedTransfer = state->rx_done;
        }
    } else {
        finishedTransfer = state->tx_done;
    }

    if (finishedTransfer) {
        // End of transfer, if user doesn't want the line kept low,
        // time to de-assert it.
        if (state->transfer.cs.keep_active == false) {
            cs_deassert(state->transfer.cs.pin, state->transfer.cs.active_low);
        }

        state->state = SPI_STATE_READY;

        // If the user callback is provided, call that:
        if (NULL != state->transfer.callback) {
            (state->transfer.callback)(true);
        }
    }

    // Acknowledge interrupt
    Interrupt_clearACKGroup(DMA_PIE_GROUP);

    // Give back the mutex, tracking if we need to yield to another task
    configASSERT(xSemaphoreGiveFromISR(g_instanceMutex[instance], &taskWokenByGive) == pdPASS);

    // If another task was woken by either the take or give mutex action,
    // we need to make sure we yield to that task.
    portYIELD_FROM_ISR((taskWokenByTake == pdTRUE) || (taskWokenByGive == pdTRUE));
}
