/**
 * @file dsp1_i2c.h
 * @author Leonel Gomes [ Enoda Ltd ]
 * @brief Abstraction layer for writing/reading to/from an I2C instance, in
 * an RTOS context.
 * @date 15/11/2023
 *
 * @copyright Copyright (C) 2023 Enoda Ltd. - All Rights Reserved
 * 
 * @note This driver only supports I2C instance A, any attempt to use any other
 * instance will return error - always check the driver's state and returned
 * values.
 *
 * Unauthorised copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

#ifndef DSP1_I2C_H
#define DSP1_I2C_H

/* ==== INCLUDES ===== */

#include <stdint.h>
#include <stdbool.h>

/* ==== INCLUDES ===== */

/**
 * Maximum and minimum accepted values for the I2C transaction data size,
 * in 16-bit bytes.
 */
#define I2C_MAX_DATA_SIZE   (16U)
#define I2C_MIN_DATA_SIZE   (1U)

/* ==== TYPEDEFS ===== */

/**
 * Describes the possible return values from the APIs.
 */
typedef enum I2C_RETURN_en {
    I2C_RETURN_SUCCESS = 0,         /*!< API was successful */
    I2C_RETURN_NULL,                /*!< Null pointer provided */
    I2C_RETURN_NOT_INIT,            /*!< Driver not initialised */
    I2C_RETURN_BUSY,                /*!< Driver not ready for transaction */
    I2C_RETURN_INVALID_SIZE,        /*!< Data size provided not supported */
    I2C_RETURN_INVALID_INSTANCE,    /*!< Invalid instance passed */
    I2C_RETURN_MAX,                 /*!< Enum boundary max value */
} I2C_RETURN_t;

/**
 * Describes the possible return values from the internal state machine of the
 * I2C driver.
 */
typedef enum I2C_STATE_en {
    I2C_STATE_READY = 0,    /*!< Driver ready for transaction */
    I2C_STATE_NOT_INIT,     /*!< Driver not initialised */
    I2C_STATE_BUSY,         /*!< Driver busy processing ongoing transaction */
    I2C_STATE_NO_ACK,       /*!< I2C slave returned no ack */
    I2C_STATE_ARB_LOST,     /*!< I2C bus collision detected */
    I2C_STATE_MAX,          /*!< Enum boundary max value */
} I2C_STATE_t;

/**
 * Describes the possible instances of the I2C driver.
 */
typedef enum I2C_INSTANCE_en {
    I2C_INSTANCE_A = 0,     /*!< Hardware I2C_A instance */
    I2C_INSTANCE_MAX,       /*!< Enum boundary max value */
} I2C_INSTANCE_t;

/**
 * Defines the user-provided callback. Called when transfer finished.
 *
 * @note the callback function is called within ISR scope, and should only be
 * used for conditional checking and/or and to set flags.
 *
 * Boolean result represents transfer success (true ==> success).
 */
typedef void (*functionCallback)(bool);

/**
 * Describes the I2C driver data transfer structure.
 *
 * @note If callback is NULL, it'll be ignored.
 */
typedef struct i2c_transfer_st {
    /** Slave's address */
    uint16_t targetAddr;

    /** Buffer to data to be transmitted */
    const uint16_t *txBuffer;

    /** Buffer to store the received data */
    uint16_t *rxBuffer;

    /** Length of data to transmit/receive [ in 16-bit bytes ] */
    uint16_t dataSize;

    /** User-provided callback function */
    functionCallback callback;
} i2c_transfer_t;

/* ==== PUBLIC APIs ===== */

/**
 * @brief Checks the state of the I2C module, and if enabled
 * it sets the internal driver's state to 'initialised'.
 *
 * @note The I2C module is initialised outside this driver,
 * by means of the TI tool configsys, so this function only checks if it's
 * enabled.
 *
 * @warning If this function is called after the driver has being initialised,
 * it'll be reset to its initial state. This can be used if the driver becomes
 * unresponsive.
 * 
 * @warning If this function is called during a transaction, the driver will be reset 
 * to its inital state and data may be lost. To avoid this, check the driver's status first.
 * 
 * @return I2C_RETURN_t indicating success or error.
 */
I2C_RETURN_t i2c_init(const I2C_INSTANCE_t instance);

/**
 * @brief Performs a write transaction to the I2C bus. If a
 * non-null callback is passed, it'll be called once the transaction
 * is complete.
 *
 * @param instance I2C module to be used.
 * @param transfer Structure containing in/out transfer parameters.
 *
 * @return I2C_RETURN_t indicating success or error.
 */
I2C_RETURN_t i2c_write(const I2C_INSTANCE_t instance, const i2c_transfer_t *const transfer);

/**
 * @brief Performs a read transaction from an I2C slave. If a
 * non-null callback is passed, it'll be called once the transaction
 * is complete.
 *
 * @param instance I2C module to be used
 * @param transfer Structure containing in/out parameters
 *
 * @return I2C_RETURN_t indicating success or error.
 */
I2C_RETURN_t i2c_read(const I2C_INSTANCE_t instance, const i2c_transfer_t *const transfer);

/**
 * @brief Get the current state of the driver
 *
 * @param instance I2C module to be used
 * @param state Current state of the driver/transaction
 *
 * @return I2C_RETURN_t indicating success or error.
 */
I2C_RETURN_t i2c_get_status(const I2C_INSTANCE_t instance, I2C_STATE_t *state);

#endif /* DSP1_I2C_H */
