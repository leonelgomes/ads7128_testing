/**
 * @file dsp1_i2c.C
 * @author Leonel Gomes [ Enoda Ltd ]
 * @brief Abstraction layer for writing/reading to/from an I2C instance.
 * @date 15/11/2023
 *
 * @copyright Copyright (C) 2023 Enoda Ltd. - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

/* ==== INCLUDES ===== */

#include <string.h>
#include "dsp1_i2c.h"
#include "board.h"

/* ==== DEFINES ===== */

/* ==== TYPEDEFS ===== */

typedef enum FRAME_TYPE_en {
    FRAME_TYPE_NOTSET = 0,  /*!< Not set */
    FRAME_TYPE_WRITE,       /*!< Perform a single register write */
    FRAME_TYPE_READ,        /*!< Perform a single register read */
    FRAME_TYPE_MAX,         /*!< Enum boundary max value */
} FRAME_TYPE_t;

/**
 * Groups all necessary information to handle an I2C transaction, all the
 * way to completion.
 */
typedef volatile struct _i2c_handler_st {
    I2C_STATE_t state;          /*!< Driver internal state */
    FRAME_TYPE_t f_type;        /*!< Transfer frame type */
    i2c_transfer_t transfer;    /*!< Transfer parameters */
} i2c_handler_t;

/* ==== GLOBAL VARIABLES ===== */

static i2c_handler_t g_i2c_handler[I2C_INSTANCE_MAX];

/* ==== FUNCTION PROTOTYPES ===== */

static inline void transaction_read_rxfifo(I2C_INSTANCE_t instance);
static inline uint32_t get_i2c_base(const I2C_INSTANCE_t instance);

static void init(const I2C_INSTANCE_t instance);
static I2C_RETURN_t validate_transfer(const I2C_INSTANCE_t instance,
                                      const i2c_transfer_t* const transfer);
static void transaction_write(I2C_INSTANCE_t instance);
static void transaction_read(I2C_INSTANCE_t instance);
static void transaction_done(I2C_INSTANCE_t instance);

/* ==== APIs ===== */

I2C_RETURN_t i2c_init(const I2C_INSTANCE_t instance) {
    I2C_RETURN_t rc = I2C_RETURN_SUCCESS;

    if (instance >= I2C_INSTANCE_MAX) {
        rc = I2C_RETURN_INVALID_INSTANCE;
    } else {
        init(instance);
    }

    return rc;
}

I2C_RETURN_t i2c_write(const I2C_INSTANCE_t instance, const i2c_transfer_t *const transfer) {
    I2C_RETURN_t rc = validate_transfer(instance, transfer);

    if (I2C_RETURN_SUCCESS == rc) {
        // copy passed transfer parameters to global structure
        memcpy((void*)&g_i2c_handler[instance].transfer, transfer, sizeof(i2c_transfer_t));
        // Start a 'write' i2c transaction
        transaction_write(instance);
    }

    return rc;
}

I2C_RETURN_t i2c_read(const I2C_INSTANCE_t instance, const i2c_transfer_t *const transfer) {
    I2C_RETURN_t rc = validate_transfer(instance, transfer);

    if (I2C_RETURN_SUCCESS == rc) {
        // copy passed transfer parameters to global structure
        memcpy((void*)&g_i2c_handler[instance].transfer, transfer, sizeof(i2c_transfer_t));
        // Start a 'read' i2c transaction
        transaction_read(instance);
    }

    return rc;
}

I2C_RETURN_t i2c_get_status(const I2C_INSTANCE_t instance, I2C_STATE_t* state) {
    I2C_RETURN_t rc = I2C_RETURN_SUCCESS;

    if (NULL == state) {
        rc =  I2C_RETURN_NULL;
    } else {
        *state = g_i2c_handler[instance].state;
    }

    return rc;
}

/* ==== STATIC FUNCTIONS ===== */

/* ==== Inline Functions ===== */

/**
 * Utility function to convert between instance enum and I2C base.
 *
 * @note This relies on instance being checked as valid before this
 * function is called.
 *
 * @param instance The I2C Hardware Instance to convert
 * @returns The relevant I2C register base
 */
static inline uint32_t get_i2c_base(const I2C_INSTANCE_t instance) {
    return ((instance == I2C_INSTANCE_A) ? I2CA_BASE : I2CB_BASE);
}

/**
 * @brief Read RX FIFO
 *
 * @param instance I2C module to be used.
 */
static inline void transaction_read_rxfifo(I2C_INSTANCE_t instance) {
    uint32_t base = get_i2c_base(instance);

    // save transaction data to user's buffer
    for (uint16_t i = 0U; i < g_i2c_handler[instance].transfer.dataSize; i++) {
        g_i2c_handler[instance].transfer.rxBuffer[i] = I2C_getData(base);
    }
}

/* ==== Utility Functions ===== */

/**
 * @brief Initialise internal structure for a given I2C instance.
 * 
 * @param instance I2C module to be used.
 */
static void init(const I2C_INSTANCE_t instance) {
    uint32_t base = get_i2c_base(instance);

    // Just in case, it should have been enabled by syscfg, previously.
    I2C_enableModule(base);
    // initialise handler
    g_i2c_handler[instance].state = I2C_STATE_READY;
    g_i2c_handler[instance].f_type = FRAME_TYPE_NOTSET;
    g_i2c_handler[instance].transfer.txBuffer = NULL;
    g_i2c_handler[instance].transfer.rxBuffer = NULL;
    g_i2c_handler[instance].transfer.callback = NULL;
}

/**
 * @brief Performs a set of checks to validate the transfer parameters.
 * 
 * @param instance I2C module to be used.
 * @param transfer Structure containing in/out transfer parameters.
 * @return I2C_RETURN_t indicating success or error.
 */
static I2C_RETURN_t validate_transfer(const I2C_INSTANCE_t instance,
                                      const i2c_transfer_t* const transfer) {
    I2C_RETURN_t rc = I2C_RETURN_SUCCESS;
    uint32_t base = get_i2c_base(instance);

    if (instance >= I2C_INSTANCE_MAX) {
        rc = I2C_RETURN_INVALID_INSTANCE;
    }
    if (I2C_RETURN_SUCCESS == rc) {
        if (NULL == transfer) {
            rc = I2C_RETURN_NULL;
        }
    }
    if (I2C_RETURN_SUCCESS == rc) {
        if ((NULL == transfer->txBuffer) || (NULL == transfer->rxBuffer)) {
            rc = I2C_RETURN_NULL;
        }
    }
    if (I2C_RETURN_SUCCESS == rc) {
        if ((true == I2C_isBusBusy(base)) || (I2C_STATE_BUSY == g_i2c_handler[instance].state)) {
            rc = I2C_RETURN_BUSY;
        }
    }
    if (I2C_RETURN_SUCCESS == rc) {
        if ((transfer->dataSize > I2C_MAX_DATA_SIZE) || (transfer->dataSize < I2C_MIN_DATA_SIZE)) {
            rc = I2C_RETURN_INVALID_SIZE;
        }
    }
    if (I2C_RETURN_SUCCESS == rc) {
        if (I2C_STATE_NOT_INIT == g_i2c_handler[instance].state) {
            rc = I2C_RETURN_NOT_INIT;
        }
    }

    return rc;
}

/**
 * @brief Send write transaction to the I2C bus.
 * 
 * @param instance I2C module to be used.
 */
static void transaction_write(I2C_INSTANCE_t instance) {
    uint32_t base = get_i2c_base(instance);

    // update driver's internal state
    g_i2c_handler[instance].state = I2C_STATE_BUSY;
    g_i2c_handler[instance].f_type = FRAME_TYPE_WRITE;

    // requires master-transmitter mode
    I2C_setConfig(base, I2C_CONTROLLER_SEND_MODE);
    I2C_setTargetAddress(base, g_i2c_handler[instance].transfer.targetAddr);
    I2C_setDataCount(base, g_i2c_handler[instance].transfer.dataSize);
    I2C_enableFIFO(base);

    // load transaction payload
    for (uint16_t i = 0U; i < g_i2c_handler[instance].transfer.dataSize; i++) {
        I2C_putData(base, g_i2c_handler[instance].transfer.txBuffer[i]);
    }

    // Set interrupts and start transaction
    I2C_setFIFOInterruptLevel(base, I2C_FIFO_TXEMPTY, I2C_FIFO_RXFULL);
    I2C_clearInterruptStatus(base, I2C_INT_ARB_LOST | I2C_INT_NO_ACK | I2C_INT_TXFF);
    I2C_enableInterrupt(base, I2C_INT_ARB_LOST | I2C_INT_NO_ACK | I2C_INT_TXFF);
    I2C_sendStartCondition(base);
}

/**
 * @brief Send read transaction to the I2C bus.
 * 
 * @param instance I2C module to be used.
 */
static void transaction_read(I2C_INSTANCE_t instance) {
    uint32_t base = get_i2c_base(instance);

    // update driver's internal state
    g_i2c_handler[instance].state = I2C_STATE_BUSY;
    g_i2c_handler[instance].f_type = FRAME_TYPE_READ;

    // master-receiver mode
    I2C_setConfig(base, I2C_CONTROLLER_RECEIVE_MODE);
    I2C_setTargetAddress(base, g_i2c_handler[instance].transfer.targetAddr);
    I2C_setDataCount(base, g_i2c_handler[instance].transfer.dataSize);
    I2C_enableFIFO(base);

    // Set interrupts and start transaction
    I2C_setFIFOInterruptLevel(base, I2C_FIFO_TXFULL,
              (I2C_RxFIFOLevel)(g_i2c_handler[instance].transfer.dataSize));
    I2C_clearInterruptStatus(base, I2C_INT_ARB_LOST | I2C_INT_NO_ACK | I2C_INT_RXFF);
    I2C_enableInterrupt(base, I2C_INT_ARB_LOST | I2C_INT_NO_ACK | I2C_INT_RXFF);
    I2C_sendStartCondition(base);
}

/**
 * @brief Finish the I2C transaction, reset driver's internal state, and call
 * the callback function if non-null.
 * 
 * @param instance I2C module to be used.
 */
static void transaction_done(I2C_INSTANCE_t instance) {
    uint32_t base = get_i2c_base(instance);

    I2C_sendStopCondition(base);
    I2C_disableFIFO(base);
    g_i2c_handler[instance].state = I2C_STATE_READY;
    g_i2c_handler[instance].f_type = FRAME_TYPE_NOTSET;
    if (NULL != g_i2c_handler[instance].transfer.callback) {
        g_i2c_handler[instance].transfer.callback(true);
    }
}

/* ==== Interrupt Routines ===== */

/**
 * @note INT_i2c0_uc_FIFO_ISR() is hard-linked to I2CA_BASE
 */
__interrupt void INT_i2c0_uc_FIFO_ISR(void) {
    // exclude any flag that's not part of this ISR
    uint32_t flags = I2C_getInterruptStatus(I2CA_BASE) & ~(uint32_t)I2C_STR_INTMASK;

    if (flags & I2C_INT_TXFF) {
        // needs to be disabled or it keeps firing up [ trigger: TXFIFO <= 0 ]
        I2C_disableInterrupt(I2CA_BASE, I2C_INT_TXFF);
        I2C_clearInterruptStatus(I2CA_BASE, I2C_INT_TXFF);

        if (FRAME_TYPE_WRITE == g_i2c_handler[I2C_INSTANCE_A].f_type) {
            transaction_done(I2C_INSTANCE_A);
        }
    }
    // we cannot use if/else, as I2C_INT_TXFF and I2C_INT_RXFF are not
    // mutually exclusive
    if (flags & I2C_INT_RXFF) {
        // needs to be disabled, or it keeps firing up [ trigger: RXFIFO >=
        // I2C_setDataCount ] */
        I2C_disableInterrupt(I2CA_BASE, I2C_INT_RXFF);
        I2C_clearInterruptStatus(I2CA_BASE, I2C_INT_RXFF);

        if (FRAME_TYPE_READ == g_i2c_handler[I2C_INSTANCE_A].f_type) {
            transaction_read_rxfifo(I2C_INSTANCE_A);
            transaction_done(I2C_INSTANCE_A);
        }
    }

    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP8);
}

/**
 * @note INT_i2c0_uc_ISR() is hard-linked to I2CA_BASE
 */
__interrupt void INT_i2c0_uc_ISR(void) {
    // exclude any flag that's not part of this ISR
    uint32_t flags = I2C_getInterruptStatus(I2CA_BASE) & (uint32_t)I2C_STR_INTMASK;

    I2C_sendStopCondition(I2CA_BASE);
    I2C_disableFIFO(I2CA_BASE);

    // reset the frame type, as this ISR is fired when there's anything else
    // left to do
    g_i2c_handler[I2C_INSTANCE_A].f_type = FRAME_TYPE_NOTSET;

    if (flags & I2C_INT_NO_ACK) {
        g_i2c_handler[I2C_INSTANCE_A].state = I2C_STATE_NO_ACK;
        flags = I2C_INT_NO_ACK;
    } else if (flags & I2C_INT_ARB_LOST) {
        g_i2c_handler[I2C_INSTANCE_A].state = I2C_STATE_ARB_LOST;
        flags = I2C_INT_ARB_LOST;
    } else {
        // unexpected interrupt, better reset the internal state.
        // 'flags' isn't changed here, as we don't know the interrupt source[s]
        g_i2c_handler[I2C_INSTANCE_A].state = I2C_STATE_READY;
    }
    // user's callback, always false when called from this ISR
    if (NULL != g_i2c_handler[I2C_INSTANCE_A].transfer.callback) {
        g_i2c_handler[I2C_INSTANCE_A].transfer.callback(false);
    }

    I2C_clearInterruptStatus(I2CA_BASE, flags);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP8);
}
