/**
 * @file dsp1_spi.h
 * @author Adam Lofthouse-Hill (Enoda Ltd.)
 * @brief DMA-Driven SPI driver to work with the DSP C28x core in an
 * RTOS Context.
 * @date 2023-11-16
 *
 * @copyright Copyright (C) 2023 Enoda Ltd. - All Rights Reserved
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

#ifndef DSP_SPI_H
#define DSP_SPI_H

/* ==== INCLUDES ===== */

#include <stdbool.h>
#include <stdint.h>

/* ==== DEFINES ===== */

/** The maximum number of 16-bit words that can be transferred in on call */
#define SPI_MAX_TRANSFER (0xFFFFu)

/** Specifies what data should be transmitted on the Tx line for Rx-only transfers */
#define SPI_DUMMY_DATA (0xFFFFu)

/* ==== TYPEDEFS ===== */

/**
 * The different return values for each SPI API call.
 */
typedef enum SPI_RETURN_en {
    SPI_RETURN_SUCCESS,          /*!< API call successful */
    SPI_RETURN_NULL,             /*!< NULL pointer passed */
    SPI_RETURN_NOT_INIT,         /*!< Driver not yet initialised */
    SPI_RETURN_ALREADY_INIT,     /*!< Driver is already initialised */
    SPI_RETURN_BUSY,             /*!< Driver busy with another transfer */
    SPI_RETURN_INVALID_SIZE,     /*!< Invalid size parameter passed */
    SPI_RETURN_INVALID_PIN,      /*!< Invalid GPIO pin passed */
    SPI_RETURN_INVALID_INSTANCE, /*!< Invalid instance parameter passed */
    SPI_RETURN_DEVICE_ERROR,     /*!< Unknown internal SPI device error */
    SPI_RETURN_RTOS_ERROR,       /*!< Internal FreeRTOS error */
    SPI_RETURN_MAX,              /*!< Enum boundary max value */
} SPI_RETURN_t;

/**
 * Describes all possible states the driver can be in.
 */
typedef enum SPI_STATE_en {
    SPI_STATE_READY, /*!< Driver is initialised, and read for a transfer */
    SPI_STATE_BUSY,  /*!< Driver is busy with an ongoing transfer */
    SPI_STATE_MAX,   /*!< Enum boundary max value */
} SPI_STATE_t;

/**
 * Lists all possible SPI Hardware Instances
 */
typedef enum SPI_INSTANCE_en {
    SPI_INSTANCE_A,   /*!< SPIA Hardware Instance */
    SPI_INSTANCE_B,   /*!< SPIB Hardware Instance */
    SPI_INSTANCE_MAX, /*!< Enum boundary max value */
} SPI_INSTANCE_t;

/**
 * Defines a SPI unit word type
 * @note The minimum DMA word unit is 16-bits. Therefore, the minimum
 * SPI word unit (and hence transfer size) is 16-bits.
 */
typedef uint16_t spi_word_unit_t;

/** Defines the user-provided callback. Called when transfer finished.
 * Boolean value represents transfer success (true ==> success).
 */
typedef void (*functionCallback_t)(bool);

/**
 * Chip Select (CS) configuration for a SPI transfer.
 *
 * @note The PinMux configuration is expected to be done outside the
 * driver and within SysConfig.
 */
typedef struct spi_cs_st {
    /**
     * GPIO pin to use as the chip select in the transfer.
     */
    const uint32_t pin;

    /**
     * If true, CS line is active low for transfer.
     * If false, line is active high.
     */
    const bool active_low;

    /** If true, keep CS line active/asserted after transfer has finished.
     * This can be used to chain multiple transactions together, before setting
     * this to false on the final transaction.
     *
     * @note There will be a performance overhead to chaining multiple transactions
     * together instead of performing a single, DMA-driven transfer.
    */
    const bool keep_active;
} spi_cs_t;

/**
 * Configure a single SPI transfer, whether simplex Tx/Rx or duplex.
 *
 * @note All provided buffers should be 16-bit aligned and in globally
 * shared RAM so it is accessible by the DMA.
 */
typedef struct spi_transfer_st {
    /** Data to transfer. Ignored for receive only. */
    const spi_word_unit_t *const tx_buf;

    /** Buffer for received data. Ignored for transfer only. */
    spi_word_unit_t *const rx_buf;

    /** Size of the whole transfer in 16-bit bytes. */
    const uint16_t size;

    /**
     * User-defined callback to call on transfer completion or error.
     * Callback is optional. Set to NULL for no callback.
     */
    const functionCallback_t callback;

    /** Chip-Select (CS) configuration for the transfer */
    const spi_cs_t cs;
} spi_transfer_t;

/* ==== APIS ===== */

/**
 * @brief Initialises a SPI Hardware Instance for use with the driver.
 *
 * SysConfig is used for a lot of the one-off initialisation and
 * configuration of the driver to utilise their peripheral instance
 * and interrupt management which helps avoid conflicts. As such,
 * `Board_init()` is required to be called before this function.
 *
 * SysConfig should also be used to adjust baud rate as well as clock phase
 * and clock polarity.
 *
 * @note Although the SPI driver as a whole is expected to be RTOS-compliant,
 * and thread safe, the initialisation function is NOT thread-safe. As such
 * this should only be called from a single task, or better still before.
 *
 * @note The PinMux configuration of the PICO, POCI, and CLK signals is
 * expected to be done by SysConfig outside this driver.
 *
 * @param instance SPI Hardware Instance to initialise
 * @returns SPI_RETURN_SUCCESS on success, otherwise related error.
 */
SPI_RETURN_t spi_init(const SPI_INSTANCE_t instance);

/**
 * @brief Send a transfer in the Tx (PICO) only direction.
 *
 * @param instance Hardware Instance to use for the transfer
 * @param transfer The transfer buffer, size and callback configuration
 * @returns SPI_RETURN_SUCCESS on success, otherwise related error.
 */
SPI_RETURN_t spi_transmit(const SPI_INSTANCE_t instance, const spi_transfer_t *const transfer);

/**
 * @brief Receive data in the Rx (POCI) direction only.
 *
 * @param instance Hardware Instance to use for the transfer
 * @param transfer The transfer buffer, size and callback configuration
 * @returns SPI_RETURN_SUCCESS on success, otherwise related error.
 */
SPI_RETURN_t spi_receive(const SPI_INSTANCE_t instance, const spi_transfer_t *const transfer);

/**
 * @brief Send/receive data simultaneously in a duplex transfer.
 *
 * @note If there is a subsection of the transfer where data is
 * only needed in one direction, then the relevant buffer should
 * be filled with dummy data or ignored.
 *
 * @note The size parameter in transfer indicates the length of
 * the transfer, not the combined size of both Tx data sent and
 * Rx data received.
 *
 * @param instance Hardware Instance to use for the transfer
 * @param transfer The transfer buffer, size and callback configuration
 * @returns SPI_RETURN_SUCCESS on success, otherwise related error.
 */
SPI_RETURN_t spi_transceive(const SPI_INSTANCE_t instance, const spi_transfer_t *const transfer);

/**
 * @brief Deinitialise the driver for the given instance, returning
 * all registers to default-on-reset values and disabling any enabled
 * interrupts.
 *
 * @param instance Hardware Instance to deinitialise.
 * @returns SPI_RETURN_SUCCESS on success, otherwise related error.
 */
SPI_RETURN_t spi_deinit(const SPI_INSTANCE_t instance);

/**
 * @brief Gets the current status of the driver for the instance.
 *
 * @param instance Hardware Instance to query
 * @param state Pointer which will be populated by status on success.
 * @returns SPI_RETURN_SUCCESS on success, otherwise related error.
 */
SPI_RETURN_t spi_status(const SPI_INSTANCE_t instance, SPI_STATE_t *const state);

#endif /* DSP_SPI_H */
