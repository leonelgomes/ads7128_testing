/**
 * @file
 * The alarm module header
 *
 * @date 09 May, 2023
 * @author Shoujie Li <Shoujie.Li@enodatech.com>
 * @copyright Copyright (c) 2023 ENODA - https://enodatech.com/
 * All rights reserved.
 */

#ifndef DSP1_ALARM_H
#define DSP1_ALARM_H

#include "prime_iam.pb.h"

typedef struct alarm_pattern_st {

    /** Buzzer pattern of start, middle and end */
    struct {
        /**< Signal frequency in Hz */
        uint16_t frequency;
        /**< The pwm period numbers of duration */
        uint16_t duration;
    } pattern [3];

    /**< A higher value indicates a higher priority and the higher one will
     * overwrite the lower alarm. The alarm with a priority than 200 will last
     * forever.
     */
    uint16_t priority;

    bool is_persistent;
} alarm_pattern_t;

typedef struct alarm_st {
    /** The alarm state of the UC has an associated buzzer pattern */
    enoda_prime_iam_AlarmState state;
    bool configured;
    uint16_t count;
    uint16_t stage_012;
} alarm_t;

void alarm_transit(enoda_prime_iam_AlarmState state);
enoda_prime_iam_AlarmState alarm_state();

void alarm_proc();

#endif
