/**
 * @file
 * Implementation of configuring and reading of ADS7128 measurement
 *
 * @date 09 June, 2023
 * @author Shoujie Li <Shoujie.Li@enodatech.com>
 * @copyright Copyright (c) 2023 ENODA - https://enodatech.com/
 * All rights reserved.
 */
#include <stdio.h>
#include <stdbool.h>
#include "board.h"
#include "i2c.h"

//#include "prime_iam.pb.h"
//#include "uc_analog.h"
//#include "dsp1_ipc.h"
#include "app.h"

#include "ads7128.h"

/** The following MACROs defines the ADS7128 I2C opcode */
#define ADS7128_OPCODE_SR (0x10)
#define ADS7128_OPCODE_SW (0x08)
#define ADS7128_OPCODE_SB (0x18)
#define ADS7128_OPCODE_CB (0x20)
#define ADS7128_OPCODE_RC (0x30)
#define ADS7128_OPCODE_WC (0x28)

#define RECENT_START_ADDRESS (0xA0)
#define ADS7128_RECENT_LEN (0xF)
#define RMS_START_ADDRESS (0xC1)
#define ADS7128_RMS_LEN (0x2)

typedef struct ads7128_config_st
{
    uint16_t address;
    uint16_t value;
} ads7128_config_t;

/** To verify the configuration register order during test */
const ads7128_config_t k_ADS7128_CONFIG[] =
{
    /** Enable RMS, disable CRC, all analog */
    {.address = 0x01, .value = 0x84},
    /** OSR is set to 128 */
    {.address = 0x03, .value = 0x07},
    /** Autonomous conversion with high speed oscillator, 166kSPS */
    {.address = 0x04, .value = 0x25},
    /** All analog */
    {.address = 0x05, .value = 0x00},
    /** Autonomous sequencing starts */
    {.address = 0x10, .value = 0x11},
    /** RMS 1024 for Channel 0 */
    {.address = 0xC0, .value = 0x00},
};

/** The ADS7128 I2C read transaction is defined in this structure */
typedef struct ads7128_transaction_st
{
    /**< The system tick when a request issues */
    uint32_t tick;

    /**< How many bytes to read */
    uint32_t nbytes;

    /** The ADS7128 access state */
    ADS_STATE_t state;

    enoda_prime_iam_AlarmState alarm_state;
} ads7128_transaction_t;

const uint32_t k_TRANSACTION_BYTES[ADS7128_STATE_RESERVED] =
{
    0,
    ADS7128_RECENT_LEN,
    ADS7128_RMS_LEN,
    ADS7128_RECENT_LEN,
    ADS7128_RMS_LEN,
    0,
};

ads7128_transaction_t g_ads7128_transaction =
{
    .tick = 0,
    .nbytes = 0,
    .state = CONFIG_ADS7128_INIT,
    .alarm_state = enoda_prime_iam_AlarmState_ALARM_IDLE
};

static void clear_bb(uint32_t base)
{
    I2C_disableModule(i2c0_uc_BASE);
    I2C_setConfig(i2c0_uc_BASE, I2C_CONTROLLER_SEND_MODE);
    I2C_enableModule(i2c0_uc_BASE);
    DEVICE_DELAY_US(BUSY_DELAY_50us);
}

/**
 * @fn Make sure I2C bus is free
 */
bool bus_free(uint32_t base)
{
    for (int i = 0; i < GENRERAL_TRIES; ++i)
    {
        if (!I2C_isBusBusy(base)) return true;

        clear_bb(base);
    }

    return false;
}

/**
 * @fn Check whether the last I2C message is acknowledged
 *
 * TRM 33.7.2.3 I2CSTR
 */
bool is_acked(uint32_t base)
{
    uint16_t status = I2C_getStatus(base);

    if ( (status & I2C_STS_NO_ACK) == I2C_STS_NO_ACK )
    {
        return false;
    }
    else
    {
        /** 0h (R/W) = ACK received/NACK not received */
        return true;
    }
}

/**
 * @fn Check whether I2C is free
 */
bool is_bus_free(uint32_t base)
{
    if(I2C_isBusBusy(base))
    {
        return false;
    }

    /** 0h (R/W) = STP is automatically cleared after the STOP condition has been generated */
    if(I2C_getStopConditionStatus(base))
    {
        return false;
    }

    return true;
}

/**
 * @fn Reconfigure I2C module for transmission
 *
 * @brief After the reconfiguration, the I2C bus should be free [ not busy ]
 *
 * @ret true    I2C bus free - success
 *      false   I2C bus busy - fail
 */
bool i2c_txinit(uint16_t addr, I2C_TxFIFOLevel txLevel, I2C_RxFIFOLevel rxLevel)
{
    bool flag = false;
    for (int i = 0; i < GENRERAL_TRIES; ++i)
    {
        I2C_disableModule(i2c0_uc_BASE);
        I2C_initController(i2c0_uc_BASE, DEVICE_SYSCLK_FREQ, 400000, I2C_DUTYCYCLE_33);
        I2C_setConfig(i2c0_uc_BASE, I2C_CONTROLLER_SEND_MODE | I2C_REPEAT_MODE);
        I2C_setTargetAddress(i2c0_uc_BASE, addr);
        I2C_disableLoopback(i2c0_uc_BASE);
        I2C_setBitCount(i2c0_uc_BASE, I2C_BITCOUNT_8);
        I2C_setAddressMode(i2c0_uc_BASE, I2C_ADDR_MODE_7BITS);
        I2C_enableFIFO(i2c0_uc_BASE);
        I2C_clearInterruptStatus(i2c0_uc_BASE, I2C_INT_ARB_LOST | I2C_INT_NO_ACK | I2C_INT_RXFF | I2C_INT_TXFF);
        I2C_setFIFOInterruptLevel(i2c0_uc_BASE, txLevel, rxLevel);
        I2C_enableInterrupt(i2c0_uc_BASE, I2C_INT_ARB_LOST | I2C_INT_NO_ACK | I2C_INT_RXFF | I2C_INT_TXFF);
        I2C_setEmulationMode(i2c0_uc_BASE, I2C_EMULATION_FREE_RUN);
        I2C_enableModule(i2c0_uc_BASE);

        if (!I2C_isBusBusy(i2c0_uc_BASE))
        {
            flag = true;
            break;
        }
        else
        {
            // shouldn't get here...
            ESTOP0;
        }
    }

    return flag;
}

/**
 * @fn Initialize the I2C component
 *
 * The configuration steps are:
 * - Reset chip
 * - Force all channels to be analog input
 * - Disable HS mode (Not supported by F2838xD)
 * - Enable CRC (Test from no CRC), CRC-8-CCITT polynomial (x8 + x2 + x + 1)
 * - Configure OSR to the highest one in OSR_CFG, 111b = 128 samples
 * - Configure OSC_SEL = 0 and CLK_DIV 0110b in OPMODE_CFG,
 * - Configure AUTO_SEQ_CH_SEL for Auto-Sequence mode
 * - Enable autonomous mode
 * - Enable Auto-Sequence Mode
 * - Configure DC link RMS in RMS_CFG
 * -
 * @note
 * To write to a register in ADS7128, I2C_setDataCount need 2:
 * - The 0th is the register address
 * - The 1st is the register value
 */
bool ads7128_init(uint16_t addr)
{
    bool flag = false;

    int num = sizeof(k_ADS7128_CONFIG)/sizeof(ads7128_config_t);
    for (int i = 0; i < num; ++i)
    {
        flag = false;

        /** Try several times */
        for (int j = 0; j < GENRERAL_TRIES; ++j)
        {
            /** Configure ADS7128 */
            if (!i2c_txinit(addr, I2C_FIFO_TX0, I2C_FIFO_RX1)) continue;
            I2C_putData(i2c0_uc_BASE, ADS7128_OPCODE_SW);
            I2C_putData(i2c0_uc_BASE, k_ADS7128_CONFIG[i].address);
            I2C_putData(i2c0_uc_BASE, k_ADS7128_CONFIG[i].value);
            I2C_sendStartCondition(i2c0_uc_BASE);
            DEVICE_DELAY_US(BUSY_DELAY_100us);
            I2C_sendStopCondition(i2c0_uc_BASE);
            DEVICE_DELAY_US(BUSY_DELAY_1ms);

            /** Read the configuration back and check */
            if (!i2c_txinit(addr, I2C_FIFO_TX0, I2C_FIFO_RX1)) continue;
            I2C_putData(i2c0_uc_BASE, ADS7128_OPCODE_SR);
            I2C_putData(i2c0_uc_BASE, k_ADS7128_CONFIG[i].address);
            I2C_sendStartCondition(i2c0_uc_BASE);
            DEVICE_DELAY_US(BUSY_DELAY_100us);
            I2C_sendStopCondition(i2c0_uc_BASE);
            DEVICE_DELAY_US(BUSY_DELAY_1ms);

            I2C_setConfig(i2c0_uc_BASE, I2C_CONTROLLER_RECEIVE_MODE | I2C_REPEAT_MODE);
            I2C_sendStartCondition(i2c0_uc_BASE);
            DEVICE_DELAY_US(BUSY_DELAY_100us);
            I2C_sendStopCondition(i2c0_uc_BASE);
            DEVICE_DELAY_US(BUSY_DELAY_1ms);

            uint16_t data = I2C_getData(i2c0_uc_BASE);
            if (data == k_ADS7128_CONFIG[i].value)
            {
                flag = true;
                break;
            }
        }

        if (flag == false) break;
    }

    return flag;
}

/**
 * @fn Reset ADS7128
 */
void ads7128_reset(uint16_t addr)
{
    if (!i2c_txinit(addr, I2C_FIFO_TX0, I2C_FIFO_RX1)) return;

    I2C_putData(i2c0_uc_BASE, ADS7128_OPCODE_SW);
    /** Put ADS7128 register address */
    I2C_putData(i2c0_uc_BASE, 0x01);
    /** Put ADS7128 register value */
    I2C_putData(i2c0_uc_BASE, 0x01);
    I2C_sendStartCondition(i2c0_uc_BASE);
    DEVICE_DELAY_US(BUSY_DELAY_100us);
    I2C_sendStopCondition(i2c0_uc_BASE);
    DEVICE_DELAY_US(BUSY_DELAY_100us);
}

void ads7128_transaction(ADS_STATE_t state)
{
    g_ads7128_transaction.nbytes = k_TRANSACTION_BYTES[state];
    g_ads7128_transaction.tick = g_dsp1.tick_sys;
    g_ads7128_transaction.state = state;
}

void ads7128_data_read_req(uint16_t addr)
{
    uint32_t num = k_TRANSACTION_BYTES[g_ads7128_transaction.state];

    I2C_setTargetAddress(i2c0_uc_BASE, addr);
    I2C_setDataCount(i2c0_uc_BASE, 2);
    I2C_setFIFOInterruptLevel(i2c0_uc_BASE, I2C_FIFO_TX1, (I2C_RxFIFOLevel)(num));

    I2C_putData(i2c0_uc_BASE, ADS7128_OPCODE_RC);
    I2C_putData(i2c0_uc_BASE, RECENT_START_ADDRESS);
    I2C_setConfig(i2c0_uc_BASE, I2C_CONTROLLER_SEND_MODE);
    I2C_sendStartCondition(i2c0_uc_BASE);
    I2C_sendStopCondition(i2c0_uc_BASE);
}

void ads7128_rms_read_req(uint16_t addr)
{
    uint32_t num = k_TRANSACTION_BYTES[g_ads7128_transaction.state];

    I2C_setFIFOInterruptLevel(i2c0_uc_BASE, I2C_FIFO_TX1, (I2C_RxFIFOLevel)(num));
    I2C_setTargetAddress(i2c0_uc_BASE, addr);
    I2C_setDataCount(i2c0_uc_BASE, 2);

    I2C_putData(i2c0_uc_BASE, ADS7128_OPCODE_RC);
    I2C_putData(i2c0_uc_BASE, RMS_START_ADDRESS);
    I2C_setConfig(i2c0_uc_BASE, I2C_CONTROLLER_SEND_MODE);
    I2C_sendStartCondition(i2c0_uc_BASE);
    I2C_sendStopCondition(i2c0_uc_BASE);
}

bool ads7128_init_state()
{
    bool iflag = ads7128_init(INTERNAL_MEASUREMENT_I2CADDR);
//    bool eflag = ads7128_init(EXTERNAL_MEASUREMENT_I2CADDR);
    bool eflag = true;

    if (iflag && eflag)
    {
        // @todo
        //ads7128_transaction(READ_ADS7128_INTERNAL_CHANNEL);
        //ads7128_data_read_req(INTERNAL_MEASUREMENT_I2CADDR);
    }

    printf("iflag[0x%x]: %s\r\n", INTERNAL_MEASUREMENT_I2CADDR, iflag ? "OK" : "ERROR");
    printf("eflag[0x%x]: %s\r\n", EXTERNAL_MEASUREMENT_I2CADDR, eflag ? "OK" : "ERROR");
    fflush(stdout);

    return (iflag && eflag);
}

static void read_measurement()
{
    /** Get the FIFO reading number */
    int16_t num = I2C_getRxFIFOStatus(i2c0_uc_BASE);
    if (num != g_ads7128_transaction.nbytes)
    {
        ads7128_transaction(ADS7128_STATE_RESERVED);
        return;
    }

    /** Read data */
    for(int i = 0; i < num; ++i)
    {
        uint16_t data = I2C_getData(i2c0_uc_BASE);

        // Put it into cache
    }

    if (g_ads7128_transaction.state == READ_ADS7128_INTERNAL_CHANNEL)
    {
        /** Read internal RMS measurement */
        ads7128_transaction(READ_ADS7128_INTERNAL_RMS);
        //ads7128_config_read_req(INTERNAL_MEASUREMENT_I2CADDR);
    }
    else if (g_ads7128_transaction.state == READ_ADS7128_INTERNAL_RMS)
    {
        /** Read external data measurement */
        g_ads7128_transaction.state = READ_ADS7128_EXTERNAL_CHANNEL;
        //ads7128_config_read_req(EXTERNAL_MEASUREMENT_I2CADDR);
    }
    else if (g_ads7128_transaction.state == READ_ADS7128_EXTERNAL_CHANNEL)
    {
        /** Read external RMS */
        ads7128_transaction(READ_ADS7128_EXTERNAL_RMS);
//        ads7128_config_read_req(INTERNAL_MEASUREMENT_I2CADDR);
    }
    else if (g_ads7128_transaction.state == READ_ADS7128_EXTERNAL_RMS)
    {
        /** ADS7128 state transit to the measurement reading */
        g_ads7128_transaction.state = READ_ADS7128_INTERNAL_CHANNEL;

        // The internal measurement request is issued by 1ms interrupt
    }
}

/**
 * @fn Request to read all measurement
 *
 * The function is called in the 1ms interrupt
 */
void ads7128_read_start()
{
    if (g_ads7128_transaction.state == CONFIG_ADS7128_IDLE)
    {
        ads7128_transaction(READ_ADS7128_INTERNAL_CHANNEL);
        //ads7128_config_read_req(INTERNAL_MEASUREMENT_I2CADDR);
    }
}

/** Just a dummy method */
void ads7128_dummy()
{
//    ESTOP0;
}

static void (*itr_state_cb[ADS7128_STATE_RESERVED])() =
{
    ads7128_dummy,
    read_measurement,
    read_measurement,
    read_measurement,
    read_measurement,
    ads7128_dummy
};

void ads7128_proc()
{
    if (g_ads7128_transaction.state == CONFIG_ADS7128_INIT)
    {
        ads7128_init_state();
        g_ads7128_transaction.state = READ_ADS7128_INTERNAL_CHANNEL;
    }
    else if (g_ads7128_transaction.state == ADS7128_STATE_RESERVED)
    {
        ads7128_reset(INTERNAL_MEASUREMENT_I2CADDR);
        ads7128_reset(EXTERNAL_MEASUREMENT_I2CADDR);
        ads7128_transaction(CONFIG_ADS7128_INIT);
    }
    else
    {
        /** nothing to do */
    }
}

/** @fn I2C FIFO interrupt */
 __interrupt void INT_i2c0_uc_FIFO_ISR(void)
{
#if 1
     uint32_t flags = I2C_getInterruptStatus(i2c0_uc_BASE);
    //
    // If receive FIFO interrupt flag is set, read data
    //
    if( (flags & I2C_INT_TXFF) != 0 )
    {
        I2C_clearInterruptStatus(i2c0_uc_BASE, I2C_INT_TXFF);
    }

    if( (flags & I2C_INT_RXFF) != 0 )
    {
        itr_state_cb[g_ads7128_transaction.state]();
        I2C_clearInterruptStatus(i2c0_uc_BASE, I2C_INT_RXFF);
    }

    I2C_clearInterruptStatus(i2c0_uc_BASE, flags);
    Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP8);
#endif
}

 /** @fn I2C interrupt, not used */
 __interrupt void INT_i2c0_uc_ISR(void)
 {
#if 1
     uint32_t flags = I2C_getInterruptStatus(i2c0_uc_BASE);

     if( (flags & I2C_INT_ARB_LOST) != 0 )
     {
         I2C_clearInterruptStatus(i2c0_uc_BASE, I2C_INT_ARB_LOST);
     }

     if( (flags & I2C_INT_NO_ACK) != 0 )
     {
         I2C_sendStopCondition(i2c0_uc_BASE);
         I2C_clearInterruptStatus(i2c0_uc_BASE, I2C_INT_NO_ACK);
     }
     flags &= ~I2C_INT_RXFF;
     I2C_clearInterruptStatus(i2c0_uc_BASE, flags);
     Interrupt_clearACKGroup(INTERRUPT_ACK_GROUP8);
#endif
 }

/*************************************************************************************************/
/**************************** MOCK DATA STRUCTURES/VARIABLES *************************************/
 /*************************************************************************************************/

 dsp1_t g_dsp1 =
 {
     .tick_switch = 0,
     .tick_sps = 0,
     .tick_sys = 0,

     .crc_serving = false,
     .crc_locked = false,

     .grid_frequency = 50,
     .switch_frequency = 20000,

     .channel_calibration =
     {
         {0.0, 1.0}, {0.0, 1.0}, {0.0, 1.0}, {0.0, 1.0}
     },

     .ipc_cm_flags = 0,

     .calibration =
     {
         .state = CALIBRATION_STATE_IDLE,
         .sample_idx = 0,
         .sample_num = 0,
         .sample_cache = NULL,
         .cycle_sine_sum = 0.0,
         .req =
         {
             .channel = enoda_prime_iam_SensorChannel_CHANNEL_CHANNEL_MAX,
             .type = 0,
             .amplitude = 0.0,
         },
     },

     .alarm =
     {
         .state = enoda_prime_iam_AlarmState_ALARM_IDLE,
         .configured = false,
         .count = 0,
         .stage_012 = 0,
     },
     .time_profile_idx = 0,
     .time_profile_50us = {0}
 };

 /*************************************************************************************************/
 /**************************** MOCK DATA STRUCTURES/VARIABLES - END *******************************/
 /*************************************************************************************************/

