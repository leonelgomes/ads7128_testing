/**
 * @file
 * Definitions for DSP1
 *
 * @date 24 Apr, 2023
 * @author Shoujie Li <Shoujie.Li@enodatech.com>
 * @copyright Copyright (c) 2023 ENODA - https://enodatech.com/
 * All rights reserved.
 */

#ifndef DSP1_APP_H
#define DSP1_APP_H

#include "driverlib.h"

#include "prime_iam.pb.h"
#include "uc_analog.h"
#include "alarm.h"

#define SIZE_64  (1<<6)
#define SIZE_128 (1<<7)
#define SIZE_256 (1<<8)
#define SIZE_512 (1<<9)
#define SIZE_1K  (1<<10)
#define SIZE_2K  ((SIZE_1K)<<1)

#define TIMEOUT_1ms (1)
#define TIMEOUT_2ms (2)
#define TIMEOUT_10ms (10)
#define TIMEOUT_100ms (100)

#define BUSY_DELAY_1us  (1)
#define BUSY_DELAY_10us  (10)
#define BUSY_DELAY_20us  (20)
#define BUSY_DELAY_50us  (50)
#define BUSY_DELAY_100us  (100)
#define BUSY_DELAY_1ms   (1000)
#define BUSY_DELAY_5ms   (BUSY_DELAY_1ms*5)
#define BUSY_DELAY_10ms  (BUSY_DELAY_1ms*10)
#define BUSY_DELAY_50ms  (BUSY_DELAY_10ms*10)
#define BUSY_DELAY_100ms (BUSY_DELAY_50ms*2)

//#define GENRERAL_TRIES (5)
#define GENRERAL_TRIES (1)

/** C28 sizeof use 2 bytes as the minimum */
#define SIZEOF_MULTIPLIER (2)

#define SOFTWARE_TIMER_NUM (16)
#define TIME_PROFILE_NUM (16)

#define BGCRC_ENODA_SEED (0xcafecafeUL)

typedef enum CALIBRATION_STATE_en
{
    CALIBRATION_STATE_IDLE = 0,
    CALIBRATION_STATE_CALIBRATING,
    CALIBRATION_STATE_DONE,
    CALIBRATION_STATE_FAILED,

    CALIBRATION_STATE_RVD
} CALIBRATION_STATE_t;

/**
 * @brief
 * The software timer can be used for some time related tasks.
 */
typedef struct software_timer_st
{
    uint32_t start;
    uint32_t period;
    uint32_t repeat;

    void (*cb)(uint32_t repeat_idx);
} software_timer_t;

/**
 * @brief This is the data structure for all DSP1 states.
 */
typedef struct dsp1_st
{
    /**< timer 0 in 50us period for the PWMs 20kHz switch */
    volatile uint32_t tick_switch;

    /**< timer 1 in 3125ns for the ADC measurement trigger */
    volatile uint32_t tick_sps;

    /**< timer 2 in 100us for the system tick */
    volatile uint32_t tick_sys;

    /**< BGCRC is servering if true */
    volatile bool crc_serving;
    volatile bool crc_locked;

    /**< The grid frequency can be 50, 60Hz, or even 400Hz */
    uint16_t grid_frequency;

    /**< The power electronics switching frequency */
    uint16_t switch_frequency;

    /**< The channel calibrations need be kept in flash */
    bool calibrated;
    channel_calibration_t channel_calibration[enoda_prime_iam_SensorChannel_CHANNEL_CHANNEL_MAX];

    switch_cache_t switch_cache;

    report_t report;

    volatile uint32_t ipc_cm_flags;

    struct {
        CALIBRATION_STATE_t state;
        uint16_t sample_idx;
        uint16_t sample_num;
        /**< channel_calibration is aliased for reusing */
        uint16_t* sample_cache;
        float32_t cycle_sine_sum;
        enoda_prime_iam_SensorCalibrateRequest req;
    } calibration;

    alarm_t alarm;

    uint32_t time_profile_idx;
    uint32_t time_profile_50us[TIME_PROFILE_NUM];
    software_timer_t software_timer[SOFTWARE_TIMER_NUM];
} dsp1_t;

extern dsp1_t g_dsp1;

#endif
