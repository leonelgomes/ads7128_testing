/**
 * @file dsp1_ads7128.h
 * @author Leonel Gomes [ Enoda Ltd ]
 * @brief Module interface for the ADS7128 [ 8-channel Analogue-to-Digital converter ] middleware
 * for the C28x DSP core.
 * @date 18/12/2023
 * @see ADS7128 Datasheet (https://www.ti.com/lit/ds/symlink/ads7128.pdf)
 *
 * @copyright Copyright (C) 2023 Enoda Ltd. - All Rights Reserved
 * Unauthorised copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

#ifndef APP_MIDDLEWARE_DSP1_ADS7128_H_
#define APP_MIDDLEWARE_DSP1_ADS7128_H_

/* ==== INCLUDES ===== */

#include <stdint.h>
#include <stdbool.h>

/* ==== MACROS ===== */

/** Invalid reading marker */
#define ADS7128_RMS_BAD_VALUE       (0xFFFFu)

/** Maximum configuration parameters allowed */
#define ADS7128_MAX_CFG_ENTRIES     (10u)

/** Minimum configuration parameters allowed */
#define ADS7128_MIN_CFG_ENTRIES     (1u)

/** Maximum number of channels the ADS7128 can support */
#define ADS7128_MAX_CH              (8u)

/** ADS7128 single register read opcode */
#define ADS7128_OPCODE_SR           (0x0010u)
/** ADS7128 single register write opcode */
#define ADS7128_OPCODE_SW           (0x0008u)
/** ADS7128 set bit opcode */
#define ADS7128_OPCODE_SB           (0x0018u)
/** ADS7128 reading a continuous block of registers opcode */
#define ADS7128_OPCODE_RC           (0x0030u)

/** GENERAL_CFG Register */
#define ADS7128_GEN_CFG_R           (0x0001u)
/** Software reset all registers to default values Flag */
#define GEN_CFG_RST_F               (0x0001u)
/** Calibrate ADC offset Flag */
#define ADS7128_GEN_CFG_CAL_F       (0x0002u)

/** OPMODE_CFG Register */
#define ADS7128_OPMODE_CFG_R        (0x0004u)
/** SEQUENCE_CFG Register */
#define ADS7128_SEQUENCE_CFG_R      (0x0010u)
/** AUTO_SEQ_CH_SEL Register */
#define ADS7128_AUTO_SEQ_CH_SEL_R   (0x0012u)
/** RMS_CFG Register */
#define ADS7128_RMS_CFG_R           (0x00C0u)
/** RMS_LSB Register */
#define ADS7128_RMS_RESULT_LSB_R    (0x00C1u)
/* ==== TYPEDEFS ===== */

/**
 * Describes the different return values for each API call.
 */
typedef enum ADS7128_RETURN_en {
    ADS7128_RETURN_SUCCESS = 0u,    /*!< API was successful */
    ADS7128_RETURN_NULL,            /*!< Null pointer provided */
    ADS7128_RETURN_NOT_INIT,        /*!< Module hasn't been initialised yet */
    ADS7128_RETURN_BUSY,            /*!< Module is busy, not ready for the operation */
    ADS7128_RETURN_BAD_PARAM,       /*!< Invalid parameter provided */
    ADS7128_RETURN_ALREADY_INIT,    /*!< Module has already been initialised */
    ADS7128_RETURN_INTERNAL_ERROR,  /*!< Unknown internal error */
    ADS7128_RETURN_MAX,             /*!< Enum boundary max value */
} ADS7128_RETURN_t;

/**
 * Describes the internal state of the module.
 */
typedef enum ADS7128_STATE_en {
    ADS7128_STATE_UNINIT = 0u,  /*!< Module not yet initialised */
    ADS7128_STATE_READY,        /*!< Module is initialised and ready [ but not running ] */
    ADS7128_STATE_RUNNING,      /*!< Module is running [ reading the ADC channels ] */
    ADS7128_STATE_BUSY,         /*!< Module is busy processing ongoing task */
    ADS7128_STATE_MAX,          /*!< Enum boundary max value */
} ADS7128_STATE_t;

/**
 * Defines the user-provided callback function that will be called when a
 * sampling cycle has been completed.
 */
typedef void (*functionCallback_t)(void);

/**
 * Describes the ADS7128 internal configuration register/value pair footprint.
 */
typedef struct ads7128_reg_st {
    /** Register address */
    uint16_t addr;
    /** Register value */
    uint16_t value;
} ads7128_reg_t;

/* ==== PUBLIC APIs ===== */

/**
 * @brief This API intialises the underlying I2C driver and the ADS7128 internal
 * registers. If no configuration is passed, the module will use defaults. It also
 * allows the user to register a callback function that will be called whenever a
 * sampling cycle is completed.
 * 
 * The initialisation is non-blocking, and performed in three steps: first, by
 * writing to the ADS internal registers, second, by reading those registers back
 * from the ADS - If the comparison fails, the initialisation fails and the status
 * of the module is set to 'ADS7128_STATE_UNINIT'. Thus, the user should poll the
 * module's internal status by calling 'ads7128_get_status()'. The third and final
 * step, is the calibration of all channels, this step won't change the module status.
 *
 * @param ads7128_cfg Array of configuration parameters [ optional ], if NULL
 * defaults will be used.
 * @param arr_size Number of configuration parameters.
 * @param  i2c_addr I2C address of the ADS7128 device.
 * @param callback User-defined callback function to be called at the end of a
 * reading cycle [ Optional, NULL for no callback ].
 * @return ADS7128_RETURN_SUCCESS on success, otherwise related error.
 */
ADS7128_RETURN_t ads7128_init(const ads7128_reg_t ads7128_cfg[],
                    const uint16_t arr_size,
                    const uint16_t i2c_addr,
                    const functionCallback_t callback);

/**
 * @brief Release any acquired resources and sets the internal state of the
 * module to its default.
 * 
 * @note This API always returns 'ADS7128_RETURN_SUCCESS', as it always
 * resets its internal states and frees up allocated resources, if applicable.
 *
 * @return ADS7128_RETURN_SUCCESS.
 */
ADS7128_RETURN_t ads7128_deinit(void);

/**
 * @brief Performs a software reset on the ADS7128 device over I2C, setting all
 * its registers to default.
 * 
 * @note After a device reset, the internal state of the module will be set to
 * 'ADS7128_STATE_UNINIT', so requires re-initialisation.
 *
 * @return ADS7128_RETURN_SUCCESS on success, otherwise related error.
 */
ADS7128_RETURN_t ads7128_reset(void);

/**
 * @brief This function initiates the periodic sampling cycle of the ADC channels.
 * 
 * @note If a non-null callback was passed during the initialisation, it'll be
 * called at the end of each sampling cycle.
 *
 * @note If the module is already running, the API silently ignores the call,
 * and returns 'ADS7128_RETURN_SUCCESS'.
 *
 * @return ADS7128_RETURN_SUCCESS on success, otherwise related error.
 */
ADS7128_RETURN_t ads7128_start(void);

/**
 * @brief This function stops the periodic sampling cycles of the ADC channels.
 * 
 * @note If the module is not running, the API silently ignores the call,
 * and returns 'ADS7128_RETURN_SUCCESS'.
 *
 * @return ADS_RETURN_t, indicating success or error.
 */
ADS7128_RETURN_t ads7128_stop(void);

/**
 * @brief Return the last readings from all ADC channels.
 *
 * @note The block of memory 'data' points to needs to be at least
 * ADS7128_MAX_CH * 16-bit size.
 * 
 * @note The readings are 16-bit true RMS [ root-mean-square ], however they are
 * initialised as invalid at the beginning of each reading cycle, with RMS_BAD_VALUE.
 * This invalid value is overriden if a new RMS is successfully read, and stayed
 * unchanged otherwise. Thus it's the user responsibility to verify the validity of the
 * readings, after each call to this API, by comparing them against RMS_BAD_VALUE.
 *
 * @param data Pointer to store the readings.
 * @return ADS7128_RETURN_SUCCESS on success, otherwise related error.
 */
ADS7128_RETURN_t ads7128_read(uint16_t *const data);

/**
 * @brief Calibrate from the offset error and changes in temperature.
 * 
 * @note The calibration function works by internally shorting the ADC input to
 * ground an run through the conversion process, and takes 16us max.
 * [TI Forum](https://e2e.ti.com/support/data-converters-group/data-converters
 * /f/data-converters-forum/1300075/ads7138-q1-adc-calibration-and-input-offset-error)
 *
 * @note The module's internal state MUST be 'ADS7128_STATE_READY' before calling
 * this API, or it'll return 'ADS7128_RETURN_BUSY'. If the module was started, it
 * must be stopped by calling ads7128_stop().
 *
 * @return ADS7128_RETURN_SUCCESS on success, otherwise related error.
 */
ADS7128_RETURN_t ads7128_calibration(void);

/**
 * @brief Get the current status of the module.
 *
 * @param state Pointer to the return status.
 * @return ADS7128_RETURN_SUCCESS on success, otherwise related error.
 */
ADS7128_RETURN_t ads7128_get_status(ADS7128_STATE_t *const state);

/**
 * @brief Run the internal state machine.
 *
 * @note This function should be called periodically. For the current implementation
 * the period is 1ms, if this is ever changed, time related functions should be
 * adjusted accordingly.
 *
 * @return ADS7128_RETURN_SUCCESS on success, otherwise related error.
 */
ADS7128_RETURN_t ads7128_run(void);

#ifdef TEST
/**
 * @brief Set the ADS7128 internal states.
 * This helps with unit testing, as some features are only available if the
 * module was previously initialised, and the initialisation is a complex
 * and long operation.
 * The callback is optional, if null is passed, it's ignored.
 * 
 * @param st_internal, ADS7128 internal state.
 * @param user_callback, user callback function.
 *
 * @return ADS_RETURN_t, indicating success or error.
 * 
 */
ADS7128_RETURN_t ads7128_set_internal_status(ADS7128_STATE_t st_internal,
                                        functionCallback_t user_callback);
#endif  // TEST

#endif  // APP_MIDDLEWARE_DSP1_ADS7128_H_
