/**
 * @file dsp1_ads7128.h
 * @author Leonel Gomes [ Enoda Ltd ]
 * @brief Module interface for the ADS7128 [ 8-channel Analogue-to-Digital converter ]
 * for the C28x DSP core.
 * @see https://www.ti.com/lit/ds/symlink/ads7128.pdf
 * @date 18/12/2023
 *
 * @copyright Copyright (C) 2023 Enoda Ltd. - All Rights Reserved
 *
 * Unauthorised copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

/* ==== INCLUDES ===== */

#include <string.h>
#include "dsp1_ads7128.h"
#include "dsp1_i2c.h"

/* ==== DEFINES ===== */

/**
 * Number of ads7128_run() calls to perform a delay of 10ms, based on a
 * timer interrupt of 1ms. If the interrupt frequency changes, this must
 * be updated accordingly.
 */
#define RMS_CONV_COUNTER_10MS (10u)

/** Single register size */
#define SINGLE_REG_SIZE     (1u)
/** RMS size, LSB + MSB */
#define RMS_RESULT_SIZE     (2u)

/** Invalid reading marker - uint8_t */
#define RMS_BAD_VALUE_UINT8 (0xFFu)

/* ==== TYPEDEFS ===== */

/**
 * Describes the module state machine.
 */
typedef enum STATE_MACHINE_en {
    STATE_MACHINE_IDLE = 0u,        /*!< Idle state */
    STATE_MACHINE_PREP_INIT,        /*!< Prepare to initialise ADS7128 */
    STATE_MACHINE_INIT_I2C,         /*!< Initialise ADS7128 over I2C */
    STATE_MACHINE_READ_CFG,         /*!< Prepare to read configuration */
    STATE_MACHINE_READ_INIT,        /*!< Read initialisation configuration over I2C */
    STATE_MACHINE_PREP_CFG_RMS,     /*!< Prepare to configure the RMS module */
    STATE_MACHINE_CFG_RMS,          /*!< Configure the RMS module */
    STATE_MACHINE_READ_RMS,         /*!< Prepare to read the RMS register over I2C */
    STATE_MACHINE_PREP_CAL,         /*!< Prepare to calibrate the ADS7128 chip over I2C */
    STATE_MACHINE_CAL,              /*!< Calibrate the ADS7128 chip over I2C */
    STATE_MACHINE_POLL_CAL_STAT,    /*!< Poll calibration status flag over I2C */
    STATE_MACHINE_PREP_RESET,       /*!< Prepare to reset the ADS7128 device over I2C */
    STATE_MACHINE_RESET,            /*!< ADS software reset over I2C */
    STATE_MACHINE_STATE_MAX,        /*!< Enum boundary max value */
} STATE_MACHINE_t;

/**
 * Describes the internal state of a handler function.
 */
typedef enum HANDLER_STATE_en {
    HANDLER_STATE_IDLE = 0u,        /*!< Handler is idle */
    HANDLER_STATE_RUNNING,          /*!< Handler is running task */
    HANDLER_STATE_WAIT_CALLBACK,    /*!< Wait for I2C operation to complete */
    HANDLER_STATE_FAIL,             /*!< Task failed */
    HANDLER_STATE_MAX,              /*!< Enum boundary max value */
} HANDLER_STATE_t;

/**
 * Groups all necessary data to handle the I2C transactions.
 */
typedef struct i2c_handler_st {
    I2C_INSTANCE_t i2c_instance;    /*!< I2C Instance ID */
    i2c_transfer_t i2c_transfer;    /*!< Transfer parameters */
} i2c_handler_t;

/**
 * Groups all necessary data to handle ADS7128.
 */
typedef volatile struct _ads_handler_st {
    ADS7128_STATE_t st_internal;        /*!< Internal module state */
    STATE_MACHINE_t st_machine;         /*!< State machine*/
    functionCallback_t usr_callback;    /*!< User callback function */
    uint16_t callback_count;            /*!< Number of times the callback was called */
    bool callback_ok;                   /*!< User callback return value */
    ads7128_reg_t *cfg_reg;             /*!< Pointer to ADS array configuration */
    uint16_t cfg_size;                  /*!< Size of the ADS array configuration */
} ads_handler_t;

/* ==== GLOBAL VARIABLES ===== */

/** Flag to reset channel id to 0, to support unit testing! */
static bool g_reset_channel_id = false;

/** Buffer to store RMS channel readings, available through API */
static uint16_t g_rms_buffer[ADS7128_MAX_CH] = {0};

/** Buffer to temporary store RMS channel readings, internal use only */
static uint16_t g_rms_temp_buf[ADS7128_MAX_CH] = {0};

/**
 * Array containing the registers to be used in the ADS initialisation, either from
 * defaults or provided by the user.
 */
static ads7128_reg_t g_ads_init_reg[ADS7128_MAX_CFG_ENTRIES] = {0};

/** Array to store I2C driver transmit frames */
static uint16_t g_i2c_tx_buf[I2C_MAX_DATA_SIZE] = {0};

/** Array to store I2C driver receive frames */
static uint16_t g_i2c_rx_buf[I2C_MAX_DATA_SIZE] = {0};

/**
 * I2C global handler, initialised with defaults, case information is
 * required before init() is called.
 */
static i2c_handler_t g_i2c_handler = {
    .i2c_instance = I2C_INSTANCE_A,
    .i2c_transfer = {
        .targetAddr = 0U,
        .txBuffer = g_i2c_tx_buf,
        .rxBuffer = g_i2c_rx_buf,
        .dataSize = 0U,
        .callback = NULL,
    },
};

/**
 * ADS global handler, initialised with defaults, case information is
 * required before init() is called
 */
static ads_handler_t g_ads_handler = {
    .st_internal = ADS7128_STATE_UNINIT,
    .st_machine = STATE_MACHINE_IDLE,
    .callback_count = 0u,
    .callback_ok = false,
    .cfg_reg = g_ads_init_reg,
    .cfg_size = 0u,
};

/* ==== FUNCTION PROTOTYPES ===== */

static ADS7128_RETURN_t validate_init_params(const ads7128_reg_t ads7128_cfg[],
                                             const uint16_t arr_size);
static void i2c_callback(bool result);
static void i2c_reset_callback(void);
static void set_internal_state(ADS7128_STATE_t state);
static void set_state_machine(STATE_MACHINE_t state);
static void notify_user(void);
static ADS7128_RETURN_t validate_internal_state(void);
static HANDLER_STATE_t handle_init(bool reset_state);
static HANDLER_STATE_t handle_read_init(bool reset_state);
static HANDLER_STATE_t handle_rms_config(bool reset_state, uint16_t channel_id);
static HANDLER_STATE_t handle_rms_read(bool reset_state, uint16_t channel_id);
static HANDLER_STATE_t handle_device_reset(bool reset_state);
static HANDLER_STATE_t handle_calibration(bool reset_state);
static HANDLER_STATE_t handle_poll_cal_status(bool reset_state);

/* ==== APIs ===== */

ADS7128_RETURN_t ads7128_init(const ads7128_reg_t ads7128_cfg[],
                              const uint16_t arr_size,
                              const uint16_t i2c_addr,
                              const functionCallback_t callback) {
    /** Array containing the ADS7128 default configuration registers */
    static const ads7128_reg_t g_ads_reg_default[] = {
        /** RMS_CFG - RMS on CH1, Do not subtract DC, samples = 1024 */
        {.addr = ADS7128_RMS_CFG_R, .value = 0x0010u},
        /** GENERAL_CFG - RMS_EN, No CRC, STATS_EN, No Comparator, */
        /** GENERAL_CFG - normal conv start, default ch config, no cal, no reset */
        {.addr = ADS7128_GEN_CFG_R, .value = 0x00A0u},
        /** OPMODE_CFG - Autonomous mode, high speed oscillator, */
        /** OPMODE_CFG - Sampling freq = 125ksps */
        {.addr = ADS7128_OPMODE_CFG_R, .value = 0x0026u},
    };

    ADS7128_RETURN_t rc = validate_init_params(ads7128_cfg, arr_size);

    // initialise the ADS7128 device
    if (ADS7128_RETURN_SUCCESS == rc) {
        // set state machine to process the ADS initialisation sequence
        set_state_machine(STATE_MACHINE_PREP_INIT);

        // load array with the ADS configuration registers, use defaults if not provided
        if (NULL != ads7128_cfg) {
            memcpy(g_ads_init_reg, ads7128_cfg, sizeof(ads7128_reg_t) * arr_size);
            g_ads_handler.cfg_size = arr_size;
        } else {
            uint16_t array_size = sizeof(g_ads_reg_default) / sizeof(g_ads_reg_default[0u]);
            memcpy(g_ads_init_reg, g_ads_reg_default, sizeof(ads7128_reg_t) * array_size);
            g_ads_handler.cfg_size = array_size;
        }

        // populate the globals with data passed in by the user
        g_i2c_handler.i2c_transfer.targetAddr = i2c_addr;
        g_ads_handler.usr_callback = callback;

        // reset buffers
        memset(g_rms_buffer, RMS_BAD_VALUE_UINT8, sizeof(g_rms_buffer));
        memset(g_rms_temp_buf, RMS_BAD_VALUE_UINT8, sizeof(g_rms_buffer));

        // the internal callback is now visible
        g_i2c_handler.i2c_transfer.callback = i2c_callback;
    }

    return rc;
}

ADS7128_RETURN_t ads7128_deinit(void) {
    // reset states
    set_internal_state(ADS7128_STATE_UNINIT);
    set_state_machine(STATE_MACHINE_IDLE);

    return ADS7128_RETURN_SUCCESS;
}

ADS7128_RETURN_t ads7128_reset(void) {
    ADS7128_RETURN_t rc = validate_internal_state();

    // On top of generic validation, check if the module is running as well
    if (ADS7128_RETURN_SUCCESS == rc) {
        if (ADS7128_STATE_RUNNING == g_ads_handler.st_internal) {
            rc = ADS7128_RETURN_BUSY;
        }
    }

    if (ADS7128_RETURN_SUCCESS == rc) {
        set_state_machine(STATE_MACHINE_PREP_RESET);
    }

    return rc;
}

ADS7128_RETURN_t ads7128_start(void) {
    ADS7128_RETURN_t rc = validate_internal_state();

    // Set the state machine to start reading the RMS from the analogue channels.
    if (ADS7128_RETURN_SUCCESS == rc) {
        // if already running, silently ignore this API call
        if (ADS7128_STATE_RUNNING != g_ads_handler.st_internal) {
            g_reset_channel_id = true;
            set_state_machine(STATE_MACHINE_PREP_CFG_RMS);
        }
    }

    return rc;
}

ADS7128_RETURN_t ads7128_stop(void) {
    ADS7128_RETURN_t rc = validate_internal_state();

    // Set states to stop reading the RMS.
    if (ADS7128_RETURN_SUCCESS == rc) {
        // if not running, silently ignore this API call
        if (ADS7128_STATE_RUNNING == g_ads_handler.st_internal) {
            set_state_machine(STATE_MACHINE_IDLE);
            set_internal_state(ADS7128_STATE_READY);
        }
    }

    return rc;
}

ADS7128_RETURN_t ads7128_read(uint16_t *const data) {
    ADS7128_RETURN_t rc = ADS7128_RETURN_SUCCESS;

    if (NULL == data) {
        rc = ADS7128_RETURN_NULL;
    }

    if (ADS7128_RETURN_SUCCESS == rc) {
        memcpy(data, g_rms_buffer, sizeof(g_rms_buffer));
    }

    return rc;
}

ADS7128_RETURN_t ads7128_calibration(void) {
    ADS7128_RETURN_t rc = validate_internal_state();

    // On top of generic validation, check if the module is running
    if (ADS7128_RETURN_SUCCESS == rc) {
        if (ADS7128_STATE_RUNNING == g_ads_handler.st_internal) {
            rc = ADS7128_RETURN_BUSY;
        }
    }

    // Set state machine to start calibration
    if (ADS7128_RETURN_SUCCESS == rc) {
        set_state_machine(STATE_MACHINE_PREP_CAL);
    }

    return rc;
}

ADS7128_RETURN_t ads7128_get_status(ADS7128_STATE_t *state) {
    ADS7128_RETURN_t rc = ADS7128_RETURN_SUCCESS;

    if (NULL == state) {
        rc = ADS7128_RETURN_NULL;
    }

    if (ADS7128_RETURN_SUCCESS == rc) {
        *state = g_ads_handler.st_internal;
    }

    return rc;
}

ADS7128_RETURN_t ads7128_run(void) {
    ADS7128_RETURN_t rc = ADS7128_RETURN_SUCCESS;
    static uint16_t rms_channel_id = ADS7128_MAX_CH;
    static ADS7128_STATE_t last_internal_state = ADS7128_STATE_MAX;

    switch (g_ads_handler.st_machine) {
    case STATE_MACHINE_IDLE:
        break;

    case STATE_MACHINE_PREP_INIT:
        // reset init handler to a known state.
        handle_init(true);
        set_internal_state(ADS7128_STATE_BUSY);
        set_state_machine(STATE_MACHINE_INIT_I2C);
        break;

    case STATE_MACHINE_INIT_I2C: {
        // Configure the ADS7128 module
        // if success, read back config
        HANDLER_STATE_t ret_c = handle_init(false);

        if (HANDLER_STATE_IDLE == ret_c) {
            // task successfully finished, read back config
            handle_read_init(true);
            set_state_machine(STATE_MACHINE_READ_INIT);
        }
        if (HANDLER_STATE_FAIL == ret_c) {
            // failed config, reset states
            set_internal_state(ADS7128_STATE_UNINIT);
            set_state_machine(STATE_MACHINE_IDLE);
        }
    }break;

    case STATE_MACHINE_READ_INIT: {
        // Read back the ADS7128 configuration registers
        // if success, perform a calibration
        HANDLER_STATE_t ret_c = handle_read_init(false);

        if (HANDLER_STATE_IDLE == ret_c) {
            // task successfully finished, perform a calibration
            // internal state is now ready, even if calibration fails
            set_internal_state(ADS7128_STATE_READY);
            set_state_machine(STATE_MACHINE_PREP_CAL);
        }
        if (HANDLER_STATE_FAIL == ret_c) {
            // failed, reset states
            set_internal_state(ADS7128_STATE_UNINIT);
            set_state_machine(STATE_MACHINE_IDLE);
        }
    }break;

    case STATE_MACHINE_PREP_CFG_RMS:
        // first configure the RMS, then read it. This is done channel by channel.
        // case it fails, continue to the next channel
        if ((++rms_channel_id >= ADS7128_MAX_CH) || g_reset_channel_id) {
            rms_channel_id = 0u;
            g_reset_channel_id = false;
        }
        handle_rms_config(true, rms_channel_id);
        set_internal_state(ADS7128_STATE_RUNNING);
        set_state_machine(STATE_MACHINE_CFG_RMS);
        break;

    case STATE_MACHINE_CFG_RMS: {
        // Configure RMS
        // if success, read RMS result
        HANDLER_STATE_t ret_c = handle_rms_config(false, rms_channel_id);

        if (HANDLER_STATE_IDLE == ret_c) {
            // RMS successfully configured, read its result
            handle_rms_read(true, rms_channel_id);
            set_state_machine(STATE_MACHINE_READ_RMS);
        }
        if (HANDLER_STATE_FAIL == ret_c) {
            // RMS failed, continue to the next channel
            handle_rms_config(true, rms_channel_id);
            set_state_machine(STATE_MACHINE_PREP_CFG_RMS);
        }
    }break;

    case STATE_MACHINE_READ_RMS: {
        // Read RMS result
        // whether it fails or succeeds, continue to the next channel
        HANDLER_STATE_t ret_c = handle_rms_read(false, rms_channel_id);

        if ((HANDLER_STATE_IDLE == ret_c) || (HANDLER_STATE_FAIL == ret_c)) {
            // task finished, continue to the next channel
            handle_rms_config(true, rms_channel_id);
            set_state_machine(STATE_MACHINE_PREP_CFG_RMS);
        }
    }break;

    case STATE_MACHINE_PREP_RESET:
        handle_device_reset(true);
        last_internal_state = g_ads_handler.st_internal;
        set_internal_state(ADS7128_STATE_BUSY);
        set_state_machine(STATE_MACHINE_RESET);
        break;

    case STATE_MACHINE_RESET: {
        HANDLER_STATE_t ret_c = handle_device_reset(false);

        if (HANDLER_STATE_IDLE == ret_c) {
            set_internal_state(ADS7128_STATE_UNINIT);
            set_state_machine(STATE_MACHINE_IDLE);
        }

        if (HANDLER_STATE_FAIL == ret_c) {
            set_internal_state(last_internal_state);
            set_state_machine(STATE_MACHINE_IDLE);
        }
    }break;

    case STATE_MACHINE_PREP_CAL:
        // reset calibration handler to a known state.
        handle_calibration(true);
        set_internal_state(ADS7128_STATE_BUSY);
        set_state_machine(STATE_MACHINE_CAL);
        break;

    case STATE_MACHINE_CAL: {
        // Start calibration sequence
        // if success, poll calibration status
        HANDLER_STATE_t ret_c = handle_calibration(false);

        if (HANDLER_STATE_IDLE == ret_c) {
            // task successfully finished, poll calibration status
            handle_poll_cal_status(true);
            set_state_machine(STATE_MACHINE_POLL_CAL_STAT);
        }

        if (HANDLER_STATE_FAIL == ret_c) {
            // failed, bail out
            set_internal_state(ADS7128_STATE_READY);
            set_state_machine(STATE_MACHINE_IDLE);
        }
    }break;

    case STATE_MACHINE_POLL_CAL_STAT: {
        HANDLER_STATE_t ret_c = handle_poll_cal_status(false);

        if ((HANDLER_STATE_IDLE == ret_c) || (HANDLER_STATE_FAIL == ret_c)) {
            // calibration complete
            set_internal_state(ADS7128_STATE_READY);
            set_state_machine(STATE_MACHINE_IDLE);
        }
    }break;

    default:
        set_state_machine(STATE_MACHINE_IDLE);
        rc = ADS7128_RETURN_INTERNAL_ERROR;
        break;
    }

    return rc;
}

#ifdef TEST
ADS7128_RETURN_t ads7128_set_internal_status(ADS7128_STATE_t st_internal,
                                        functionCallback_t user_callback) {
    ADS7128_RETURN_t rc = ADS7128_RETURN_SUCCESS;

    if (st_internal >= ADS7128_STATE_MAX) {
        rc = ADS7128_RETURN_BAD_PARAM;
    }

    if (ADS7128_RETURN_SUCCESS == rc) {
        set_internal_state(st_internal);

        if (NULL != user_callback) {
            g_ads_handler.usr_callback = user_callback;
        }
    }

    return rc;
}
#endif

/* ==== STATIC FUNCTIONS ===== */

/* ==== Inline Functions ===== */

/* ==== Utility Functions ===== */


static ADS7128_RETURN_t validate_init_params(const ads7128_reg_t ads7128_cfg[],
                                             const uint16_t arr_size) {
    ADS7128_RETURN_t rc = ADS7128_RETURN_SUCCESS;

    if (ADS7128_STATE_READY == g_ads_handler.st_internal) {
        rc = ADS7128_RETURN_ALREADY_INIT;
    }

    if (ADS7128_RETURN_SUCCESS == rc) {
        if ((ADS7128_STATE_BUSY == g_ads_handler.st_internal) ||
            (ADS7128_STATE_RUNNING == g_ads_handler.st_internal)) {
            rc = ADS7128_RETURN_BUSY;
        }
    }

    if (ADS7128_RETURN_SUCCESS == rc) {
        if (NULL != ads7128_cfg) {
            if ((arr_size > ADS7128_MAX_CFG_ENTRIES) || (arr_size < ADS7128_MIN_CFG_ENTRIES)) {
                rc = ADS7128_RETURN_BAD_PARAM;
            }
        }
    }

    // Initialise the I2C driver
    if (ADS7128_RETURN_SUCCESS == rc) {
        if (I2C_RETURN_SUCCESS != i2c_init(g_i2c_handler.i2c_instance)) {
            rc = ADS7128_RETURN_INTERNAL_ERROR;
        }
    }

    return rc;
}

/**
 * @brief Callback passed to the I2C driver to be called after each
 * I2C transaction is completed.
 * 
 * @param result, boolean indicating success or error.
 */
static void i2c_callback(bool result) {
    g_ads_handler.callback_count++;
    g_ads_handler.callback_ok = result;
}

/**
 * @brief Reset all callback data.
 * 
 * @note This should be called before each I2C transaction that'll make use
 * of the callback.
 */
static void i2c_reset_callback(void) {
    g_ads_handler.callback_count = 0u;
    g_ads_handler.callback_ok = false;
}

/**
 * @brief Set the internal state.
 *
 * @param state, new state.
 */

static void set_internal_state(ADS7128_STATE_t state) {
    g_ads_handler.st_internal = state;
}

/**
 * @brief Set the state machine.
 * 
 * @param state, new state machine.
 */
static void set_state_machine(STATE_MACHINE_t state) {
    g_ads_handler.st_machine = state;
}

/**
 * @brief Notify the user by calling the callback function with success
 * of operation.
 *
 * @param success, indicating success or error.
 */
static void notify_user(void) {
    if (NULL != g_ads_handler.usr_callback) {
        g_ads_handler.usr_callback();
    }
}

/**
 * @brief Checks if the module is ready to execute a given task.
 *
 * @return ADS7128_RETURN_SUCCESS on success, otherwise related error.
 */
static ADS7128_RETURN_t validate_internal_state(void) {
    ADS7128_RETURN_t rc = ADS7128_RETURN_SUCCESS;

    // Any operation over I2C requires the module to be initialised
    if (ADS7128_STATE_UNINIT == g_ads_handler.st_internal) {
        rc = ADS7128_RETURN_NOT_INIT;
    }

    if (ADS7128_RETURN_SUCCESS == rc) {
        if (ADS7128_STATE_BUSY == g_ads_handler.st_internal) {
            rc = ADS7128_RETURN_BUSY;
        }
    }

    return rc;
}

/**
 * @brief Configure the ADS7128 intialisation registers over I2C.
 *
 * @param reset_state, if true reset the handler's internal state
 * @return ADS_HANDLE_STATE_t, current internal state of this handler
 */
static HANDLER_STATE_t handle_init(bool reset_state) {
    static HANDLER_STATE_t handler_state = HANDLER_STATE_IDLE;
    static uint16_t array_idx = 0u;

    if (reset_state) {
        handler_state = HANDLER_STATE_IDLE;
    }

    switch (handler_state) {
    case HANDLER_STATE_IDLE:
        array_idx = 0u;
        handler_state = HANDLER_STATE_RUNNING;
        break;

    case HANDLER_STATE_RUNNING:
        // Load a single register at a time, and send it over I2C
        if (array_idx < g_ads_handler.cfg_size) {
            uint16_t i = 0u;
            g_i2c_tx_buf[i++] = ADS7128_OPCODE_SW;
            g_i2c_tx_buf[i++] = g_ads_handler.cfg_reg[array_idx].addr;
            g_i2c_tx_buf[i++] = g_ads_handler.cfg_reg[array_idx].value;
            g_i2c_handler.i2c_transfer.dataSize = i;
            i2c_reset_callback();

            // Check if the I2C driver is ready
            if (I2C_RETURN_SUCCESS == i2c_write(g_i2c_handler.i2c_instance,
                                                &g_i2c_handler.i2c_transfer)) {
                array_idx++;
                handler_state = HANDLER_STATE_WAIT_CALLBACK;
            } else {
                // The I2C driver is not ready, cancel task
                handler_state = HANDLER_STATE_FAIL;
            }
        } else {
            // Configuration done
            handler_state = HANDLER_STATE_IDLE;
        }
        break;

    case HANDLER_STATE_WAIT_CALLBACK:
        if (g_ads_handler.callback_count > 0u) {
            if (!g_ads_handler.callback_ok) {
                // failed the I2C transaction
                handler_state = HANDLER_STATE_FAIL;
            } else {
                // continue with the ADS7128 initialisation
                handler_state = HANDLER_STATE_RUNNING;
            }
        }
        break;

    case HANDLER_STATE_FAIL:
        handler_state = HANDLER_STATE_IDLE;
        break;

    default:
        // this should never happen
        handler_state = HANDLER_STATE_FAIL;
        break;
    }

    return handler_state;
}

/**
 * @brief Read back the ADS7128 intialisation registers, over I2C.
 *
 * @param reset_state, if true reset the handler's internal state
 * @return ADS_HANDLE_STATE_t, current internal state of this handler
 */
static HANDLER_STATE_t handle_read_init(bool reset_state) {
    static HANDLER_STATE_t handler_state = HANDLER_STATE_IDLE;
    static uint16_t array_idx = 0u;
    static bool is_i2c_write = true;

    if (reset_state) {
        handler_state = HANDLER_STATE_IDLE;
    }
    switch (handler_state) {
    case HANDLER_STATE_IDLE:
        array_idx = 0u;
        is_i2c_write = true;
        handler_state = HANDLER_STATE_RUNNING;
        break;

    case HANDLER_STATE_RUNNING:
        // Read a single register at a time
        // Reading over I2C requires a write followed by a read transaction
        if (array_idx < g_ads_handler.cfg_size) {
            I2C_RETURN_t rc = I2C_RETURN_SUCCESS;
            i2c_reset_callback();

            // check if we need to send a read or a write frame
            if (is_i2c_write) {
                uint16_t i = 0u;
                g_i2c_tx_buf[i++] = ADS7128_OPCODE_SR;
                g_i2c_tx_buf[i++] = g_ads_handler.cfg_reg[array_idx].addr;
                g_i2c_handler.i2c_transfer.dataSize = i;
                rc = i2c_write(g_i2c_handler.i2c_instance, &g_i2c_handler.i2c_transfer);
            } else {
                array_idx++;
                g_i2c_handler.i2c_transfer.dataSize = SINGLE_REG_SIZE;
                rc = i2c_read(g_i2c_handler.i2c_instance, &g_i2c_handler.i2c_transfer);
            }

            // Check if the I2C driver is ready,
            if (I2C_RETURN_SUCCESS == rc) {
                handler_state = HANDLER_STATE_WAIT_CALLBACK;
            } else {
                handler_state = HANDLER_STATE_FAIL;
            }
        } else {
            // Done reading back the configuration
            handler_state = HANDLER_STATE_IDLE;
        }
        break;

    case HANDLER_STATE_WAIT_CALLBACK:
        if (g_ads_handler.callback_count > 0u) {
            // next state, if no errors
            handler_state = HANDLER_STATE_RUNNING;

            if (!g_ads_handler.callback_ok) {
                handler_state = HANDLER_STATE_FAIL;
            } else if (!is_i2c_write) {
                if (g_i2c_rx_buf[0u] != g_ads_handler.cfg_reg[array_idx - 1u].value) {
                    // error - register's value mismatch
                    handler_state = HANDLER_STATE_FAIL;
                }
            }
            // next frame will be of the other type
            is_i2c_write = !is_i2c_write;
        }
        break;

    case HANDLER_STATE_FAIL:
        handler_state = HANDLER_STATE_IDLE;
        break;

    default:
        // this should never happen
        handler_state = HANDLER_STATE_FAIL;
        break;
    }

    return handler_state;
}

/**
 * @brief Configure RMS, start conversion, wait for conversion to finish, and
 * then stop conversion.
 *
 * @note RMS computation takes time: CYCLE TIME * SAMPLING FREQUENCY.
 * CYCLE TIME is given by Table 5. [ADS7128 datasheet]
 * SAMPLING FREQUENCY is set in the RMS_CFG register [ADS7128 datasheet]
 * If the defaults are used, RMS computation time is ~8.5us.
 *
 * @param reset_state, if true reset the handler's internal state
 * @param channel_id, channel to configure
 * @return ADS_HANDLE_STATE_t, current internal state of this handler
 */
static HANDLER_STATE_t handle_rms_config(bool reset_state, uint16_t channel_id) {
    /** Array containing the ADS7128 RMS configuration registers */
    const ads7128_reg_t g_ads_rms_cfg[] = {
       /** RMS_CFG - RMS on CHx, Do not subtract DC, samples = 1024 */
       {.addr = ADS7128_RMS_CFG_R, .value = 0x0010u},
       /** AUTO_SEQ_CH_SEL - This selects channel x as the only channel in the sequence */
       {.addr = ADS7128_AUTO_SEQ_CH_SEL_R, .value = 0x0002u},
       /** SYSTEM_STATUS - clear RMS_DONE */
       {.addr = ADS7128_RMS_CFG_R, .value = 0x0010u},
       /** GENERAL_CFG - clear RMS_EN */
       {.addr = ADS7128_GEN_CFG_R, .value = 0x0000u},
       /** GENERAL_CFG - set RMS_EN */
       {.addr = ADS7128_GEN_CFG_R, .value = 0x0080u},
       /** SEQUENCE_CFG - start channel sequencing, auto sequence mode */
       {.addr = ADS7128_SEQUENCE_CFG_R, .value = 0x0011u},
       /** SEQUENCE_CFG - stop the sequencer */
       {.addr = ADS7128_SEQUENCE_CFG_R, .value = 0x0001u},
    };

    const uint16_t arr_elements = sizeof(g_ads_rms_cfg) / sizeof(g_ads_rms_cfg[0u]);
    static HANDLER_STATE_t handler_state = HANDLER_STATE_IDLE;
    static uint16_t array_idx = 0u;

    if (reset_state) {
        handler_state = HANDLER_STATE_IDLE;
    }

    switch (handler_state) {
    case HANDLER_STATE_IDLE:
        array_idx = 0u;
        handler_state = HANDLER_STATE_RUNNING;
        break;

    case HANDLER_STATE_RUNNING: {
        static uint16_t rms_conv_counter = 0u;

        // RMS takes time: CYCLE TIME * (RMS_SAMPLES[1:0] + 40 samples) ~8.5ms
        // This timeout starts counting before loading the stop channel sequencing
        // command, which is done by setting GEN_CFG_CAL_F to 0x01
        if ((ADS7128_SEQUENCE_CFG_R == g_ads_rms_cfg[array_idx].addr) &&
            (0x0001u == g_ads_rms_cfg[array_idx].value)) {
            if (++rms_conv_counter >= RMS_CONV_COUNTER_10MS) {
                rms_conv_counter = 0u;
            } else {
                // bail out!
                // this break is here as it would be too convoluted to use if/else.
                break;
            }
        }

        // Load a single register at a time, and send it over I2C
        if (array_idx < arr_elements) {
            uint16_t i = 0u;
            g_i2c_tx_buf[i++] = ADS7128_OPCODE_SW;
            g_i2c_tx_buf[i++] = g_ads_rms_cfg[array_idx].addr;

            // some registers depend on the channel id
            if (ADS7128_RMS_CFG_R == g_ads_rms_cfg[array_idx].addr) {
                // RMS_CHID takes the RMS_CFG_R [7-4] position
                g_i2c_tx_buf[i++] = channel_id << 4u;
            } else if (ADS7128_AUTO_SEQ_CH_SEL_R == g_ads_rms_cfg[array_idx].addr) {
                g_i2c_tx_buf[i++] = 1u << channel_id;
            } else {
                g_i2c_tx_buf[i++] = g_ads_rms_cfg[array_idx].value;
            }
            g_i2c_handler.i2c_transfer.dataSize = i;
            i2c_reset_callback();

            // Check if the I2C driver is ready
            if (I2C_RETURN_SUCCESS == i2c_write(g_i2c_handler.i2c_instance,
                                                &g_i2c_handler.i2c_transfer)) {
                array_idx++;
                handler_state = HANDLER_STATE_WAIT_CALLBACK;
            } else {
                // The I2C driver is not ready
                handler_state = HANDLER_STATE_FAIL;
            }
        } else {
            // Configuration done
            handler_state = HANDLER_STATE_IDLE;
        }
    }break;

    case HANDLER_STATE_WAIT_CALLBACK:
        if (g_ads_handler.callback_count > 0u) {
            if (!g_ads_handler.callback_ok) {
                // failed the I2C transaction
                handler_state = HANDLER_STATE_FAIL;
            } else {
                // continue with the RMS configuration
                handler_state = HANDLER_STATE_RUNNING;
            }
        } else {
            // consider to add here a timeout
        }
        break;

    case HANDLER_STATE_FAIL:
        handler_state = HANDLER_STATE_IDLE;
        break;

    default:
        // this should never happen
        handler_state = HANDLER_STATE_FAIL;
        break;
    }

    return handler_state;
}

/**
 * @brief Read RMS from each channel, and store it in buffer.
 *
 * @param reset_state, if true reset the handler's internal state
 * @param channel_id, channel to configure
 * @return ADS_HANDLE_STATE_t, current internal state of this handler
 */
static HANDLER_STATE_t handle_rms_read(bool reset_state, uint16_t channel_id) {
    static HANDLER_STATE_t handler_state = HANDLER_STATE_IDLE;
    static bool is_i2c_write = true;

    if (reset_state) {
        handler_state = HANDLER_STATE_IDLE;
    }

    switch (handler_state) {
    case HANDLER_STATE_IDLE:
        is_i2c_write = true;
        handler_state = HANDLER_STATE_RUNNING;
        break;

    case HANDLER_STATE_RUNNING: {
        // Reading over I2C requires a write followed by a read operation
        I2C_RETURN_t rc = I2C_RETURN_SUCCESS;
        i2c_reset_callback();

        // check if we need to send a read or a write frame over I2C
        if (is_i2c_write) {
            uint16_t i = 0u;
            g_i2c_tx_buf[i++] = ADS7128_OPCODE_RC;
            g_i2c_tx_buf[i++] = ADS7128_RMS_RESULT_LSB_R;
            g_i2c_handler.i2c_transfer.dataSize = i;
            rc = i2c_write(g_i2c_handler.i2c_instance, &g_i2c_handler.i2c_transfer);
        } else {
            g_i2c_handler.i2c_transfer.dataSize = RMS_RESULT_SIZE;
            rc = i2c_read(g_i2c_handler.i2c_instance, &g_i2c_handler.i2c_transfer);
        }

        // Check if the I2C driver is ready,
        if (I2C_RETURN_SUCCESS == rc) {
            handler_state = HANDLER_STATE_WAIT_CALLBACK;
        } else {
            handler_state = HANDLER_STATE_FAIL;
        }
    }break;

    case HANDLER_STATE_WAIT_CALLBACK:
        if (g_ads_handler.callback_count > 0u) {
            if (!g_ads_handler.callback_ok) {
                handler_state = HANDLER_STATE_FAIL;
            } else if (!is_i2c_write) {
                // it's a 16-bit value, with LSB received first.
                g_rms_temp_buf[channel_id] = g_i2c_rx_buf[0u];
                g_rms_temp_buf[channel_id] |= (g_i2c_rx_buf[1u] << 8u);

                // If this is the end of a RMS cycle, copy buffers over
                if (channel_id >= (ADS7128_MAX_CH - 1u)) {
                    memcpy(g_rms_buffer, g_rms_temp_buf, sizeof(g_rms_buffer));
                    // reset intermediate buffer
                    memset(g_rms_temp_buf, RMS_BAD_VALUE_UINT8, sizeof(g_rms_temp_buf));
                    notify_user();
                }

                // Done reading RMS
                handler_state = HANDLER_STATE_IDLE;
            } else {
                // continue with the transaction
                handler_state = HANDLER_STATE_RUNNING;
            }
            // next frame will be of the other type
            is_i2c_write = !is_i2c_write;
        }
        break;

    case HANDLER_STATE_FAIL:
        g_rms_buffer[channel_id] = ADS7128_RMS_BAD_VALUE;
        handler_state = HANDLER_STATE_IDLE;
        break;

    default:
        // this should never happen
        handler_state = HANDLER_STATE_FAIL;
        break;
    }

    return handler_state;
}

/**
 * @brief Reset the ADS7128 device over I2C.
 *
 * @param reset_state, if true reset the handler's internal state
 * @return ADS_HANDLE_STATE_t, current internal state of this handler
 */
static HANDLER_STATE_t handle_device_reset(bool reset_state) {
    static HANDLER_STATE_t handler_state = HANDLER_STATE_IDLE;

    if (reset_state) {
        handler_state = HANDLER_STATE_IDLE;
    }

    switch (handler_state) {
    case HANDLER_STATE_IDLE:
        handler_state = HANDLER_STATE_RUNNING;
        break;

    case HANDLER_STATE_RUNNING: {
        uint16_t i = 0u;
        g_i2c_tx_buf[i++] = ADS7128_OPCODE_SB;
        g_i2c_tx_buf[i++] = ADS7128_GEN_CFG_R;
        g_i2c_tx_buf[i++] = GEN_CFG_RST_F;
        g_i2c_handler.i2c_transfer.dataSize = i;
        i2c_reset_callback();

        // Check if the I2C driver is ready
        if (I2C_RETURN_SUCCESS == i2c_write(g_i2c_handler.i2c_instance,
                                            &g_i2c_handler.i2c_transfer)) {
            handler_state = HANDLER_STATE_WAIT_CALLBACK;
        } else {
            // The I2C driver is not ready
            handler_state = HANDLER_STATE_FAIL;
        }
    }break;

    case HANDLER_STATE_WAIT_CALLBACK:
        if (g_ads_handler.callback_count > 0u) {
            if (!g_ads_handler.callback_ok) {
                // failed the I2C transaction
                handler_state = HANDLER_STATE_FAIL;
            } else {
                // transaction successfully done, end task.
                handler_state = HANDLER_STATE_IDLE;
            }
        }
        break;

    case HANDLER_STATE_FAIL:
        handler_state = HANDLER_STATE_IDLE;
        break;

    default:
        // this should never happen
        handler_state = HANDLER_STATE_FAIL;
        break;
    }

    return handler_state;
}

/**
 * @brief Starts calibration routine on the ADS7128 device, over I2C.
 *
 * @param reset_state, if true reset the handler's internal state
 * @return ADS_HANDLE_STATE_t, current internal state of this handler
 */
static HANDLER_STATE_t handle_calibration(bool reset_state) {
    static HANDLER_STATE_t handler_state = HANDLER_STATE_IDLE;

    if (reset_state) {
        handler_state = HANDLER_STATE_IDLE;
    }

    switch (handler_state) {
    case HANDLER_STATE_IDLE:
        handler_state = HANDLER_STATE_RUNNING;
        break;

    case HANDLER_STATE_RUNNING: {
        uint16_t i = 0u;
        g_i2c_tx_buf[i++] = ADS7128_OPCODE_SB;
        g_i2c_tx_buf[i++] = ADS7128_GEN_CFG_R;
        g_i2c_tx_buf[i++] = ADS7128_GEN_CFG_CAL_F;
        g_i2c_handler.i2c_transfer.dataSize = i;
        i2c_reset_callback();

        // Check if the I2C driver is ready
        if (I2C_RETURN_SUCCESS == i2c_write(g_i2c_handler.i2c_instance,
                                            &g_i2c_handler.i2c_transfer)) {
            handler_state = HANDLER_STATE_WAIT_CALLBACK;
        } else {
            // The I2C driver is not ready
            handler_state = HANDLER_STATE_FAIL;
        }
    }break;

    case HANDLER_STATE_WAIT_CALLBACK:
        if (g_ads_handler.callback_count > 0u) {
            if (!g_ads_handler.callback_ok) {
                // failed the I2C transaction
                handler_state = HANDLER_STATE_FAIL;
            } else {
                // transaction successfully done, end task.
                handler_state = HANDLER_STATE_IDLE;
            }
        }
        break;

    case HANDLER_STATE_FAIL:
        handler_state = HANDLER_STATE_IDLE;
        break;

    default:
        // this should never happen
        handler_state = HANDLER_STATE_FAIL;
        break;
    }

    return handler_state;
}

/**
 * @brief Poll the CAL bit to check the ADC offset calibration completion status.
 *
 * @note GEN_CFG_CAL_F bit is set to 0b after calibration is complete.
 *
 * @param reset_state, if true reset the handler's internal state
 * @return ADS_HANDLE_STATE_t, current internal state of this handler
 */
static HANDLER_STATE_t handle_poll_cal_status(bool reset_state) {
    static HANDLER_STATE_t handler_state = HANDLER_STATE_IDLE;
    static bool is_i2c_write = true;

    if (reset_state) {
        handler_state = HANDLER_STATE_IDLE;
    }

    switch (handler_state) {
    case HANDLER_STATE_IDLE:
        is_i2c_write = true;
        handler_state = HANDLER_STATE_RUNNING;
        break;

    case HANDLER_STATE_RUNNING: {
        // Reading over I2C requires a write followed by a read operation
        I2C_RETURN_t rc = I2C_RETURN_SUCCESS;
        i2c_reset_callback();

        // check if we need to send a read or a write frame over I2C
        if (is_i2c_write) {
            uint16_t i = 0u;
            g_i2c_tx_buf[i++] = ADS7128_OPCODE_SR;
            g_i2c_tx_buf[i++] = ADS7128_GEN_CFG_R;
            g_i2c_handler.i2c_transfer.dataSize = i;
            rc = i2c_write(g_i2c_handler.i2c_instance, &g_i2c_handler.i2c_transfer);
        } else {
            g_i2c_handler.i2c_transfer.dataSize = SINGLE_REG_SIZE;
            rc = i2c_read(g_i2c_handler.i2c_instance, &g_i2c_handler.i2c_transfer);
        }

        // Check if the I2C driver is ready,
        if (I2C_RETURN_SUCCESS == rc) {
            handler_state = HANDLER_STATE_WAIT_CALLBACK;
        } else {
            handler_state = HANDLER_STATE_FAIL;
        }
    }break;

    case HANDLER_STATE_WAIT_CALLBACK:
        if (g_ads_handler.callback_count > 0u) {
            if (!g_ads_handler.callback_ok) {
                handler_state = HANDLER_STATE_FAIL;
            } else if (!is_i2c_write && !(g_i2c_rx_buf[0u] & ADS7128_GEN_CFG_CAL_F)) {
                // calibration complete
                handler_state = HANDLER_STATE_IDLE;
            } else {
                // continue with the transaction
                handler_state = HANDLER_STATE_RUNNING;
            }
            // next frame will be of the other type
            is_i2c_write = !is_i2c_write;
        } else {
            // consider adding here a timeout
        }
        break;

    case HANDLER_STATE_FAIL:
        handler_state = HANDLER_STATE_IDLE;
        break;

    default:
        // this should never happen
        handler_state = HANDLER_STATE_FAIL;
        break;
    }

    return handler_state;
}
