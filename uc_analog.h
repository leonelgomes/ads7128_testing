/**
 * @file
 * The analog signal associated handling is declared.
 *
 * @date 27 Apr, 2023
 * @author Shoujie Li <Shoujie.Li@enodatech.com>
 * @copyright Copyright (c) 2023 ENODA - https://enodatech.com/
 * All rights reserved.
 */

#ifndef UC_ANALOG_H
#define UC_ANALOG_H

#include <stdint.h>

#include "driverlib.h"
#include "adc.h"
#include "prime_iam.pb.h"

/** The channel mask does not include DC Link voltage because it is not read by internal ADC */
#define CHANNEL_MASK (0x00FFFFFFul)

#define SWITCH_OVERSAMPLING_FACTOR (32)
#define SAMPLE_CACHE_SIZE (SWITCH_OVERSAMPLING_FACTOR)
#define MODULE_SOC_NUM  (ADC_SOC_NUMBER8)

/** The size of cached analog channel signals for switching decision */
#define SWITCH_CACHE_SIZE (20)

/** The report cache size, at least 10 cycles and 10 data in each cycle */
#define REPORT_CACHE_SIZE (100)

typedef enum ADC_MODULE_en
{
    ADC_MODULE_A = 0,
    ADC_MODULE_B,
    ADC_MODULE_C,
    ADC_MODULE_D,

    ADC_MODULE_NUM
} ADC_MODULE_t;

/**
 * The calibration definition for the analog channels.
 */
typedef struct channel_calibration_st
{
    float32_t offset;
    float32_t gain;
} channel_calibration_t;

/**
 * The switch cache is collected later for report. It is also used
 * for calibration as well.
 */
typedef struct switch_cache_st
{
    /**< The mask indicates all channels were read for this switch */
    uint32_t mask;

    /**< The switch cache index */
    int16_t idx;

    /**< The cached data, 1500 words in total. */
    struct {
        uint16_t raw;
        float32_t calibrated;
    } data[SWITCH_CACHE_SIZE][enoda_prime_iam_SensorChannel_CHANNEL_CHANNEL_MAX];
} switch_cache_t;

typedef struct report_st
{
    /**< The switch interrupt number to cache one measurement */
    uint32_t cache_switch_num;
    volatile uint32_t cache_switch_cnt;

    /**< The system tick interrupt number to report to network */
    uint32_t report_systick_num;
    volatile uint32_t report_systick_cnt;

    uint32_t sample_num;
    uint32_t report_size;

    /**< The values to report */
    int16_t idx;
    float32_t data[REPORT_CACHE_SIZE][enoda_prime_iam_SensorChannel_CHANNEL_CHANNEL_MAX];
} report_t;

void analog_init();
void calibration_proc();

#endif
