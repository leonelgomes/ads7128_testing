#ifdef TEST
/**
 * @file test_dsp1_i2c.c
 * @date 13 Dec, 2023
 * @author Leonel Gomes [ Enoda Ltd. ]
 * @copyright Copyright (C) 2024 Enoda Ltd. - All Rights Reserved
 * 
 * This file contains the declarations for the test functions and
 * test-specific elements related to the ADS7128 middleware module. It uses
 * the Unity test framework for test assertions and the CMock library
 * for mocking dependencies.
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

/* ==== INCLUDES ==== */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "unity.h"
#include "../../../middleware/dsp1_ads7128.h"
#include "mock_dsp1_i2c.h"

/* ==== DEFINES ===== */

/** Size of the user's configuration array */
#define USER_CONFIG_ARRAY_SIZE (7u)

/** Size of the default configuration array */
#define DEFAULT_CONFIG_ARRAY_SIZE (3u)

/** Size of the RMS configuration array */
#define RMS_CONFIG_ARRAY_SIZE (7u)

/** RMS LSB dummy value */
#define RMS_LSB_VALUE (0x00AAu)

/** RMS MSB dummy value */
#define RMS_MSB_VALUE (0x0055u)

/** Number of times the ads7128 runner should be called */
#define ADS7128_RUNNER_COUNTER (15)

/* ==== TYPEDEFS ==== */

/** Describes the possible contexts of calls to 'my_stub_function' */
typedef enum MY_STUB_en {
    MY_STUB_I2C_OK = 0u,
    MY_STUB_I2C_NOT_OK,
    MY_STUB_ERROR_HANDLING_INIT_CFG,
    MY_STUB_FUNCTIONALITY_INIT_CFG_DEFAULTS,
    MY_STUB_FUNCTIONALITY_INIT_CFG_USER_DATA,
    MY_STUB_FUNCTIONALITY_RESET,
    MY_STUB_FUNCTIONALITY_START,
    MY_STUB_MAX,
} MY_STUB_t;

/* ==== GLOBAL VARIABLES ==== */

/** Array containing dummy configuration values */
static const ads7128_reg_t ads7128_user_reg_cfg[USER_CONFIG_ARRAY_SIZE] = {
     {.addr = 0x0016u, .value = 0x0002u},
     {.addr = 0x0017u, .value = 0x0001u},
     {.addr = 0x00C0u, .value = 0x0010u},
     {.addr = 0x0001u, .value = 0x00A0u},
     {.addr = 0x0012u, .value = 0x0002u},
     {.addr = 0x0010u, .value = 0x0001u},
     {.addr = 0x0004u, .value = 0x0026u}
};

/** Array containing the ADS7128 default configuration */
static const ads7128_reg_t g_ads_reg_default[DEFAULT_CONFIG_ARRAY_SIZE] = {
    {.addr = RMS_CFG_R, .value = 0x0010u},
    {.addr = GEN_CFG_R, .value = 0x00A0u},
    {.addr = OPMODE_CFG_R, .value = 0x0026u}
};

/** Array containing the ADS7128 RMS configuration */
const ads7128_reg_t g_ads_rms_cfg[RMS_CONFIG_ARRAY_SIZE] = {
    {.addr = RMS_CFG_R, .value = 0x0010u},
    {.addr = AUTO_SEQ_CH_SEL_R, .value = 0x0002u},
    {.addr = RMS_CFG_R, .value = 0x0010u},
    {.addr = GEN_CFG_R, .value = 0x0000u},
    {.addr = GEN_CFG_R, .value = 0x0080u},
    {.addr = SEQUENCE_CFG_R, .value = 0x0011u},
    {.addr = SEQUENCE_CFG_R, .value = 0x0001u}
};

/** Global variable containing the current context of calls to 'my_stub_function' */
static MY_STUB_t g_stub_context = MY_STUB_MAX;

/** Global variable containing the number of calls to the user's callback function ' */
static volatile unsigned int g_usr_callback_counter = 0u;

/** 
 * 'cmock_num_calls' cannot be reset mid-test, needs to be implemented manually.
 * Also, cmock_num_calls gets messed up when a callback is linked to different stubs,
 * like with i2c_write and i2c_read, both link to the same callback - 'my_stub_function'.
*/
static unsigned int g_callback_num_calls = 0u;

/* ==== STATIC FUNCTION PROTOTYPES ===== */

static I2C_RETURN_t my_stub_function(const I2C_INSTANCE_t instance,
                                        const i2c_transfer_t* const transfer,
                                        int cmock_num_calls);
static void my_user_callback(void);

/* ==== UNIT TEST FUNCTIONS ===== */

void setUp(void) {
    // Reset the ADS internal states
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_deinit());
    g_callback_num_calls = 0u;
    g_usr_callback_counter = 0u;
}
void tearDown(void) {}

/* ==== INPUT VALIDATION ==== */

void test_ads7128_input_validation_init(void) {
    // 1. Wrong array size, greater that the maximum
    uint16_t size = ADS7128_MAX_CFG_ENTRIES + 1;
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BAD_PARAM,
                            ads7128_init(ads7128_user_reg_cfg, size, 0, NULL));

    // 2. Less than the  minimum
    if (ADS7128_MIN_CFG_ENTRIES > 0) {
        size = ADS7128_MIN_CFG_ENTRIES - 1;
    }
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BAD_PARAM,
                            ads7128_init(ads7128_user_reg_cfg, size, 0, NULL));
}

void test_ads7128_input_validation_read(void) {
    // 1. NULL parameter passed, should return 'ADS7128_RETURN_NULL'
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_NULL, ads7128_read(NULL));
}

void test_ads7128_input_validation_status(void) {
    // 1. NULL parameter passed, should return 'ADS7128_RETURN_NULL'
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_NULL, ads7128_get_status(NULL));
}

/* ==== ERROR HANDLING AND INTERNAL STATES ==== */

void test_ads7128_error_handling_init(void) {
    int ads7128_run_counter = ADS7128_RUNNER_COUNTER;
    ADS7128_STATE_t state;

    // 1. if i2c_init() fails, return 'ADS7128_RETURN_INTERNAL_ERROR'
    i2c_init_ExpectAnyArgsAndReturn(I2C_RETURN_INVALID_INSTANCE);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_INTERNAL_ERROR, ads7128_init(NULL, 0U, 0U, NULL));

    // 2. if i2c_write() fails, keep internal state in 'ADS7128_STATE_UNINIT'
    i2c_init_IgnoreAndReturn(I2C_RETURN_SUCCESS);
    i2c_write_ExpectAnyArgsAndReturn(I2C_RETURN_INVALID_INSTANCE);

    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_init(NULL, 0U, 0U, NULL));
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_UNINIT, state);

    // 3. if i2c_read() fails, keep internal state in 'ADS7128_STATE_UNINIT'
    i2c_init_IgnoreAndReturn(I2C_RETURN_SUCCESS);
    g_stub_context = MY_STUB_I2C_OK;
    i2c_write_Stub(my_stub_function);
    i2c_read_ExpectAnyArgsAndReturn(I2C_RETURN_INVALID_INSTANCE);

    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_init(NULL, 0U, 0U, NULL));
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_UNINIT, state);

    // 4. if i2c_callback() returns failure, keep internal state in 'ADS7128_STATE_UNINIT'
    i2c_init_IgnoreAndReturn(I2C_RETURN_SUCCESS);
    g_stub_context = MY_STUB_I2C_NOT_OK;
    i2c_write_Stub(my_stub_function);

    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_init(NULL, 0U, 0U, NULL));
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_UNINIT, state);

    // 5. if register value mismatches, keep internal state in 'ADS7128_STATE_UNINIT'
    i2c_init_IgnoreAndReturn(I2C_RETURN_SUCCESS);
    g_stub_context = MY_STUB_ERROR_HANDLING_INIT_CFG;
    i2c_write_Stub(my_stub_function);
    i2c_read_Stub(my_stub_function);

    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_init(NULL, 0U, 0U, NULL));
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_UNINIT, state);
}

void test_ads7128_ErrorHandling_reset(void) {
    int ads7128_run_counter = ADS7128_RUNNER_COUNTER;
    ADS7128_STATE_t last_state, state;

    // 1. if i2c_write() fails, keep previous internal state
    i2c_write_ExpectAnyArgsAndReturn(I2C_RETURN_INVALID_INSTANCE);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_READY, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&last_state));

    // 1.1 start the test sequence
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_reset());
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(last_state, state);

    // 2. if i2c_callback() returns failure, keep previous internal state
    g_stub_context = MY_STUB_I2C_NOT_OK;
    i2c_write_Stub(my_stub_function);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_READY, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&last_state));

    // 2.1 start the test sequence
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_reset());
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(last_state, state);
}

void test_ads7128_ErrorHandling_start(void) {
    int ads7128_run_counter = ADS7128_RUNNER_COUNTER;
    ADS7128_STATE_t state;
    uint16_t ads7128_data[ADS7128_MAX_CH] = {0};

    // 1. if i2c_write() fails, the ads7128 readings should be invalid [ 0xFFFF ]
    i2c_write_IgnoreAndReturn(I2C_RETURN_INVALID_INSTANCE);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_READY, NULL));

    // 1.1 start the test sequence
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_start());
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_RUNNING, state);

    // 1.2 assess the results
    memset(ads7128_data, 0x0, sizeof(ads7128_data));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_read(ads7128_data));
    for (uint16_t i = 0u; i < ADS7128_MAX_CH; i++) {
         TEST_ASSERT_EQUAL_UINT16(RMS_BAD_VALUE, ads7128_data[i]);
    }

    // 2. if i2c_read() fails, the ads7128 readings should be invalid [ 0xFFFF ]
    g_stub_context = MY_STUB_I2C_OK;
    i2c_write_Stub(my_stub_function);
    i2c_read_IgnoreAndReturn(I2C_RETURN_INVALID_INSTANCE);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_READY, NULL));

    // 2.1 start the test sequence
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_start());
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_RUNNING, state);

    // 2.2 assess the results
    memset(ads7128_data, 0x0, sizeof(ads7128_data));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_read(ads7128_data));
    for (uint16_t i = 0u; i < ADS7128_MAX_CH; i++) {
         TEST_ASSERT_EQUAL_UINT16(RMS_BAD_VALUE, ads7128_data[i]);
    }

    // 3. if i2c_callback() returns failure, the ads7128 readings should be invalid [ 0xFFFF ]
    g_stub_context = MY_STUB_I2C_NOT_OK;
    i2c_write_Stub(my_stub_function);

    // 3.1 start the test sequence
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_READY, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_start());
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_RUNNING, state);

    // 3.2 assess the results
    memset(ads7128_data, 0x0, sizeof(ads7128_data));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_read(ads7128_data));
    for (uint16_t i = 0u; i < ADS7128_MAX_CH; i++) {
         TEST_ASSERT_EQUAL_UINT16(RMS_BAD_VALUE, ads7128_data[i]);
    }
}

void test_ads7128_ErrorHandling_calibration(void) {
    int ads7128_run_counter = ADS7128_RUNNER_COUNTER;
    ADS7128_STATE_t last_state, state;

    // 1. if i2c_write() fails, keep previous internal state
    i2c_write_IgnoreAndReturn(I2C_RETURN_INVALID_INSTANCE);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_READY, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&last_state));

    // 1.1 start the test sequence
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_calibration());
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(last_state, state);

    // 2. if i2c_callback() returns failure, keep previous internal state
    g_stub_context = MY_STUB_I2C_NOT_OK;
    i2c_write_Stub(my_stub_function);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_READY, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&last_state));

    // 2.1 start the test sequence
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_calibration());
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(last_state, state);
}

/* ==== STATE READINESS ==== */

void test_ads7128_Readiness_init(void) {
    // 1. if module already initialised, return ADS7128_RETURN_ALREADY_INIT
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_READY, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_ALREADY_INIT, ads7128_init(NULL, 0U, 0U, NULL));

    // 2. if module is busy, return ADS7128_RETURN_BUSY
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_BUSY, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BUSY, ads7128_init(NULL, 0U, 0U, NULL));

    // 3. if module is running, return ADS7128_RETURN_BUSY
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_RUNNING, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BUSY, ads7128_init(NULL, 0U, 0U, NULL));
}

void test_ads7128_Readiness_reset(void) {
    // 1. if module not initialised, return ADS7128_RETURN_NOT_INIT
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_NOT_INIT, ads7128_reset());

    // 2. if module is busy, return ADS7128_RETURN_BUSY
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_BUSY, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BUSY, ads7128_reset());

    // 3. if module is running, return ADS7128_RETURN_BUSY
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_RUNNING, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BUSY, ads7128_reset());
}

void test_ads7128_Readiness_start(void) {
    // 1. if module not initialised, return ADS7128_RETURN_NOT_INIT
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_NOT_INIT, ads7128_start());

    // 2. if module is busy, return ADS7128_RETURN_BUSY
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_BUSY, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BUSY, ads7128_start());
}

void test_ads7128_Readiness_stop(void) {
    // 1. if module not initialised, return ADS7128_RETURN_NOT_INIT
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_NOT_INIT, ads7128_stop());

    // 2. if module is busy, return ADS7128_RETURN_BUSY
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_BUSY, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BUSY, ads7128_stop());
}

void test_ads7128_Readiness_calibration(void) {
    // 1. if module not initialised, return ADS7128_RETURN_NOT_INIT
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_NOT_INIT, ads7128_calibration());

    // 2. if module is busy, return ADS7128_RETURN_BUSY
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_BUSY, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BUSY, ads7128_calibration());

    // 3. if module is running, return ADS7128_RETURN_BUSY
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_RUNNING, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BUSY, ads7128_calibration());
}

/* ==== FUNCTIONALITY AND INTERNAL STATES ==== */

void test_ads7128_Functionality_init(void) {
    int ads7128_run_counter = ADS7128_RUNNER_COUNTER * 2;
    ADS7128_STATE_t state;

    // 1. check internal state is 'ADS7128_STATE_BUSY' while performing the task
    i2c_init_ExpectAnyArgsAndReturn(I2C_RETURN_SUCCESS);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_UNINIT, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_init(NULL, 0U, 0U, NULL));
    ads7128_run();
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_BUSY, state);

    // 2. initialise the module with DEFAULTS
    i2c_init_ExpectAnyArgsAndReturn(I2C_RETURN_SUCCESS);
    g_stub_context = MY_STUB_FUNCTIONALITY_INIT_CFG_DEFAULTS;
    i2c_write_Stub(my_stub_function);
    i2c_read_Stub(my_stub_function);

    // 2.1 start the test sequence
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_UNINIT, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_init(NULL, 0U, 0U, NULL));
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }

    // 2.2 at the end of initialisation, the state should be 'ADS7128_STATE_READY'
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_READY, state);

    // 3. initialise the module with USER DATA
    i2c_init_ExpectAnyArgsAndReturn(I2C_RETURN_SUCCESS);
    g_stub_context = MY_STUB_FUNCTIONALITY_INIT_CFG_USER_DATA;
    g_callback_num_calls = 0u;
    i2c_write_Stub(my_stub_function);
    i2c_read_Stub(my_stub_function);

    // 3.1 start the test sequence
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_UNINIT, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_init(ads7128_user_reg_cfg, USER_CONFIG_ARRAY_SIZE, 0u, NULL));
    for (int i = 0; i < (ads7128_run_counter * 2u); i++) {
        ads7128_run();
    }

    // 3.2 At the end of the initialisation, the state should be 'ADS7128_STATE_READY'
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_READY, state);
}

void test_ads7128_Functionality_deinit(void) {
    ADS7128_STATE_t set_state, get_state;
    set_state = ADS7128_STATE_UNINIT;

    while (++set_state < ADS7128_STATE_MAX) {
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                                ads7128_set_internal_status(set_state, NULL));
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_deinit());
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&get_state));
        TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_UNINIT, get_state);
    }
}


void test_ads7128_Functionality_reset(void) {
    int ads7128_run_counter = ADS7128_RUNNER_COUNTER;
    ADS7128_STATE_t state;

    // 1. check internal state is 'ADS7128_STATE_BUSY' while performing the task
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_READY, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_reset());
    ads7128_run();
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_BUSY, state);

    // 2. check the reset command is sent over I2C
    g_stub_context = MY_STUB_FUNCTIONALITY_RESET;
    i2c_write_Stub(my_stub_function);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_READY, NULL));

    // 2.1 start the test sequence
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_reset());
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }

    // 2.2 after a successful reset, the state should be 'ADS7128_STATE_UNINIT'
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_UNINIT, state);
}

void test_ads7128_Functionality_start(void) {
    int ads7128_run_counter = ADS7128_RUNNER_COUNTER * 20;
    ADS7128_STATE_t state;
    uint16_t ads7128_data[ADS7128_MAX_CH] = {0};

    // 1. sanity check: ads7128 data is invalid
    memset(ads7128_data, 0x0, sizeof(ads7128_data));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_read(ads7128_data));
    for (uint16_t i = 0u; i < ADS7128_MAX_CH; i++) {
         TEST_ASSERT_EQUAL_UINT16(RMS_BAD_VALUE, ads7128_data[i]);
    }

    // 2. check internal state is 'ADS7128_STATE_RUNNING' while performing the task
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_READY, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_start());
    ads7128_run();
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_RUNNING, state);

    // 3. kick off the measurements
    g_stub_context = MY_STUB_FUNCTIONALITY_START;
    i2c_write_Stub(my_stub_function);
    i2c_read_Stub(my_stub_function);
    g_usr_callback_counter = 0u;

    // 3.1 start the test sequence
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_READY, my_user_callback));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_start());
    for (int i = 0; i < ads7128_run_counter; i++) {
        ads7128_run();
    }

    // 3.2 at the end of each reading cycle, the user callback must be called
    TEST_ASSERT_GREATER_THAN_UINT(0u, g_usr_callback_counter);

    // 3.3 at any moment durning 'measuring', the state should be 'ADS7128_STATE_RUNNING'
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_RUNNING, state);

    // 4. check the module under test filled its buffer with RMS readings
    memset(ads7128_data, 0x0, sizeof(ads7128_data));
    uint16_t expected_value = RMS_LSB_VALUE | (RMS_MSB_VALUE << 8u);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_read(ads7128_data));
    for (uint16_t i = 0u; i < ADS7128_MAX_CH; i++) {
         TEST_ASSERT_EQUAL_UINT16(expected_value, ads7128_data[i]);
    }
}

void test_ads7128_Functionality_stop(void) {
    ADS7128_STATE_t set_state, get_state;
    set_state = ADS7128_STATE_UNINIT;

    while (++set_state < ADS7128_STATE_BUSY) {
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                                ads7128_set_internal_status(set_state, NULL));
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_stop());
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&get_state));
        TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_READY, get_state);
    }
}

void test_ads7128_Functionality_status(void) {
    ADS7128_STATE_t set_state, get_state;

    for (set_state = ADS7128_STATE_UNINIT; set_state < ADS7128_STATE_MAX; set_state++) {
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                                ads7128_set_internal_status(set_state, NULL));
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&get_state));
        TEST_ASSERT_EQUAL_UINT16(set_state, get_state);
    }
}

/* ==== STATIC FUNCTIONS ===== */

/**
 * @brief This callback is passed by the ADS7128 to the I2C driver, and it's called
 * by the I2C driver when a transaction is finished. Taking control of it means we can
 * inject/inspect data passed back and forth between the ADS7128 and the I2C driver.
 * 
 * @param instance, I2C instance
 * @param transfer, pointer to the I2C transfer structure
 * @param cmock_num_calls, this is handle by cmock, and is a counter for the callback calls 
 * @return I2C_RETURN_t, indicating success or error.  
 */
static I2C_RETURN_t my_stub_function(const I2C_INSTANCE_t instance,
                                    const i2c_transfer_t* const transfer,
                                    int cmock_num_calls) {
    I2C_RETURN_t rc = I2C_RETURN_SUCCESS;
    uint8_t tx_buf[10u];
    uint8_t rx_buf[10u];
    bool b_callback_return_value = false;

    switch (g_stub_context) {
    case MY_STUB_I2C_OK:
        // calls the ads7128 callback to notify the I2C transaction was successful
        b_callback_return_value = true;
        break;

    case MY_STUB_I2C_NOT_OK:
        // calls the ads7128 callback to notify the I2C transaction failed
        b_callback_return_value = false;
        break;

    case MY_STUB_ERROR_HANDLING_INIT_CFG:
        // There's no register value 0x00FF in the default config array
        // so this will force an internal failure while checking the ADS7128 configuration
        transfer->rxBuffer[0u] = 0x00FF;
        b_callback_return_value = true;
        break;

    case MY_STUB_FUNCTIONALITY_INIT_CFG_DEFAULTS:
        /** 
         * Initialisation with defaults or user data is very similar, only
         * the array and size passed are different, so they can be both handled
         * with the same code, with just a few tweaks.
        */
    case MY_STUB_FUNCTIONALITY_INIT_CFG_USER_DATA: {
        /**
         * The initialisation is done in 4 steps:
         * 1. ADS7128 device configuration - over I2C, using write frames only.
         * 2. Read the back configuration - over I2C, using write & read frames.
         * 3. ADS7218 device calibration - over I2C, using write frames only.
         * 4. Poll calibration status - over I2C, using write & read frames.
         */
        static unsigned int execution_step = 0u;
        static unsigned int transaction_counter = 0u;
        static bool is_i2c_write = true;
        static unsigned int index = 0u;
        static ads7128_reg_t g_ads_reg[USER_CONFIG_ARRAY_SIZE];

        // differentiate between user data and defaults at the beginning of each test
        if (0u == g_callback_num_calls) {
            execution_step = 1u;
            if (MY_STUB_FUNCTIONALITY_INIT_CFG_DEFAULTS == g_stub_context) {
                // write frames only, 1 transaction per register, of array of registers
                transaction_counter = DEFAULT_CONFIG_ARRAY_SIZE;
                memcpy(g_ads_reg,
                        g_ads_reg_default,
                        sizeof(ads7128_reg_t) * DEFAULT_CONFIG_ARRAY_SIZE);
            } else {
                transaction_counter = USER_CONFIG_ARRAY_SIZE;
                memcpy(g_ads_reg,
                        ads7128_user_reg_cfg,
                        sizeof(ads7128_reg_t) * USER_CONFIG_ARRAY_SIZE);
            }
            is_i2c_write = true;
            index = 0u;
        }

        if (1u == execution_step) {
            // check if the module is passing the right registers to the I2C driver
            TEST_ASSERT_EQUAL_UINT16(g_ads_reg[index].addr, transfer->txBuffer[1u]);
            TEST_ASSERT_EQUAL_UINT16(g_ads_reg[index].value, transfer->txBuffer[2u]);
            index++;

            if (0u == (--transaction_counter)) {
                execution_step++;
                // 2 transactions per register: i2c_write + i2c_read, of arrays of registers
                if (MY_STUB_FUNCTIONALITY_INIT_CFG_DEFAULTS == g_stub_context) {
                    transaction_counter = DEFAULT_CONFIG_ARRAY_SIZE * 2u;
                } else {
                    transaction_counter = USER_CONFIG_ARRAY_SIZE * 2u;
                }
                is_i2c_write = true;
                index = 0u;
            }
        } else if (2u == execution_step) {
            if (is_i2c_write) {
                // check the ADS7128 register's address being passed to the I2C driver
                TEST_ASSERT_EQUAL_UINT16(g_ads_reg[index].addr, transfer->txBuffer[1u]);
            } else {
                // mock the I2C driver READ frame with the configuration defaults
                transfer->rxBuffer[0] = g_ads_reg[index].value;
                index++;
            }
            if (0u == (--transaction_counter)) {
                execution_step++;
                // only write frames, 1 transaction per register, single register
                transaction_counter = 1u;
                is_i2c_write = true;
                index = 0u;
            }

            // next frame is of the other type
            is_i2c_write = !is_i2c_write;
        } else if (3u == execution_step) {
            TEST_ASSERT_EQUAL_UINT16(GEN_CFG_R, transfer->txBuffer[1u]);
            TEST_ASSERT_EQUAL_UINT16(GEN_CFG_CAL_F, transfer->txBuffer[2u]);

            if (0u == (--transaction_counter)) {
                execution_step++;
                // 2 transactions per register: i2c_write + i2c_read, single register
                transaction_counter = 1u * 2u;
                is_i2c_write = true;
                index = 0u;
            }
        } else if (4u == execution_step) {
            if (is_i2c_write) {
                // check the calibration register's address being passed to the I2C driver
                TEST_ASSERT_EQUAL_UINT16(GEN_CFG_R, transfer->txBuffer[1u]);
            } else {
                // mock the I2C driver READ frame with the calibration register
                // '0u' will make the comparison pass
                transfer->rxBuffer[0] = 0u;
            }
            if (0u == (--transaction_counter)) {
                execution_step++;
                // write frames only, 1 transaction per register, single register
                transaction_counter = 1u;
                is_i2c_write = true;
                index = 0u;
            }

            // next frame is of the other type
            is_i2c_write = !is_i2c_write;
        } else {
            // do nothing
        }

        b_callback_return_value = true;
        g_callback_num_calls++;
    }break;

    case MY_STUB_FUNCTIONALITY_RESET:
        // check the ADS is passing the sw reset code to the I2C driver
        TEST_ASSERT_EQUAL_UINT16(GEN_CFG_R, transfer->txBuffer[1u]);
        TEST_ASSERT_EQUAL_UINT16(GEN_CFG_RST_F, transfer->txBuffer[2u]);

        b_callback_return_value = true;
        break;

    case MY_STUB_FUNCTIONALITY_START: {
        /**
         * The RMS measurement is done in done in 2 steps:
         * 1. RMS configuration, start, and stop computation - over I2C, using write frames only.
         * 2. RMS result read - over I2C, using write & read frames.
         * 3. Do this for each channel id, wrapping to channel zero when last channel is reached
         */
        static unsigned int execution_step = 0u;
        static unsigned int transaction_counter = 0u;
        static bool is_i2c_write = true;
        static unsigned int index = 0u;
        static uint16_t channel_id = ADS7128_MAX_CH;

        // differentiate between user data and defaults at the beginning of each test
        if (0u == g_callback_num_calls) {
            execution_step = 1u;
            // only write frames, 1 transaction per register, of array of registers
            transaction_counter = RMS_CONFIG_ARRAY_SIZE;
            is_i2c_write = true;
            index = 0u;
            if (++channel_id >= ADS7128_MAX_CH) {
                channel_id = 0u;
            }
        }

        if (1u == execution_step) {
            // check if the module is passing the RMS configuration registers to the I2C driver
            TEST_ASSERT_EQUAL_UINT16(g_ads_rms_cfg[index].addr, transfer->txBuffer[1u]);
            if (RMS_CFG_R == g_ads_rms_cfg[index].addr) {
                TEST_ASSERT_EQUAL_UINT16(channel_id << 4u, transfer->txBuffer[2u]);
            } else if (AUTO_SEQ_CH_SEL_R == g_ads_rms_cfg[index].addr) {
                TEST_ASSERT_EQUAL_UINT16(1u << channel_id, transfer->txBuffer[2u]);
            } else  {
                TEST_ASSERT_EQUAL_UINT16(g_ads_rms_cfg[index].value, transfer->txBuffer[2u]);
            }
            index++;

            if (0u == (--transaction_counter)) {
                execution_step++;
                // 2 transactions per register: i2c_write + i2c_read, of arrays of registers
                transaction_counter = 1u * 2u;
                is_i2c_write = true;
                index = 0u;
            }
        } else if (2u == execution_step) {
            if (is_i2c_write) {
                // check the RMS_RESULT_LSB_R's register address is being passed to the I2C driver
                TEST_ASSERT_EQUAL_UINT16(RMS_RESULT_LSB_R, transfer->txBuffer[1u]);
            } else {
                // mock the I2C driver READ frame with the configuration defaults
                transfer->rxBuffer[0u] = RMS_LSB_VALUE;
                transfer->rxBuffer[1u] = RMS_MSB_VALUE;
                index++;
            }
            if (0u == (--transaction_counter)) {
                execution_step++;
            }
            // switch to the other frame type
            is_i2c_write = !is_i2c_write;
        } else {
            // do nothing
        }

        // if current channel is read, move to the next one
        if (3u == execution_step) {
            g_callback_num_calls = 0u;
        } else {
            g_callback_num_calls++;
        }

        b_callback_return_value = true;
    }break;

    default:
        TEST_PRINTF("Callback code: %d", g_stub_context);
        TEST_FAIL_MESSAGE("Callback code unknown!");
        break;
    }

    transfer->callback(b_callback_return_value);
    return rc;
}

/**
 * @brief This callback is passed to the ADS module.
 * It uses a global variable to share the result of the
 * callback across the module.
 */
static void my_user_callback(void) {
    g_usr_callback_counter++;
}
#endif TEST // TEST