/**
 * @file interrupt.h
 * @date 13 Dec, 2023
 * @author Leonel Gomes (Enoda Ltd.)
 * @copyright Copyright (C) 2023 Enoda Ltd. - All Rights Reserved
 * 
 * This file is used by CMock to generate all the required mocks for the unit tests.
 * 
 * This file is our own implementation of the original file [ interrupt.h ] included in
 * TI driverlib. Its content was cherry picked [ copy/pasted ] from the original, to have just
 * the minimum required by CMock to mock the dependencies of the module under test.
 * 
 * Mocking the dependencies of the module under test is a critical aspect of unit testing
 * on the host. This removes hardware dependency, and makes testing faster.
 * 
 * Given other modules [ specially drivers ] also include this header, it's expected to
 * edit this file as more unit tests are added to the pool.
 * 
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

#ifndef INTERRUPT_H
#define INTERRUPT_H

#define INTERRUPT_ACK_GROUP8    0x80U   //!< Acknowledge PIE Interrupt Group 8

void Interrupt_clearACKGroup(uint16_t group);

#endif // INTERRUPT_H