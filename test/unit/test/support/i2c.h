/**
 * @file i2c.h
 * @date 13 Dec, 2023
 * @author Leonel Gomes (Enoda Ltd.)
 * @copyright Copyright (C) 2023 Enoda Ltd. - All Rights Reserved
 * 
 * This file is used by CMock to generate all the required mocks for the unit tests.
 * 
 * This file is our own implementation of the original file [ I2C.h ] included in TI driverlib.
 * Its content was cherry picked [ copy/pasted ] from the original, to have just the minimum
 * required by CMock to mock the dependencies of the module under test.
 * 
 * Mocking the dependencies of the module under test is a critical aspect of unit testing
 * on the host. This removes hardware dependency, and makes testing faster.
 * 
 * Given other modules [ specially drivers ] also include this header, it's expected to
 * edit this file as more unit tests are added to the pool.
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

#ifndef I2C_H
#define I2C_H

#include <stdint.h>
#include <stdbool.h>

#define I2CA_BASE   0x00007300U
#define I2CB_BASE   0x00007340U

#define I2C_CONTROLLER_SEND_MODE    0x0600U //!< Controller-transmitter mode
#define I2C_CONTROLLER_RECEIVE_MODE 0x0400U //!< Controller-receiver mode

#define I2C_INT_ARB_LOST        0x00001U //!< Arbitration-lost interrupt
#define I2C_INT_NO_ACK          0x00002U //!< NACK interrupt
#define I2C_INT_REG_ACCESS_RDY  0x00004U //!< Register-access-ready interrupt
#define I2C_INT_RX_DATA_RDY     0x00008U //!< Receive-data-ready interrupt
#define I2C_INT_TX_DATA_RDY     0x00010U //!< Transmit-data-ready interrupt
#define I2C_INT_STOP_CONDITION  0x00020U //!< Stop condition detected
#define I2C_INT_ADDR_TARGET      0x00200U //!< Addressed as target interrupt
#define I2C_INT_RXFF            0x10000U //!< RX FIFO level interrupt
#define I2C_INT_TXFF            0x20000U //!< TX FIFO level interrupt

#define I2C_STR_INTMASK     ((uint16_t)I2C_INT_ARB_LOST |                      \
                             (uint16_t)I2C_INT_NO_ACK |                        \
                             (uint16_t)I2C_INT_REG_ACCESS_RDY |                \
                             (uint16_t)I2C_INT_RX_DATA_RDY |                   \
                             (uint16_t)I2C_INT_TX_DATA_RDY |                   \
                             (uint16_t)I2C_INT_STOP_CONDITION |                \
                             (uint16_t)I2C_INT_ADDR_TARGET)

typedef enum
{
    I2C_FIFO_TXEMPTY    = 0x0000U,      //!< Transmit FIFO empty
    I2C_FIFO_TX0        = 0x0000U,      //!< Transmit FIFO empty
    I2C_FIFO_TX1        = 0x0001U,      //!< Transmit FIFO 1/16 full
    I2C_FIFO_TX2        = 0x0002U,      //!< Transmit FIFO 2/16 full
    I2C_FIFO_TX3        = 0x0003U,      //!< Transmit FIFO 3/16 full
    I2C_FIFO_TX4        = 0x0004U,      //!< Transmit FIFO 4/16 full
    I2C_FIFO_TX5        = 0x0005U,      //!< Transmit FIFO 5/16 full
    I2C_FIFO_TX6        = 0x0006U,      //!< Transmit FIFO 6/16 full
    I2C_FIFO_TX7        = 0x0007U,      //!< Transmit FIFO 7/16 full
    I2C_FIFO_TX8        = 0x0008U,      //!< Transmit FIFO 8/16 full
    I2C_FIFO_TX9        = 0x0009U,      //!< Transmit FIFO 9/16 full
    I2C_FIFO_TX10       = 0x000AU,      //!< Transmit FIFO 10/16 full
    I2C_FIFO_TX11       = 0x000BU,      //!< Transmit FIFO 11/16 full
    I2C_FIFO_TX12       = 0x000CU,      //!< Transmit FIFO 12/16 full
    I2C_FIFO_TX13       = 0x000DU,      //!< Transmit FIFO 13/16 full
    I2C_FIFO_TX14       = 0x000EU,      //!< Transmit FIFO 14/16 full
    I2C_FIFO_TX15       = 0x000FU,      //!< Transmit FIFO 15/16 full
    I2C_FIFO_TX16       = 0x0010U,      //!< Transmit FIFO full
    I2C_FIFO_TXFULL     = 0x0010U       //!< Transmit FIFO full
} I2C_TxFIFOLevel;

typedef enum
{
    I2C_FIFO_RXEMPTY    = 0x0000U,      //!< Receive FIFO empty
    I2C_FIFO_RX0        = 0x0000U,      //!< Receive FIFO empty
    I2C_FIFO_RX1        = 0x0001U,      //!< Receive FIFO 1/16 full
    I2C_FIFO_RX2        = 0x0002U,      //!< Receive FIFO 2/16 full
    I2C_FIFO_RX3        = 0x0003U,      //!< Receive FIFO 3/16 full
    I2C_FIFO_RX4        = 0x0004U,      //!< Receive FIFO 4/16 full
    I2C_FIFO_RX5        = 0x0005U,      //!< Receive FIFO 5/16 full
    I2C_FIFO_RX6        = 0x0006U,      //!< Receive FIFO 6/16 full
    I2C_FIFO_RX7        = 0x0007U,      //!< Receive FIFO 7/16 full
    I2C_FIFO_RX8        = 0x0008U,      //!< Receive FIFO 8/16 full
    I2C_FIFO_RX9        = 0x0009U,      //!< Receive FIFO 9/16 full
    I2C_FIFO_RX10       = 0x000AU,      //!< Receive FIFO 10/16 full
    I2C_FIFO_RX11       = 0x000BU,      //!< Receive FIFO 11/16 full
    I2C_FIFO_RX12       = 0x000CU,      //!< Receive FIFO 12/16 full
    I2C_FIFO_RX13       = 0x000DU,      //!< Receive FIFO 13/16 full
    I2C_FIFO_RX14       = 0x000EU,      //!< Receive FIFO 14/16 full
    I2C_FIFO_RX15       = 0x000FU,      //!< Receive FIFO 15/16 full
    I2C_FIFO_RX16       = 0x0010U,      //!< Receive FIFO full
    I2C_FIFO_RXFULL     = 0x0010U       //!< Receive FIFO full
} I2C_RxFIFOLevel;

bool I2C_isBusBusy(uint32_t base);
uint16_t I2C_getData(uint32_t base);
void I2C_enableModule(uint32_t base);
void I2C_setConfig(uint32_t base, uint16_t config);
void I2C_setTargetAddress(uint32_t base, uint16_t targetAddr);
void I2C_setDataCount(uint32_t base, uint16_t count);
void I2C_enableFIFO(uint32_t base);
void I2C_putData(uint32_t base, uint16_t data);
void I2C_setFIFOInterruptLevel(uint32_t base, I2C_TxFIFOLevel txLevel, I2C_RxFIFOLevel rxLevel);

void I2C_clearInterruptStatus(uint32_t base, uint32_t intFlags);
void I2C_enableInterrupt(uint32_t base, uint32_t intFlags);
void I2C_sendStartCondition(uint32_t base);
void I2C_setConfig(uint32_t base, uint16_t config);
void I2C_sendStopCondition(uint32_t base);
void I2C_disableFIFO(uint32_t base);
uint32_t I2C_getInterruptStatus(uint32_t base);
void I2C_disableInterrupt(uint32_t base, uint32_t intFlags);
void I2C_clearInterruptStatus(uint32_t base, uint32_t intFlags);

#endif // I2C_H