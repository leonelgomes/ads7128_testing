/**
 * @file test_dsp1_i2c.c
 * @date 13 Dec, 2023
 * @author Leonel Gomes [ Enoda Ltd. ]
 * @copyright Copyright (C) 2023 Enoda Ltd. - All Rights Reserved
 * 
 * This file contains the declarations for the test functions and
 * test-specific elements related to the I2C driver module. It uses
 * the Unity test framework for test assertions and the CMock library
 * for mocking dependencies.
 *
 * @note Ensure that the necessary mock headers and the I2C driver
 *       header are included appropriately in this file.
 *
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

/* ==== INCLUDES ===== */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "unity.h"
#include "../../../drivers/dsp1_i2c.h"
#include "mock_i2c.h"
#include "mock_board.h"
#include "mock_interrupt.h"

/* ==== DEFINES ===== */

#define I2C_ADDR    (0x13)

/* ==== TYPEDEFS ===== */

/* ==== GLOBAL VARIABLES ===== */

static uint16_t g_tx_buffer[I2C_MAX_DATA_SIZE] = {0};
static uint16_t g_rx_buffer[I2C_MAX_DATA_SIZE] = {0};
static bool volatile g_callback_invoked = false;
static bool volatile g_callback_result = false;

/* ==== FUNCTION PROTOTYPES ===== */

static void callback_function(bool res);
static void ignore_mocks_group(void);

/* ==== UNIT TEST FUNCTIONS ===== */

void setUp(void) {}

void tearDown(void) {}

/**
 * @brief INITIALISATION, test group
 * 
 * - Verify that returned status is correct --- OK
 * - Verify internal state of the driver is correct --- OK
 */
void test_dsp_i2c_init(void) {
    // ignore any calls to the following mocks:
    I2C_enableModule_Ignore();

    // 1. initialise the module under test [ MUT ] with wrong instance
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_INVALID_INSTANCE, i2c_init(I2C_INSTANCE_MAX));

    // 2. initialise [ MUT ] with the right instance
    I2C_STATE_t state = I2C_STATE_MAX;
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_init(I2C_INSTANCE_A));
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_READY, state);
}

/**
 * @brief READ & WRITE OPERATIONS, test group
 * 
 * - Test validation is done correctly --- OK
 * - Verify that data can be received successfully --- OK
 * - Test that the callback function is called with the right parameters --- OK
 * - Verify that data can be written successfully --- OK
 * - Verify that returned status are correct --- OK
 * - Test different data types and sizes --- OK
 * 
 */
void test_dsp_i2c_read_param_validation(void) {
    i2c_transfer_t transfer = {
        .callback = NULL,
        .txBuffer = g_tx_buffer,
        .rxBuffer = g_rx_buffer,
        .dataSize = 0,
        .targetAddr = 0,
    };

    // Ignore parameters passed to 'I2C_isBusBusy()'
    I2C_isBusBusy_IgnoreAndReturn(false);

    // 1. invalid instance
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_INVALID_INSTANCE, i2c_read(I2C_INSTANCE_MAX, NULL));

    // 2. NULL pointers
    // 2.1. NULL transfer structure passed
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_NULL, i2c_read(I2C_INSTANCE_A, NULL));

    // 2.2 txBuffer & rxBuffer
    transfer.txBuffer = NULL;
    transfer.rxBuffer = NULL;
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_NULL, i2c_read(I2C_INSTANCE_A, &transfer));

    // 2.3 txBuffer & rxBuffer
    transfer.txBuffer = g_tx_buffer;
    transfer.rxBuffer = NULL;
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_NULL, i2c_read(I2C_INSTANCE_A, &transfer));

    // 2.4 txBuffer & rxBuffer
    transfer.txBuffer = NULL;
    transfer.rxBuffer = g_rx_buffer;
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_NULL, i2c_read(I2C_INSTANCE_A, &transfer));

    // 3. data size
    // 3.1 data size < min
    transfer.txBuffer = g_tx_buffer;
    transfer.rxBuffer = g_rx_buffer;
    transfer.dataSize = I2C_MIN_DATA_SIZE - 1;
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_INVALID_SIZE, i2c_read(I2C_INSTANCE_A, &transfer));

    // 3.2 data size > max
    transfer.dataSize = I2C_MAX_DATA_SIZE + 1;
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_INVALID_SIZE, i2c_read(I2C_INSTANCE_A, &transfer));

    // 4. null status param
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_NULL, i2c_get_status(I2C_INSTANCE_A, NULL));
}

void test_dsp_i2c_read_frame_received_with_callback(void) {
    i2c_transfer_t transfer = {
        .callback = callback_function,
        .txBuffer = g_tx_buffer,
        .rxBuffer = g_rx_buffer,
        .dataSize = I2C_MAX_DATA_SIZE,
        .targetAddr = I2C_ADDR,
    };
    I2C_STATE_t state = I2C_STATE_MAX;

    // ignore any calls to the following mocks:
    ignore_mocks_group();

    // 1. Reset the driver to its initial state
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_init(I2C_INSTANCE_A));

    // 1.1 Confirm driver state to be ready
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_READY, state);

    // 2. Send read frame
    I2C_setTargetAddress_Expect(0, transfer.targetAddr);
    I2C_setTargetAddress_IgnoreArg_base();
    I2C_setDataCount_Expect(0, transfer.dataSize);
    I2C_setDataCount_IgnoreArg_base();
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_read(I2C_INSTANCE_A, &transfer));

    // 2.1 Driver should be busy
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_BUSY, state);

    // 3. Handle ISR with expected interrupt flags [I2C_INT_RXFF]
    I2C_getInterruptStatus_ExpectAndReturn(0, I2C_INT_RXFF);
    I2C_getInterruptStatus_IgnoreArg_base();

    // the callback [ if invoked ] should change these flags
    g_callback_invoked = false;
    g_callback_result = false;

    // 3.1 mock read rx fifo with pseudo-random data
    srand(NULL);
    uint8_t data_array[transfer.dataSize];

    for (uint16_t i = 0; i < transfer.dataSize; i++) {
        data_array[i] = rand()  % 256;
        I2C_getData_ExpectAndReturn(0, data_array[i]);
        I2C_getData_IgnoreArg_base();
    }

    // 3.2 Call ISR [ direct call to the API, this is not a mock ]
    INT_i2c0_uc_FIFO_ISR();

    // 3.3 Check the rx buffer contains the pseudo-random data
    for (uint16_t i = 0; i < transfer.dataSize; i++) {
        TEST_ASSERT_EQUAL_UINT8(data_array[i], transfer.rxBuffer[i]);
    }

    // 3.4 Driver should be in 'ready' state
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_READY, state);

    // 4.0 verify callback was invoked by the driver
    TEST_ASSERT_TRUE(g_callback_invoked);
    TEST_ASSERT_TRUE(g_callback_result);
}

void test_dsp_i2c_write_frame_with_callback(void) {
        i2c_transfer_t transfer = {
        .callback = callback_function,
        .txBuffer = g_tx_buffer,
        .rxBuffer = g_rx_buffer,
        .dataSize = I2C_MAX_DATA_SIZE,
        .targetAddr = I2C_ADDR,
    };
    I2C_STATE_t state = I2C_STATE_MAX;

    // ignore any calls to the following mocks:
    ignore_mocks_group();

    // 1. Reset the driver to its initial state
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_init(I2C_INSTANCE_A));

    // 1.1 Confirm driver state to be ready
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_READY, state);

    // 2. Send write frame for writing "dataSize" bytes
    I2C_setConfig_Expect(0, I2C_CONTROLLER_SEND_MODE);
    I2C_setConfig_IgnoreArg_base();
    I2C_setTargetAddress_Expect(0, transfer.targetAddr);
    I2C_setTargetAddress_IgnoreArg_base();
    I2C_setDataCount_Expect(0, transfer.dataSize);
    I2C_setDataCount_IgnoreArg_base();

    // 2.1 load tx fifo with pseudo-random data and check it's later passed to the mock
    srand(NULL);
    uint8_t data_array[transfer.dataSize];
    for (uint16_t i = 0; i < transfer.dataSize; i++) {
        data_array[i] = rand() % 256;
        g_tx_buffer[i] = data_array[i];
        I2C_putData_Expect(0, g_tx_buffer[i]);
        I2C_putData_IgnoreArg_base();
    }

    // 3. start the transaction successfully
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_write(I2C_INSTANCE_A, &transfer));

    // 3.1 Driver state should be busy
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_BUSY, state);

    // 3.2 Should prevent new i2c_write frames, even if the I2C bus is NOT busy
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_BUSY, i2c_write(I2C_INSTANCE_A, &transfer));

    // 4. Handle ISR with the expected interrupt flags [I2C_INT_TXFF]
    I2C_getInterruptStatus_ExpectAndReturn(0, I2C_INT_TXFF);
    I2C_getInterruptStatus_IgnoreArg_base();

    // the callback [ if invoked ] should change these flags
    g_callback_invoked = false;
    g_callback_result = false;

    // 4.1 Call ISR [ this is not a mock ]
    INT_i2c0_uc_FIFO_ISR();

    // 4.2 After the transaction, the driver should be in 'ready' state
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_READY, state);

    // 4.3 verify the callback was invoked by the driver with no errors
    TEST_ASSERT_TRUE(g_callback_invoked);
    TEST_ASSERT_TRUE(g_callback_result);
}

/**
 * @brief ERROR HANDLING, test group
 * 
 * - Test scenarios with the I2C bus busy --- OK
 * - Test error handling for NACKs and other bus errors --- OK
 * - Test the state of the I2C bus after different error conditions [NACK, ARB_LOST] --- OK
 * - Ensure the bus returns to an idle state after error handling --- OK
 */
void test_dsp_i2c_bus_busy_handling(void) {
    i2c_transfer_t transfer = {
        .callback = callback_function,
        .txBuffer = g_tx_buffer,
        .rxBuffer = g_rx_buffer,
        .dataSize = I2C_MAX_DATA_SIZE,
        .targetAddr = I2C_ADDR,
    };
    I2C_STATE_t state = I2C_STATE_MAX;

    // ignore any calls to the following mocks:
    ignore_mocks_group();

    // 1. Reset the driver to its initial state
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_init(I2C_INSTANCE_A));

    // 1.1 Confirm driver state to be ready
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_READY, state);

    // 2. test sending a read and write frames while a transaction is ongoing
    // 2.1 Send READ frame and start a transaction
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_read(I2C_INSTANCE_A, &transfer));

    // 2.2 Send READ frame again, and expect 'busy'
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_BUSY, i2c_read(I2C_INSTANCE_A, &transfer));

    // 2.3 Confirm driver state to be busy
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_BUSY, state);

    // 2.4 Send WRITE frame, and expect 'busy'
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_BUSY, i2c_write(I2C_INSTANCE_A, &transfer));

    // 2.3 Confirm driver state to be busy
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_BUSY, state);
}

void test_dsp_i2c_bus_error_handling(void) {
    i2c_transfer_t transfer = {
        .callback = callback_function,
        .txBuffer = g_tx_buffer,
        .rxBuffer = g_rx_buffer,
        .dataSize = I2C_MAX_DATA_SIZE,
        .targetAddr = I2C_ADDR,
    };
    I2C_STATE_t state = I2C_STATE_MAX;

    // ignore any calls to the following mocks:
    ignore_mocks_group();

    // 1. Reset the driver to its initial state
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_init(I2C_INSTANCE_A));

    // 1.1 Confirm driver state to be ready
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_READY, state);

    // 2. Send read frame
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_read(I2C_INSTANCE_A, &transfer));

    // 2.1 Confirm driver state to be busy
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_BUSY, state);

    // 3. Handle ISR with when 'no ack' error flag is set
    I2C_getInterruptStatus_ExpectAndReturn(0, I2C_INT_NO_ACK);
    I2C_getInterruptStatus_IgnoreArg_base();

    // the callback [ if invoked ] should change these flags
    g_callback_invoked = false;
    g_callback_result = true;

    // 3.1 Call ISR [ this is not a mock ]
    INT_i2c0_uc_ISR();

    // 3.2 the driver should be in 'no ack' error state
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_NO_ACK, state);

    // 3.3 verify the callback was invoked
    TEST_ASSERT_TRUE(g_callback_invoked);
    TEST_ASSERT_FALSE(g_callback_result);

    // 4. Handle ISR with when 'arbitration lost' error flag is set
    I2C_getInterruptStatus_ExpectAndReturn(0, I2C_INT_ARB_LOST);
    I2C_getInterruptStatus_IgnoreArg_base();

    // the callback [ if invoked ] should change these flags
    g_callback_invoked = false;
    g_callback_result = true;

    // 4.1 Call ISR [ this is not a mock ]
    INT_i2c0_uc_ISR();

    // 4.2 the driver should be in 'arb lost' error state
    TEST_ASSERT_EQUAL_UINT16(I2C_RETURN_SUCCESS, i2c_get_status(I2C_INSTANCE_A, &state));
    TEST_ASSERT_EQUAL_UINT16(I2C_STATE_ARB_LOST, state);

    // 4.3 verify callback was invoked by the driver
    TEST_ASSERT_TRUE(g_callback_invoked);
    TEST_ASSERT_FALSE(g_callback_result);
}


/* ==== Utility Functions ===== */

/**
 * @brief This callback only change a set of flags, so we know it was called.
 * 
 * @note These flags should be reset to a different value, before this 
 * function is called.
 * 
 * @param res  Boolean result represents transfer success (true ==> success).
 */
static void callback_function(bool res) {
    g_callback_invoked = true;
    g_callback_result = res;
}

/**
 * @brief This function tells cmock what mocks to ignore.
 * 
 * @note This may hide the flow of calls to mocks, as it's an
 * unordered list, so the sequence of calls may be different between tests.
 * 
 */
static void ignore_mocks_group(void) {
    I2C_enableModule_Ignore();
    I2C_setConfig_Ignore();
    I2C_enableFIFO_Ignore();
    I2C_setFIFOInterruptLevel_Ignore();
    I2C_clearInterruptStatus_Ignore();
    I2C_enableInterrupt_Ignore();
    I2C_sendStartCondition_Ignore();
    I2C_disableInterrupt_Ignore();
    I2C_sendStopCondition_Ignore();
    I2C_disableFIFO_Ignore();
    Interrupt_clearACKGroup_Ignore();
    I2C_setTargetAddress_Ignore();
    I2C_setDataCount_Ignore();
    // always return bus NOT busy
    I2C_isBusBusy_IgnoreAndReturn(false);
}
