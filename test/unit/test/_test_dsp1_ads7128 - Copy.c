/**
 * @file test_dsp1_i2c.c
 * @date 13 Dec, 2023
 * @author Leonel Gomes [ Enoda Ltd. ]
 * @copyright Copyright (C) 2023 Enoda Ltd. - All Rights Reserved
 * 
 * This file contains the declarations for the test functions and
 * test-specific elements related to the I2C driver module. It uses
 * the Unity test framework for test assertions and the CMock library
 * for mocking dependencies.
 *
 * @note Ensure that the necessary mock headers and the I2C driver
 *       header are included appropriately in this file.
 *
 * 
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 */

/* ==== INCLUDES ===== */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "unity.h"
#include "../../../middleware/dsp1_ads7128.h"
#include "mock_dsp1_i2c.h"

/* ==== DEFINES ===== */

/** GENERAL_CFG Register */
#define GEN_CFG_R       (0x01)
/** Software reset all registers to default values Flag */
#define GEN_CFG_RST_F   (0x01)

/** Maximum number of calls to run() before considered a fail */
#define ADS_RUN_CALLS_MAX   (100U)

/* ==== TYPEDEFS ===== */

typedef enum MY_STUB_en {
    STUB_CODE_INIT_DEFAULTS = 0U,
    STUB_CODE_INIT_USER_CFG,
    STUB_CODE_INIT_READ_BACK_CFG,
    STUB_CODE_SW_RESET,
    MY_STUB_MAX,
} MY_STUB_t;

/* ==== GLOBAL VARIABLES ===== */

static MY_STUB_t g_stub_context = MY_STUB_MAX;
static ads7128_reg_t g_init_user_data[ADS7128_MAX_CFG_ENTRIES] = {0};
static volatile int g_usr_callback_success = 0;

/* ==== FUNCTION PROTOTYPES ===== */

static I2C_RETURN_t my_stub_function(const I2C_INSTANCE_t instance,
                                        const i2c_transfer_t* const transfer,
                                        int cmock_num_calls);

static void my_user_callback(bool result);

/* ==== UNIT TEST FUNCTIONS ===== */

void setUp(void) {
    // Initialise user data with dummy values
    for (uint16_t i = 0U; i < ADS7128_MAX_CFG_ENTRIES; i++) {
        g_init_user_data[i].addr = i + 1U;
        g_init_user_data[i].value = (i + 1U) + 100U;
    }

    // Reset the ADS internal states
    ADS7128_STATE_t state = ADS7128_STATE_MAX;
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_UNINIT, 0U, my_user_callback));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_UNINIT, state);

    // reset callback flag
     g_usr_callback_success = 0;
}
void tearDown(void) {}

/* ==== FUNCTIONALITY ===== */
/* ==== BOUNDARY CONDITIONS ===== */
/* ==== ERROR HANDLING ===== */
/* ==== INPUT VALIDATION ===== */
/* ==== STATE CHANGES ===== */
/* ==== PERFORMANCE CONSTRAINS ===== */


/**
 * @brief INITIALISATION, test group
 * 
 * - Verify parameter/state validation --- OK
 * - Verify returned values --- OK
 * - Verify internal state of the driver is correct --- OK
 * - Verify defaults are used if no initialisation data is provided by use --- OK
 * - Verify error handling during the driver initialisation ---  
 * - Verify provided initialisation data is passed correctly to the I2C driver/bus --- OK
 * - Verify the callback is called once the API is served --- OK
 * - Verify reading back configuration --- OK
 */
void test_ads7128_init_validation(void) {
    ads7128_reg_t dummy;
    ADS7128_STATE_t state = ADS7128_STATE_MAX;

    // 1. The ADS should be in 'ADS7128_STATE_UNINIT' state
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_UNINIT, state);

    // 2. Wrong array size passed in
    // 2.1 Greater that the maximum
    uint16_t size = ADS7128_MAX_CFG_ENTRIES + 1;
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BAD_PARAM, ads7128_init(&dummy, size, 0, NULL));
    // 2.2 Less than the  minimum
    if (ADS7128_MIN_CFG_ENTRIES > 0) {
        size = ADS7128_MIN_CFG_ENTRIES - 1;
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BAD_PARAM, ads7128_init(&dummy, size, 0, NULL));
    }

    // 3. Forcing initialisation to fail
    i2c_init_ExpectAnyArgsAndReturn(I2C_RETURN_INVALID_INSTANCE);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_INTERNAL_ERROR, ads7128_init(NULL, 0U, 0U, NULL));

    // 4. If the ADS7128 passes initialisation, its state should be 'ADS7128_STATE_BUSY'
    i2c_init_ExpectAnyArgsAndReturn(I2C_RETURN_SUCCESS);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_init(NULL, 0U, 0U, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_BUSY, state);

    // 5. If busy, should return not accept new command and return 'ADS7128_RETURN_BUSY'
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BUSY, ads7128_init(NULL, 0U, 0U, NULL));
}

void test_ads7128_init_with_defaults(void) {
    ADS7128_STATE_t state = ADS7128_STATE_MAX;

    // 1. Call ADS init() with no configuration data passed in, to force use defaults
    g_stub_context = STUB_CODE_INIT_DEFAULTS;
    i2c_init_ExpectAnyArgsAndReturn(I2C_RETURN_SUCCESS);
    i2c_write_Stub(my_stub_function);

    // 2. The ADS7128 should pass the configuration defaults to the I2C driver 
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_init(NULL, 0U, 0U, my_user_callback));
    g_usr_callback_success = 0;
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_BUSY, state);
    // 2.2 run() x times to handle transitions between states
    // 1x, to init the transaction state machine
    // 2x, per I2C transaction
    // 1x, to end the transaction state machine
    uint16_t count = 1U + (2U * 2U) + 1U;
    for (uint16_t i = 0; i < count; i++) {
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_run());
    }
    // 2.3 The ADS should be in 'ADS7128_STATE_BUSY' state, as it'll still going to do
    // the validation  [ read back configuration ]
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_BUSY, state);
}
void test_ads7128_init_error_handling(void) {
    // This test will check the response of the module under test when the I2C driver
    // returns error during calls to i2c_write()
    ADS7128_STATE_t state = ADS7128_STATE_MAX;

    // 1. Call ADS init(), and make i2c_write() return error
    i2c_init_ExpectAnyArgsAndReturn(I2C_RETURN_SUCCESS);
    i2c_write_ExpectAnyArgsAndReturn(I2C_RETURN_BUSY);

    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_init(NULL, 0U, 0U, NULL));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_BUSY, state);
    // 1.2 run() x times to handle transitions between states
    // 1x, to init the transaction state machine
    // 2x, per I2C transaction
    // 1x, to end the transaction state machine
    uint16_t count = 1U + (2U * 0U) + 1U;
    for (uint16_t i = 0; i < count; i++) {
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_run());
    }
    
    // 1.3 The ADS7128 should be in 'ADS7128_STATE_UNINIT' state, as it failed initialisation
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_UNINIT, state);
}

void test_ads7128_init_with_user_data(void) {
    // First part of the test is to validate configuring ADS7128 with user's configuration data.
    // The second part is to read back the ADS17128 configuration, and do an error matching.

    uint16_t arr_size = ADS7128_MAX_CFG_ENTRIES;
    ADS7128_STATE_t state = ADS7128_STATE_MAX;

    // 1. Stubs override mocks, i.e., will be called every time
    g_stub_context = STUB_CODE_INIT_USER_CFG;
    i2c_init_ExpectAnyArgsAndReturn(I2C_RETURN_SUCCESS);
    i2c_write_Stub(my_stub_function);
    i2c_read_Stub(my_stub_function);

    // 2. The ADS7128 should pass the user's configuration data to the I2C driver 
    g_usr_callback_success = 0;
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_init(g_init_user_data,
                                                                arr_size, 0U, my_user_callback));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_BUSY, state);
    
    // 2.2 run() x times to handle transitions between states
    // 1x, to init the transaction state machine
    // 2x, per I2C transaction
    // 1x, to end the transaction state machine
    uint16_t count = 1U + (2U * ADS7128_MAX_CFG_ENTRIES) + 1U;
    for (uint16_t i = 0; i < count; i++) {
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_run());
    }
    // 2.3 The ADS7128 should be in 'ADS7128_STATE_BUSY' state, as it's still going to do
    // the validation [ read back configuration ]
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_BUSY, state);

    // 3. Read back configuration 
    g_stub_context = STUB_CODE_INIT_READ_BACK_CFG;

    // 3.1 run() x times to handle transitions between states
    // 1x, to init the transaction state machine
    // 2x, per I2C transaction
    // 2x, per configuration register [ write + read operations]
    // 1x, to end the transaction state machine
    count = 1U + (2U * 2U * ADS7128_MAX_CFG_ENTRIES) + 1U;
    for (uint16_t i = 0; i < count; i++) {
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_run());
    }

    // 3.2 The ADS7128 should be in 'ADS7128_ST_READY' state, validation is done,
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_READY, state);

    // 4. The user callback was called with a successful result
    TEST_ASSERT_EQUAL_INT(1, g_usr_callback_success);

    // 5. The module should prevent re-initialisation and return 'ADS7128_RET_ALREADY_INIT'
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_ALREADY_INIT, ads7128_init(NULL, 0U, 0U, NULL));
}
/**
 * @brief DEINITIALISATION, test group
 * 
 * - Verify returned values --- OK
 * - Verify internal state and variables are reset to defaults --- OK
 */
void test_ads7128_deinit(void) {

    ADS7128_STATE_t state = ADS7128_STATE_MAX;   
    
    // 1. De-init should always return 'ADS7128_RETURN_SUCCESS'
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_deinit());

    // 2. The ADS7128 should be in 'ADS7128_STATE_UNINIT' state, after de-initialised
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_UNINIT, state);
}


/**
 * @brief RESET, test group
 * 
 * - Verify parameter/state validation --- OK
 * - Verify returned values --- OK
 * - Verify reset command is is correctly sent [ i2C driver/bus ] --- OK
 */
void test_ads7128_reset_fail(void) {
    ADS7128_STATE_t state = ADS7128_STATE_MAX;

    // 1. Initialisation of the module is required prior to performing a software reset
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_READY, 0U, my_user_callback));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_READY, state);

    // 2. Force the I2C write to return 'I2C_RETURN_BUSY'
    i2c_write_ExpectAnyArgsAndReturn(I2C_RETURN_BUSY);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_reset());
    g_usr_callback_success = 0;

    // 2.1 run() x times to handle transitions between states
    // 1x, per I2C transaction
    uint16_t count = 1U;
    for (uint16_t i = 0U; i < count; i++) {
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_run());
    }

    // 3. The user callback was called with an unsuccessful result
    TEST_ASSERT_EQUAL_INT(-1, g_usr_callback_success);

    // 4. After a software reset fail, the module shouldn't change state
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_READY, state);
}

void test_ads7128_reset_success(void) {
    ADS7128_STATE_t state = ADS7128_STATE_MAX;

    // 1. If the module is not initialised, it should return 'ADS7128_RETURN_NOT_INIT'
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_NOT_INIT, ads7128_reset());
    // 1.1 or if the module is busy, it should return 'ADS7128_RETURN_BUSY'
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                            ads7128_set_internal_status(ADS7128_STATE_BUSY, 0U, my_user_callback));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_BUSY, ads7128_reset());

    // 2. Initialisation of the module is required prior to performing a software reset
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS,
                                ads7128_set_internal_status(ADS7128_STATE_READY, 0U, my_user_callback));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_READY, state);

    // 3. Check the module sends the right reset transaction over I2C
    g_stub_context = STUB_CODE_SW_RESET;
    i2c_write_Stub(my_stub_function);
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_reset());
    g_usr_callback_success = 0;

    // 3.2 run() x times to handle transitions between states
    // 2x, per I2C transaction
    uint16_t count = 2U;
    for (uint16_t i = 0U; i < count; i++) {
        TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_run());
    }

    // 4. The user callback was called with a successful result
    TEST_ASSERT_EQUAL_INT(1, g_usr_callback_success);

    // 5. After a software reset the module should be in 'ADS7128_STATE_UNINIT' state
    TEST_ASSERT_EQUAL_UINT16(ADS7128_RETURN_SUCCESS, ads7128_get_status(&state));
    TEST_ASSERT_EQUAL_UINT16(ADS7128_STATE_UNINIT, state);
}

/**
 * @brief RUN, test group
 * 
 * - Verify parameter/state validation
 * - Verify data is correctly read from sensors [ i2C driver/bus ]
 * - Verify readings are correctly stored in the writeOnly buffer
 * - Verify that writeOnly entries are set to default upon invalid reading
 * - Verify callback is called at the end of the reading cycle
 * - Verify reading cycle interval is implemented
 * - verify that start() starts ADC readings
 * - verify that stop() stop ADC readings
 * - Verify that set_sampling_interval() is implemented
 */
void test_ads7128_run(void) {
    TEST_IGNORE_MESSAGE("Add RUN tests...");
}

/**
 * @brief READINGS, test group
 * 
 * - Verify parameter/state validation
 * - Verify writeOnly buffer is locked for reading through API
 * - Verify data is correctly returned upon read API call, by channel id, from readOnly buffer
 * - Verify data is correctly returned upon read API call, by all channels, from readOnly buffer
 */
void test_ads7128_read_channel(void) {
    TEST_IGNORE_MESSAGE("Add READINGS tests...");
}

/**
 * @brief CALIBRATION, test group
 * 
 * - Verify parameter/state validation
 * - Verify that calibration is correctly set for a single channel
 * - Verify that calibration is correctly set for all channels simultaneously
 */
void test_ads7128_calibration(void) {
    TEST_IGNORE_MESSAGE("Add CALIBRATION tests...");
}

/**
 * @brief STATUS, test group
 * 
 * - Verify parameter/state validation
 * - Verify that status are correctly returned upon an API call to get_status()
 */
void test_ads7128_status(void) {
    TEST_IGNORE_MESSAGE("Add STATUS tests...");
    I2C_STATE_t status;
    i2c_get_status_ExpectAnyArgsAndReturn(ADS7128_RETURN_SUCCESS);
    // i2c_get_status_IgnoreAndReturn(ADS7128_RETURN_SUCCESS);
    i2c_get_status_Stub(my_stub_function);

    i2c_get_status(I2C_INSTANCE_A, &status);
    i2c_get_status(I2C_INSTANCE_A, &status);

}

/* ==== Utility Functions ===== */

/**
 * @brief This callback is passed by the ADS7128 to the I2C driver init, and it's called
 * by the I2C driver when a transaction is done. It's required to mock the inputs to ADS.
 * 
 * @param instance, I2C instance
 * @param transfer, pointer to the I2C transfer structure
 * @param cmock_num_calls, this is handle by cmock, and is a counter for the callback calls 
 * @return I2C_RETURN_t, indicating success or error.  
 */
static I2C_RETURN_t my_stub_function(const I2C_INSTANCE_t instance,
                                    const i2c_transfer_t* const transfer,
                                    int cmock_num_calls) {
    I2C_RETURN_t rc = I2C_RETURN_SUCCESS;

    // 'cmock_num_calls' cannot be reset mid-test
    // needs to be implemented manually
    static uint32_t callback_num_calls = 0;

    switch (g_stub_context) {
    case STUB_CODE_INIT_DEFAULTS:
        // call the ADS callback to signal the I2C transaction completed
        transfer->callback(true);
        break;

    case STUB_CODE_INIT_USER_CFG:
        // check the ADS is passing the user's data to the I2C driver
        TEST_ASSERT_EQUAL_UINT16(g_init_user_data[cmock_num_calls].addr,
                                transfer->txBuffer[1U]);
        TEST_ASSERT_EQUAL_UINT16(g_init_user_data[cmock_num_calls].value,
                                transfer->txBuffer[2U]);
        // call the ADS callback to signal the I2C transaction is completed
        transfer->callback(true);
        break;

    case STUB_CODE_INIT_READ_BACK_CFG:
        callback_num_calls++;

        // only even callback calls are from read frames
        // mock the I2C driver read frame with user's data
        if (0U != (callback_num_calls % 2)) {
            transfer->rxBuffer[0] = g_init_user_data[callback_num_calls / 2].value;
        }
        transfer->callback(true);
        break;

    case STUB_CODE_SW_RESET:
        // check the ADS is passing the sw reset code to the I2C driver
        TEST_ASSERT_EQUAL_UINT16(GEN_CFG_R, transfer->txBuffer[1U]);
        TEST_ASSERT_EQUAL_UINT16(GEN_CFG_RST_F, transfer->txBuffer[2U]);

        // call the ADS callback to indicate the I2C transaction has completed
        transfer->callback(true);
        break;

    default:
        TEST_PRINTF("Callback code: %d", g_stub_context);
        TEST_FAIL_MESSAGE("Callback code unknown!");
        break;
    }

    // TEST_PRINTF("cmock_num_calls %u", cmock_num_calls);
    // TEST_PRINTF("callback_num_calls %u", callback_num_calls);
    return rc;
}

#if 0 // just in case a mock_addCallback is required
/**
 * @brief 
 * 
 * @param instance 
 * @param transfer 
 * @param cmock_num_calls 
 * @return I2C_RETURN_t 
 */
static I2C_RETURN_t my_callback_function(const I2C_INSTANCE_t instance,
                                        const i2c_transfer_t* const transfer,
                                        int cmock_num_calls) {
    I2C_RETURN_t rc = I2C_RETURN_SUCCESS;
    // call the ADS callback to indicate the I2C transaction has completed
    transfer->callback(true);
 
    return rc;
}
#endif

/**
 * @brief This callback is passed to the ADS module.
 * It uses a global variable to share the result of the
 * callback across the module.
 * 
 * @param success, boolean indicating success or error.  
 */
static void my_user_callback(bool success) {
    if (success) {
        g_usr_callback_success = 1;
    } else {
        g_usr_callback_success = -1;
    }
}